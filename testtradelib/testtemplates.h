
#ifndef __TESTTEMPLATES_H__
#define __TESTTEMPLATES_H__
#include "testcommon.h"
typedef std::function<bool()> TestFunc;
std::map<std::string, TestFunc> getTestTemplates(const common::string_t& exchange, common::Logger::Level logLevel);
#endif //__TESTTEMPLATES_H__

