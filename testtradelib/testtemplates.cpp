// testactor.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "commondefs.h"
#include <vector>
#include <chrono>
#include <typeinfo>
#include <thread>
#include <algorithm>
#include "pplxtasks.h"
#include <map>
#include <chrono>
#include <iostream>
#include <functional>
#include "testcommon.h"
#include "tradeutil.h"
#include "tradeapicoll.h"
#include "exchanges.h"
#include "poloniexdepth.h"
#include "commonutil.h"
#include "tradefunc.h"
#include "testtemplates.h"

inline std::string testName(const std::string& t)
{
	return __FILENAME__ + std::string("::") + t;
}

using namespace tradelib;
using namespace common;

typedef pplx::task<tradelib::BalanceSeqRef> BalanceRefTask;
string_t apiKeyFile = U("apikeys.json");



const int nTries = 10;
const double x_amount = 0.1;
const double x_amount1 = 0.01;
const double x_priceInvalid = 0.0001;
const double x_amountInvalid = 0.0001;




std::map<std::string, TestFunc> getTestTemplates(const common::string_t& exchange, common::Logger::Level logLevel){
	return{

		std::pair<std::string, TestFunc>(testName("test0"), TestFunc([exchange]()
		{
			std::cout << "test0: Remove all open orders " << common::ws2s(exchange) << std::endl;

			auto quote = testcommon::getBidAsk();
			auto key = readApiKey(apiKeyFile, exchange);
			string_t ordNum;
			Pair pair = ETH_BTC;

			// check order status
			auto fact = tradelib::x_tradeApiCollection.at(exchange)->orderStatusFactory();
			std::shared_ptr<common::TryResult<OrderStatusRef>> tr;

			auto d = fact(pair, OrderStatusCfg(key));
			assert(d);
			tr = std::make_shared<common::TryResult<OrderStatusRef>>(exchangelib::Try(std::function<OrderStatusRef()>([d](){
				return d->fetch().get();
			})));
			auto end = std::chrono::system_clock::now();
			auto rt = std::chrono::duration_cast<std::chrono::milliseconds>(end - d->start()).count();
			std::cout << "RT time (ms): " << rt << std::endl;
			if (tr->isFailure()) std::cout << "exception: " << tr->failure().what() << std::endl;

			TSTASSERT(tr->isSuccess(), "Failed to fetch order");
			TSTASSERT(tr->success()->pair == pair, "Symbols sets size is different");
			std::cout << "Size of infos: " << tr->success()->infos.size() << std::endl;

			// cancel order
			auto cancelFact = tradelib::x_tradeApiCollection.at(exchange)->cancelOrderFactory();
			for (auto o : tr->success()->infos){
				std::cout << "Cancel order: " << ws2s(o.orderNumber) << std::endl;
				auto r = exchangelib::Try(std::function<CancelOrderRef()>([&cancelFact, &pair, &o, &key](){
					return cancelFact(CancelOrderIn(pair, o.orderNumber), CancelOrderCfg(key))->fetch().get();
				}));
				if (r.isFailure()) std::cout << "exception: " << r.failure().what() << std::endl;
				TSTASSERT(r.isSuccess(), "Failed to cancel order");
			}
			return true;
		}
		)),

		std::pair<std::string, TestFunc>(testName("test1"), TestFunc([exchange]()
		{
			std::cout << "test1: Query balance for exchange " << common::ws2s(exchange) << std::endl;

			auto key = readApiKey(apiKeyFile, exchange);

			std::vector<Symbol> symbols = { BTC,ETH };

			auto fact = tradelib::x_tradeApiCollection.at(exchange)->balanceFactory();
			auto d = fact(symbols, BalanceCfg(key));
			assert(d);
			auto r = exchangelib::Try(std::function<BalanceSeqRef()>([d](){
				return d->fetch().get();
			}));
			auto end = std::chrono::system_clock::now();
			auto rt = std::chrono::duration_cast<std::chrono::milliseconds>(end - d->start()).count();
			std::cout << "RT time (ms): " << rt << std::endl;
			if (r.isFailure()) std::cout << "exception: " << r.failure().what() << std::endl;
			TSTASSERT(r.isSuccess(), "Failed to fetch balance");
			TSTASSERT(r.success()->size() == symbols.size(),"Symbols sets size is different");
			decltype(symbols) syms;
			for (auto e : *(r.success())){
				syms.push_back(e.symbol);
			}
			std::sort(syms.begin(), syms.end());
			std::sort(symbols.begin(), symbols.end());
			auto rr = std::includes(syms.cbegin(), syms.cend(), symbols.cbegin(), symbols.cend());
			TSTASSERT(rr, "Sets are different");

			return true;
		}
			)),

		std::pair<std::string, TestFunc>(testName("test2"), TestFunc([exchange]()
		{
			std::cout << "test2: Cancel invalid number on exchange " << common::ws2s(exchange) << std::endl;
			// odd bug allows deleting non-existing order
			if (exchange == U("bitflyer")) return true;
			auto key = readApiKey(apiKeyFile, exchange);

			Pair pair=ETH_BTC;

			auto cancelFact = tradelib::x_tradeApiCollection.at(exchange)->cancelOrderFactory();
			std::cout << "Cancel order: " << "111" << std::endl;
			auto r = exchangelib::Try(std::function<CancelOrderRef()>([&cancelFact, &pair, &key](){
				return cancelFact(CancelOrderIn(pair, U("111")), CancelOrderCfg(key))->fetch().get();
			}));
			if (r.isFailure()) std::cout << "exception: " << r.failure().what() << std::endl;
			TSTASSERT(r.isFailure(), "Succeded in cancelling invalid order");
			
			return true;
		}
		)),


		std::pair<std::string, TestFunc>(testName("test3"), TestFunc([exchange]()
		{
			std::cout << "test3: Buy price is too low " << common::ws2s(exchange) << std::endl;

			auto key = readApiKey(apiKeyFile, exchange);
			// create buy order
			TradeOrderIn in(OrderType::Buy, ETH_BTC, x_priceInvalid, x_amount);
			auto factTrade = tradelib::x_tradeApiCollection.at(exchange)->tradeOrderFactory();
			auto d = factTrade(in, TradeOrderCfg(key));
			assert(d);
			auto r = exchangelib::Try(std::function<TradeOrderRef()>([d](){
				return d->fetch().get();
			}));
			auto end = std::chrono::system_clock::now();
			auto rt = std::chrono::duration_cast<std::chrono::milliseconds>(end - d->start()).count();
			std::cout << "RT time (ms): " << rt << std::endl;
			if (r.isFailure()) std::cout << "exception: " << r.failure().what() << std::endl;
			TSTASSERT(!r.isSuccess(), "Success to place very low order");

			return true;
		}
		)),

			std::pair<std::string, TestFunc>(testName("test4"), TestFunc([exchange]()
		{
			std::cout << "test4: Buy amount is too low " << common::ws2s(exchange) << std::endl;
			auto key = readApiKey(apiKeyFile, exchange);
			auto quote = testcommon::getBidAsk();
			// create buy order
			TradeOrderIn in(OrderType::Buy, ETH_BTC, round(quote.first*1.2,10000.0), x_amountInvalid);
			auto factTrade = tradelib::x_tradeApiCollection.at(exchange)->tradeOrderFactory();
			auto d = factTrade(in, TradeOrderCfg(key));
			assert(d);
			auto r = exchangelib::Try(std::function<TradeOrderRef()>([d](){
				return d->fetch().get();
			}));
			auto end = std::chrono::system_clock::now();
			auto rt = std::chrono::duration_cast<std::chrono::milliseconds>(end - d->start()).count();
			std::cout << "RT time (ms): " << rt << std::endl;
			if (r.isFailure()) std::cout << "exception: " << r.failure().what() << std::endl;
			TSTASSERT(!r.isSuccess(), "Success to place very low order");

			return true;
		}
		)),

			std::pair<std::string, TestFunc>(testName("test5"), TestFunc([exchange]()
		{
			std::cout << "test5: Buy and cancel order  for exchange " << common::ws2s(exchange) << std::endl;



			auto quote = testcommon::getBidAsk();
			auto key = readApiKey(apiKeyFile, exchange);
			string_t ordNum;
			Pair pair = ETH_BTC;

			{
				// create buy order
				TradeOrderIn in(OrderType::Buy, ETH_BTC, round(quote.second / 2,10000.0), x_amount);
				auto factTrade = tradelib::x_tradeApiCollection.at(exchange)->tradeOrderFactory();
				auto d = factTrade(in, TradeOrderCfg(key));
				assert(d);
				auto r = exchangelib::Try(std::function<TradeOrderRef()>([d](){
					return d->fetch().get();
				}));
				auto end = std::chrono::system_clock::now();
				auto rt = std::chrono::duration_cast<std::chrono::milliseconds>(end - d->start()).count();
				std::cout << "RT time (ms): " << rt << std::endl;
				if (r.isFailure()) std::cout << "exception: " << r.failure().what() << std::endl;
				TSTASSERT(r.isSuccess(), "Failed to place order");
				TSTASSERT(r.success()->pair == pair, "Order pairs are different different");
				TSTASSERT(!r.success()->info.orderNumber.empty(), "Order number is emptyt");
				xcout << U("Order number: ") << r.success()->info.orderNumber << r.success()->info.trades.size() << std::endl;
				ordNum = r.success()->info.orderNumber;
			}

			// check order status
			auto fact = tradelib::x_tradeApiCollection.at(exchange)->orderStatusFactory();
			std::shared_ptr<common::TryResult<OrderStatusRef>> tr;
			for (int i = 0; i < nTries; i++)
			{
				std::cout << "attempt: " << i << std::endl;
				auto d = fact(pair, OrderStatusCfg(key));
				assert(d);
				tr = std::make_shared<common::TryResult<OrderStatusRef>>(exchangelib::Try(std::function<OrderStatusRef()>([d](){
					return d->fetch().get();
				})));
				auto end = std::chrono::system_clock::now();
				auto rt = std::chrono::duration_cast<std::chrono::milliseconds>(end - d->start()).count();
				std::cout << "RT time (ms): " << rt << std::endl;
				if (tr->isFailure()) std::cout << "exception: " << tr->failure().what() << std::endl;
				if (tr->isSuccess() && !tr->success()->infos.empty()) { break; }
				std::this_thread::sleep_for(std::chrono::milliseconds(1000));
			}

			TSTASSERT(tr->isSuccess(), "Failed to fetch order");
			TSTASSERT(tr->success()->pair == pair, "Symbols sets size is different");
			std::cout << "Size of infos: " << tr->success()->infos.size() << std::endl;
			TSTASSERT(tr->success()->infos.size(), "No open orders exist");
			TSTASSERT(tr->success()->infos[0].orderNumber == ordNum, "Order num doesn't match");

			auto cancelFact = tradelib::x_tradeApiCollection.at(exchange)->cancelOrderFactory();
			// try to cancel invalid order first
			// odd bug allows deleting non-existing order
			if (exchange != U("bitflyer")) {

				std::cout << "Cancel order: " << "1122" << std::endl;
				auto r = exchangelib::Try(std::function<CancelOrderRef()>([&cancelFact, &pair, &key](){
					return cancelFact(CancelOrderIn(pair, U("1122")), CancelOrderCfg(key))->fetch().get();
				}));
				if (r.isFailure()) std::cout << "exception: " << r.failure().what() << std::endl;
				TSTASSERT(r.isFailure(), "Succeeded in cancel order");
			}

			// cancel order

			for (auto o : tr->success()->infos){
				std::cout << "Cancel order: " << ws2s(o.orderNumber) << std::endl;
				auto r = exchangelib::Try(std::function<CancelOrderRef()>([&cancelFact, &pair, &o, &key](){
					return cancelFact(CancelOrderIn(pair, o.orderNumber), CancelOrderCfg(key))->fetch().get();
				}));
				if (r.isFailure()) std::cout << "exception: " << r.failure().what() << std::endl;
				TSTASSERT(r.isSuccess(), "Failed to cancel order");
			}
			return true;
		}
			)),
				std::pair<std::string, TestFunc>(testName("test6"), TestFunc([exchange]()
			{
				std::cout << "test6: Sell and cancel order  for exchange " << common::ws2s(exchange) << std::endl;
				auto quote = testcommon::getBidAsk();

				auto key = readApiKey(apiKeyFile, exchange);

				Pair pair = ETH_BTC;
				string_t ordNum;
				{
					// create buy order
					TradeOrderIn in(OrderType::Sell, ETH_BTC, round(quote.first * 2,10000.0), x_amount);
					auto factTrade = tradelib::x_tradeApiCollection.at(exchange)->tradeOrderFactory();
					auto d = factTrade(in, TradeOrderCfg(key));
					assert(d);
					auto r = exchangelib::Try(std::function<TradeOrderRef()>([d](){
						return d->fetch().get();
					}));
					auto end = std::chrono::system_clock::now();
					auto rt = std::chrono::duration_cast<std::chrono::milliseconds>(end - d->start()).count();
					std::cout << "RT time (ms): " << rt << std::endl;
					if (r.isFailure()) std::cout << "exception: " << r.failure().what() << std::endl;
					TSTASSERT(r.isSuccess(), "Failed to place order");
					TSTASSERT(r.success()->pair == pair, "Order pairs are different different");
					TSTASSERT(!r.success()->info.orderNumber.empty(), "Order number is emptyt");
					xcout << U("Order number: ") << r.success()->info.orderNumber << r.success()->info.trades.size() << std::endl;
					ordNum = r.success()->info.orderNumber;
				}

				// check order status
				auto fact = tradelib::x_tradeApiCollection.at(exchange)->orderStatusFactory();
				std::shared_ptr<common::TryResult<OrderStatusRef>> tr;
				for (int i = 0; i < nTries; i++)
				{
					std::cout << "attempt: " << i << std::endl;
					auto d = fact(pair, OrderStatusCfg(key));
					assert(d);
					tr = std::make_shared<common::TryResult<OrderStatusRef>>(exchangelib::Try(std::function<OrderStatusRef()>([d](){
						return d->fetch().get();
					})));
					auto end = std::chrono::system_clock::now();
					auto rt = std::chrono::duration_cast<std::chrono::milliseconds>(end - d->start()).count();
					std::cout << "RT time (ms): " << rt << std::endl;
					if (tr->isFailure()) std::cout << "exception: " << tr->failure().what() << std::endl;
					if (tr->isSuccess() && !tr->success()->infos.empty()) { break; }
					std::this_thread::sleep_for(std::chrono::milliseconds(1000));
				}

				TSTASSERT(tr->isSuccess(), "Failed to fetch order");
				TSTASSERT(tr->success()->pair == pair, "Symbols sets size is different");
				std::cout << "Size of infos: " << tr->success()->infos.size() << std::endl;
				TSTASSERT(tr->success()->infos.size(), "No open orders exist");
				TSTASSERT(tr->success()->infos[0].orderNumber == ordNum, "Order num doesn't match");
				

				// cancel order
				auto cancelFact = tradelib::x_tradeApiCollection.at(exchange)->cancelOrderFactory();
				for (auto o : tr->success()->infos){
					std::cout << "Cancel order: " << ws2s(o.orderNumber) << std::endl;
					auto r = exchangelib::Try(std::function<CancelOrderRef()>([&cancelFact, &pair, &o, &key](){
						return cancelFact(CancelOrderIn(pair, o.orderNumber), CancelOrderCfg(key))->fetch().get();
					}));
					if (r.isFailure()) std::cout << "exception: " << r.failure().what() << std::endl;
					TSTASSERT(r.isSuccess(), "Failed to cancel order");
				}
				return true;
			}
			)),

				std::pair<std::string, TestFunc>(testName("test7"), TestFunc([exchange]()
			{
				std::cout << "test7: Buy small amount " << common::ws2s(exchange) << std::endl;
				auto quote = testcommon::getBidAsk();
				Pair pair = ETH_BTC;
				auto key = readApiKey(apiKeyFile, exchange);
				// create buy order
				TradeOrderIn in(OrderType::Buy, ETH_BTC, round(quote.first*1.3,10000.0), x_amount1);
				auto factTrade = tradelib::x_tradeApiCollection.at(exchange)->tradeOrderFactory();
				auto d = factTrade(in, TradeOrderCfg(key));
				assert(d);
				auto r = exchangelib::Try(std::function<TradeOrderRef()>([d](){
					return d->fetch().get();
				}));
				auto end = std::chrono::system_clock::now();
				auto rt = std::chrono::duration_cast<std::chrono::milliseconds>(end - d->start()).count();
				std::cout << "RT time (ms): " << rt << std::endl;
				if (r.isFailure()) std::cout << "exception: " << r.failure().what() << std::endl;
				TSTASSERT(r.isSuccess(), "Failure to buy very low amount");
				TSTASSERT(r.success()->pair == in.pair, "Failure pairs dont match");
				TSTASSERT(!r.success()->info.orderNumber.empty(), "Failure orderNumber is empty");
				TSTASSERT(exchange != U("poloniex") || !r.success()->info.trades.empty(), "Failure trades are empty");

				// check order status
				auto fact = tradelib::x_tradeApiCollection.at(exchange)->orderStatusFactory();
				std::shared_ptr<common::TryResult<OrderStatusRef>> tr;
				for (int i = 0; i < 3; i++)
				{
					std::cout << "attempt: " << i << std::endl;
					auto d = fact(pair, OrderStatusCfg(key));
					assert(d);
					tr = std::make_shared<common::TryResult<OrderStatusRef>>(exchangelib::Try(std::function<OrderStatusRef()>([d](){
						return d->fetch().get();
					})));
					auto end = std::chrono::system_clock::now();
					auto rt = std::chrono::duration_cast<std::chrono::milliseconds>(end - d->start()).count();
					std::cout << "RT time (ms): " << rt << std::endl;
					if (tr->isFailure()) std::cout << "exception: " << tr->failure().what() << std::endl;
					if (tr->isSuccess() && !tr->success()->infos.empty()) { break; }
					std::this_thread::sleep_for(std::chrono::milliseconds(1000));
				}
				TSTASSERT(tr->isSuccess(), "Failed to fetch order");
				std::cout << "Size of infos: " << tr->success()->infos.size() << std::endl;
				TSTASSERT(tr->success()->infos.empty(), "Open orders exists");

				return true;
			}
			)),

				std::pair<std::string, TestFunc>(testName("test8"), TestFunc([exchange]()
			{
				std::cout << "test8: Sell small amount " << common::ws2s(exchange) << std::endl;

				auto quote = testcommon::getBidAsk();
				Pair pair = ETH_BTC;
				auto key = readApiKey(apiKeyFile, exchange);
				// create buy order
				TradeOrderIn in(OrderType::Sell, ETH_BTC, round(quote.second/1.3,10000.0), x_amount1);
				auto factTrade = tradelib::x_tradeApiCollection.at(exchange)->tradeOrderFactory();
				auto d = factTrade(in, TradeOrderCfg(key));
				assert(d);
				auto r = exchangelib::Try(std::function<TradeOrderRef()>([d](){
					return d->fetch().get();
				}));
				auto end = std::chrono::system_clock::now();
				auto rt = std::chrono::duration_cast<std::chrono::milliseconds>(end - d->start()).count();
				std::cout << "RT time (ms): " << rt << std::endl;
				if (r.isFailure()) std::cout << "exception: " << r.failure().what() << std::endl;
				TSTASSERT(r.isSuccess(), "Failure to buy very low amount");
				TSTASSERT(r.success()->pair == in.pair, "Failure pairs dont match");
				TSTASSERT(!r.success()->info.orderNumber.empty(), "Failure orderNumber is empty");
				TSTASSERT(exchange != U("poloniex") || !r.success()->info.trades.empty(), "Failure trades are empty");

				// check order status
				auto fact = tradelib::x_tradeApiCollection.at(exchange)->orderStatusFactory();
				std::shared_ptr<common::TryResult<OrderStatusRef>> tr;
				for (int i = 0; i < 3; i++)
				{
					std::cout << "attempt: " << i << std::endl;
					auto d = fact(pair, OrderStatusCfg(key));
					assert(d);
					tr = std::make_shared<common::TryResult<OrderStatusRef>>(exchangelib::Try(std::function<OrderStatusRef()>([d](){
						return d->fetch().get();
					})));
					auto end = std::chrono::system_clock::now();
					auto rt = std::chrono::duration_cast<std::chrono::milliseconds>(end - d->start()).count();
					std::cout << "RT time (ms): " << rt << std::endl;
					if (tr->isFailure()) std::cout << "exception: " << tr->failure().what() << std::endl;
					if (tr->isSuccess() && !tr->success()->infos.empty()) { break; }
					std::this_thread::sleep_for(std::chrono::milliseconds(1000));
				}
				TSTASSERT(tr->isSuccess(), "Failed to fetch order");
				std::cout << "Size of infos: " << tr->success()->infos.size() << std::endl;
				TSTASSERT(tr->success()->infos.empty(), "Open orders exists");


				return true;
			}
			)),

				std::pair<std::string, TestFunc>(testName("test9"), TestFunc([exchange]()
			{
				std::cout << "test9: Sell and cancel order in bulk  for exchange  " << common::ws2s(exchange) << std::endl;
				auto quote = testcommon::getBidAsk();

				auto key = readApiKey(apiKeyFile, exchange);

				Pair pair = ETH_BTC;

				size_t N = 10;
				// create buy order
				TradeOrderIn in(OrderType::Sell, ETH_BTC, round(quote.first * 2, 10000.0), x_amount);
				auto factTrade = tradelib::x_tradeApiCollection.at(exchange)->tradeOrderFactory();
				auto qv = std::vector<TradeQueryFunc<TradeOrderRef>>();
				auto cfg = TradeOrderCfg(key);
				for (size_t i = 0; i < N; i++){
					qv.push_back(
						std::function<TradeQueryRef<pplx::task<TradeOrderRef>>()>([factTrade, in, cfg](){
						return factTrade(in, cfg);})
						);
				}

				//auto ret = queryBatchWithDelay(qv);
				auto ret = queryBatchWithDelay(qv);
				auto r = exchangelib::Try(std::function<BatchTryResultsRef<TradeOrderRef>()>([&ret](){
					return ret.get();
				}));
				TSTASSERT(r.isSuccess(), "Failed to get batch tradeorder");
				TSTASSERT(r.success() != nullptr, "Result null");
				TSTASSERT(r.success()->size() == N, "Result size is wrong");
				auto ordNums = std::vector<string_t>();
				for (auto e : *r.success()){
					if (e.isFailure()){
						std::cout << "Failure: "<<e.failure().what() << std::endl;
					}
					TSTASSERT(e.isSuccess() , "TradeOrder is failure");
					ordNums.push_back(e.success()->info.orderNumber);
				}

				// check order status
				auto fact = tradelib::x_tradeApiCollection.at(exchange)->orderStatusFactory();
				std::shared_ptr<common::TryResult<OrderStatusRef>> tr;
				for (int i = 0; i < nTries; i++)
				{
					std::cout << "attempt: " << i << std::endl;
					auto d = fact(pair, OrderStatusCfg(key));
					assert(d);
					tr = std::make_shared<common::TryResult<OrderStatusRef>>(exchangelib::Try(std::function<OrderStatusRef()>([d](){
						return d->fetch().get();
					})));
					auto end = std::chrono::system_clock::now();
					auto rt = std::chrono::duration_cast<std::chrono::milliseconds>(end - d->start()).count();
					std::cout << "RT time (ms): " << rt << std::endl;
					if (tr->isFailure()) std::cout << "exception: " << tr->failure().what() << std::endl;
					if (tr->isSuccess() && !tr->success()->infos.empty()) { break; }
					std::this_thread::sleep_for(std::chrono::milliseconds(1000));
				}

				TSTASSERT(tr->isSuccess(), "Failed to fetch order");
				TSTASSERT(tr->success()->pair == pair, "Symbols sets size is different");
				std::cout << "Size of infos: " << tr->success()->infos.size() << std::endl;
				TSTASSERT(tr->success()->infos.size()==N, "Wrong number of orders");
				for (auto i : tr->success()->infos){
					TSTASSERT(std::find(ordNums.cbegin(), ordNums.cend(), i.orderNumber) != ordNums.cend(), "Cannot find order number");
				}

				// cancel order
				auto cancelFact = tradelib::x_tradeApiCollection.at(exchange)->cancelOrderFactory();
				for (auto o : tr->success()->infos){
					std::cout << "Cancel order: " << ws2s(o.orderNumber) << std::endl;
					auto r = exchangelib::Try(std::function<CancelOrderRef()>([&cancelFact, &pair, &o, &key](){
						return cancelFact(CancelOrderIn(pair, o.orderNumber), CancelOrderCfg(key))->fetch().get();
					}));
					if (r.isFailure()) std::cout << "exception: " << r.failure().what() << std::endl;
					TSTASSERT(r.isSuccess(), "Failed to cancel order");
				}
				return true;
			}
				)),
				std::pair<std::string, TestFunc>(testName("test10"), TestFunc([exchange]()
			{
				std::cout << "test10: Sell and cancel order in bulk  sequence  for exchange  " << common::ws2s(exchange) << std::endl;
				auto quote = testcommon::getBidAsk();

				auto key = readApiKey(apiKeyFile, exchange);

				Pair pair = ETH_BTC;

				size_t N = 10;
				// create buy order
				TradeOrderIn in(OrderType::Sell, ETH_BTC, round(quote.first * 2, 10000.0), x_amount);
				auto factTrade = tradelib::x_tradeApiCollection.at(exchange)->tradeOrderFactory();
				auto qv = std::make_shared<std::vector<TradeQueryRef<pplx::task<TradeOrderRef>>>>();
				auto cfg = TradeOrderCfg(key);
				auto s1 = common::epochTimeMs();
				for (size_t i = 0; i < N; i++){
					qv->push_back(factTrade(in, cfg));
				}
				auto e1 = common::epochTimeMs();
				std::cout << "Average time to create orders " << ((double)(e1 - s1)) / N << std::endl;

				auto s2 = common::epochTimeMs();
				auto ret = queryBatch<TradeOrderRef>(qv);
				auto r = exchangelib::Try(std::function<BatchTryResultsRef<TradeOrderRef>()>([&ret](){
					return ret.get();
				}));
				auto e2 = common::epochTimeMs();
				std::cout << "Average time to execute orders " << ((double)(e2 - s2)) / N << std::endl;
				TSTASSERT(r.isSuccess(), "Failed to get batch tradeorder");
				TSTASSERT(r.success() != nullptr, "Result null");
				TSTASSERT(r.success()->size() == N, "Result size is wrong");
				auto ordNums = std::vector<string_t>();
				for (auto e : *r.success()){
					if (e.isFailure()){
						std::cout << "Failure: " << e.failure().what() << std::endl;
					}
					TSTASSERT(e.isSuccess(), "TradeOrder is failure");
					ordNums.push_back(e.success()->info.orderNumber);
				}

				// check order status
				auto fact = tradelib::x_tradeApiCollection.at(exchange)->orderStatusFactory();
				std::shared_ptr<common::TryResult<OrderStatusRef>> tr;
				for (int i = 0; i < nTries; i++)
				{
					std::cout << "attempt: " << i << std::endl;
					auto d = fact(pair, OrderStatusCfg(key));
					assert(d);
					tr = std::make_shared<common::TryResult<OrderStatusRef>>(exchangelib::Try(std::function<OrderStatusRef()>([d](){
						return d->fetch().get();
					})));
					auto end = std::chrono::system_clock::now();
					auto rt = std::chrono::duration_cast<std::chrono::milliseconds>(end - d->start()).count();
					std::cout << "RT time (ms): " << rt << std::endl;
					if (tr->isFailure()) std::cout << "exception: " << tr->failure().what() << std::endl;
					if (tr->isSuccess() && !tr->success()->infos.empty()) { break; }
					std::this_thread::sleep_for(std::chrono::milliseconds(1000));
				}

				TSTASSERT(tr->isSuccess(), "Failed to fetch order");
				TSTASSERT(tr->success()->pair == pair, "Symbols sets size is different");
				std::cout << "Size of infos: " << tr->success()->infos.size() << std::endl;
				TSTASSERT(tr->success()->infos.size() == N, "Wrong number of orders");
				for (auto i : tr->success()->infos){
					TSTASSERT(std::find(ordNums.cbegin(), ordNums.cend(), i.orderNumber) != ordNums.cend(), "Cannot find order number");
				}

				// cancel order
				auto cancelFact = tradelib::x_tradeApiCollection.at(exchange)->cancelOrderFactory();
				for (auto o : tr->success()->infos){
					std::cout << "Cancel order: " << ws2s(o.orderNumber) << std::endl;
					auto r = exchangelib::Try(std::function<CancelOrderRef()>([&cancelFact, &pair, &o, &key](){
						return cancelFact(CancelOrderIn(pair, o.orderNumber), CancelOrderCfg(key))->fetch().get();
					}));
					if (r.isFailure()) std::cout << "exception: " << r.failure().what() << std::endl;
					TSTASSERT(r.isSuccess(), "Failed to cancel order");
				}
				return true;
			}
				)),

	};
}