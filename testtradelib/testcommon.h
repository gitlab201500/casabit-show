
#ifndef __TESTCOMMON_H__
#define __TESTCOMMON_H__

#include <iostream>
#include <chrono>
#include <map>
#include <functional>
#include "commonpplx.h"
#include "commonutil.h"
#include "exchutil.h"
#include "exchentity.h"
#include "testtemplates.h"

typedef std::function<bool()> TestFunc;

namespace testcommon{
	template <class ERef>
	inline std::pair<common::Stats<long long>, int> roundtripTest(int nevents, exchangelib::Pair pair,
		const std::function<exchangelib::ExchangeQueryRef<pplx::task<ERef>>()>& fact
		)
	{
		typedef std::pair<long long, common::TryResult<ERef>> TaskPair;

		Stats<long long> stats;
		int fail = 0;
		{
			std::vector<pplx::task<TaskPair>> tasks;
			for (int i = 0; i < nevents; i++){
				auto d = fact();
				d->fetch().wait();
				auto end = std::chrono::system_clock::now();
				long long rt = std::chrono::duration_cast<std::chrono::milliseconds>(end - d->start()).count();
				stats = stats + rt;
			}
		}

		return std::pair<decltype(stats), decltype(fail)>(stats, fail);
	}


	inline void launchDelayedTask(std::chrono::milliseconds delay, int count, std::function<void()> payload){
		if (count <= 0) return;
		payload();
		//std::cout << "time: " << std::chrono::system_clock::now().time_since_epoch().count() << std::endl;
		common::create_delayed_task(delay, [delay, count, payload](){
			launchDelayedTask(delay, count - 1, payload);
		});
	}

	template <class ERef>
	inline std::pair<common::Stats<long long>, int> rateTest(std::chrono::milliseconds delay, int nevents, exchangelib::Pair pair,
		const std::function<exchangelib::ExchangeQueryRef<pplx::task<ERef>>()>& fact){

		typedef std::pair<long long, common::TryResult<ERef>> TaskPair;

		common::Stats<long long> stats;


		int fail = 0;
		{
			pplx::extensibility::event_t termEvt;
			int completed = 0;
			common::CriticalSection cs;
			auto startTest = std::chrono::system_clock::now();
			std::vector<pplx::task<TaskPair>> tasks;
			launchDelayedTask(delay, nevents, [&tasks, pair, &completed, nevents, &termEvt, &cs, fact](){
				auto d = fact();
				auto newt = d->fetch().then([d, &completed, nevents, &termEvt, &cs](pplx::task<ERef>  t){
					auto end = std::chrono::system_clock::now();
					long long rt = std::chrono::duration_cast<std::chrono::milliseconds>(end - d->start()).count();
					auto r = exchangelib::Try(std::function<ERef()>([t](){
						return t.get();
					}));

					{
						common::AutoCs lock(cs);
						completed++;
						if (completed == nevents) termEvt.set();
					}
					return std::pair<decltype(rt), decltype(r)>(rt, r);
				});
				tasks.push_back(newt);
			});

			//auto t3 = pplx::when_all(begin(tasks), end(tasks));
			//t3.wait();
			termEvt.wait();
			auto endTest = std::chrono::system_clock::now();
			long long qt = std::chrono::duration_cast<std::chrono::milliseconds>(endTest - startTest).count();
			std::cout << "test query time (ms): " << qt << std::endl;
			for (int i = 0; i < nevents; i++){
				auto r = exchangelib::Try(std::function<TaskPair()>([tasks, i](){
					return tasks[i].get();
				}));
				if (r.isFailure() || r.success().second.isFailure() ||
					r.success().second.success()->info.asks.size() == 0 ||
					r.success().second.success()->info.bids.size() == 0) fail++;
				else stats = stats + r.success().first;
			}


		}

		return std::pair<Stats<long long>, int>(stats, fail);
	}
	std::pair<double, double> getBidAsk();
}


#define TSTASSERT(x, text) if(!(x)) {std::cerr << "Error:" << text<< std::endl;return false;}
#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)

#endif //__TESTCOMMON_H__