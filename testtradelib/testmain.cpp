#ifdef WIN32
#include "stdafx.h"
#endif //WIN32
#include "testcommon.h"
#include <vector>
#include <string>
#include "commonutil.h"
#include "testbitflyer.h"
#include "testpoloniex.h"


void usage()
{
	std::cout << "Usage testexchangelib --all|--bitflyer|--poloniex [--tn testname]" << std::endl;
}

int _tmain(int argc, _TCHAR* argv[])
{
	std::vector<std::map<std::string, TestFunc> > tests;
	if (argc < 2)
	{
		usage();
		return EXIT_FAILURE;
	}

	bool runall = false;
	std::string tn;
	for (int i = 1; i < argc;)
	{
		if (std::wstring(argv[i]) == L"--all")
		{
			runall = true;
			break;
		}

		if (std::wstring(argv[i]) == L"--bitflyer")
		{
			tests.push_back(g_bitflyerTests);
		}
		else if (std::wstring(argv[i]) == L"--poloniex")
		{
			tests.push_back(g_poloniexTests);
		}
		else if (std::wstring(argv[i]) == L"--tn")
		{
			if (i + 1 >= argc)
			{
				std::cerr << "invalid option" << std::endl;
				usage();
				return EXIT_FAILURE;
			}
			tn = common::ws2s(std::wstring(argv[i + 1]));
			i++;

		}
		else
		{
			std::cerr << "invalid option" << std::endl;
			usage();
			return EXIT_FAILURE;
		}
		i++;
	}

	if (runall)
	{
		tests.clear();
		tests.push_back(g_bitflyerTests);
		tests.push_back(g_poloniexTests);
	}


	std::vector<std::string> failed;
	for (auto ts : tests)
	{
		for (auto it : ts)
		{
			if (!tn.empty() && tn != it.first)
			{
				continue;
			}
			std::cout << "Execute test: " << it.first << std::endl;
			try{
				bool r = it.second();
				if (r)
				{
					std::cout << it.first << " success" << std::endl;
				}
				else
				{
					failed.push_back(it.first);
					std::cerr << "Error: " << it.first << " failed" << std::endl;
				}
			}
			catch (const std::exception&e){
				failed.push_back(it.first);
				std::cerr << "Error: " << it.first << " failed" << "exception: " << ((e.what() != nullptr) ? e.what() : "") << std::endl;
			}
			catch (...){
				failed.push_back(it.first);
				std::cerr << "Error: unexpected exception" << std::endl;
			}
		}
	}


	if (failed.size())
	{
		std::cerr << "Failed tests: ";
		for (auto it : failed)
		{
			std::cerr << it << ",";
		}
		std::cerr << std::endl;
		return 1;
	}
	else
	{
		std::cout << "All tests passed" << std::endl;
		return 0;
	}
}

