
#include "testcommon.h"
#include "poloniexdepth.h"

namespace testcommon{
	std::pair<double, double> getBidAsk(){
		exchangelib::Pair pair = exchangelib::Pair::ETH_BTC;

		auto d = std::make_shared<exchangelib::poloniex::DepthQuery>(pair, exchangelib::DepthCfg());
		assert(d);
		auto r = exchangelib::Try(std::function<exchangelib::DepthRef()>([d](){
			return d->fetch().get();
		}));
		if (r.isFailure()) std::cout << "exception: " << r.failure().what() << std::endl;
		auto asks = r.success()->info.asks;
		auto bids = r.success()->info.bids;
		std::sort(asks.begin(), asks.end(), [](const exchangelib::DepthPairAsk&a1, const exchangelib::DepthPairAsk&a2){
			return a1.q < a2.q;
		});
		std::sort(bids.begin(), bids.end(), [](const exchangelib::DepthPairBid&a1, const exchangelib::DepthPairBid&a2){
			return a1.q > a2.q;
		});
		return std::pair<double, double>(asks[0].q, bids[0].q);
	}
}