#ifndef __ACTOR_H__
#define __ACTOR_H__
#include <deque>
#include <memory>
#include <vector>
#include <functional>
#include "pplxtasks.h"
#include "commondefs.h"
#include "commonutil.h"
#include "unistr.h"
#include "Logger.h"


namespace actor{
	DECLARE_LOGGER(Actor);
	DECLARE_LOGGER(ActorSystem);
	class ActorBase;
	typedef std::shared_ptr<ActorBase> ActorRef;
	typedef std::weak_ptr<ActorBase> ActorRefWeak;

	class ActorSystem;
	typedef std::weak_ptr<ActorSystem> ActorSystemRefWeak;
	typedef std::shared_ptr<ActorSystem> ActorSystemRef;

	const unsigned int InfiniteTimeout = (unsigned int)-1;

	struct ActorSystemConfig{
		static const unsigned int x_defaultTerminaitonMs = InfiniteTimeout;
	};

	struct ActorBaseConfig{
		ActorBaseConfig(unsigned int mailboxSz = 0xFFFFFFFF) :_mailboxSz(mailboxSz){}
		static const unsigned int x_defaultTerminaitonMs = InfiniteTimeout;
		unsigned int _mailboxSz;
	};

	typedef int MessageType;
	class Message{
		const MessageType _type;
		const ActorRefWeak _sender;
	public:
		virtual MessageType type() const { return _type; }
		virtual ~Message(){}
		virtual ActorRef sender() const { return _sender.lock(); }

	protected:
		Message(int type, ActorRefWeak sender = ActorRefWeak()) :_type(type), _sender(sender){}
		MessageType type() { return _type; }
	};

	typedef std::shared_ptr<const Message> MessageRef;

	////http://stackoverflow.com/questions/15549722/double-inheritance-of-enable-shared-from-this
	//struct virtual_enable_shared_from_this_base :
	//	std::enable_shared_from_this<virtual_enable_shared_from_this_base> {
	//	virtual ~virtual_enable_shared_from_this_base() {}
	//};
	//template<typename T>
	//struct virtual_enable_shared_from_this :
	//	virtual virtual_enable_shared_from_this_base {
	//	std::shared_ptr<T> shared_from_this() {
	//		return std::dynamic_pointer_cast<T>(
	//			virtual_enable_shared_from_this_base::shared_from_this());
	//	}
	//};

	//class ActorBase : public virtual_enable_shared_from_this<ActorBase>{
	class ActorBase : public std::enable_shared_from_this<ActorBase>{
		DISABLE_COPY_ASSIGN(ActorBase);
	public:
		typedef std::function<void(const MessageRef& m)> MessageHandler;
		virtual void tell(const MessageRef&m);
		virtual ~ActorBase() { logDebug(logActor, U("Shut down actor ") << _name); }
		virtual pplx::task<bool> terminate(unsigned int tmMs = ActorBaseConfig::x_defaultTerminaitonMs);
		virtual pplx::task<bool> onTerminated(unsigned int tmMs = ActorBaseConfig::x_defaultTerminaitonMs) { return terminationTask(tmMs); }
		virtual common::string_t name() const { return _name; }
		virtual ActorSystemRefWeak system() { return _as; }
		virtual bool isReady() const { return _state == ready; }
		virtual Concurrency::cancellation_token cancellationToken(){ return _cts.get_token(); }

	protected:
		enum State{
			none,
			ready,
			running,
			terminated
		};
		ActorBase() :_state(none){}
		void initActorBase(const common::string_t& n, actor::ActorSystemRefWeak as, const ActorBaseConfig& conf = ActorBaseConfig());
		virtual void becomes(const MessageHandler&h) { _handler = h; }
		virtual MessageHandler handler() { return _handler; }
		virtual bool isReadyToRun() { return _state == ready || _state == running; }
		virtual ActorSystemRef actorSystem(){ return _as.lock(); }

	private:
		void startTask(const MessageRef& m);
		pplx::task<bool> terminationTask(unsigned int tmMs);
		ActorBaseConfig _config;
		//common::CriticalSection _cs;
		common::SpinLock _latch;
		std::deque<const MessageRef> _mailbox;
		State _state;
		common::string_t _name;
		ActorSystemRefWeak _as;
		MessageHandler _handler;
		pplx::task<void> _currTask;
		bool _terminationSuccess;
		pplx::extensibility::event_t _termEvt;
		pplx::cancellation_token_source _cts;
	};

	class ActorSystem : public std::enable_shared_from_this<ActorSystem>{
		DECLARE_SHARED_REF_OBJ(ActorSystem, const common::string_t& n, const ActorSystemConfig& conf = ActorSystemConfig());
	public:
		void registerActor(ActorRef& actor);
		pplx::task<bool> terminate(unsigned int tmMs = ActorSystemConfig::x_defaultTerminaitonMs);
		pplx::task<bool> onTerminated(unsigned int tmMs = ActorSystemConfig::x_defaultTerminaitonMs) { return terminationTask(tmMs); }
		bool isReady() const { return _state == ready; }
		common::string_t name() const { return _name; }

		virtual ~ActorSystem(){ }
	private:
		enum State{
			none,
			ready,
			terminated
		};
		pplx::task<bool> terminationTask(unsigned int tmMs);

		ActorSystemConfig _config;
		common::CriticalSection _cs;
		common::string_t _name;
		std::vector<ActorRef> _actors;
		bool _terminationSuccess;
		State _state;
		pplx::extensibility::event_t _termEvt;

	};
}

#define MAKE_MESSAGE(message,sender,...) std::make_shared<const message>(actor::ActorRefWeak(sender), __VA_ARGS__)
#define MAKE_MESSAGE_NOSENDER(message,...) std::make_shared<const message>(actor::ActorRefWeak(), __VA_ARGS__)
#define CAST_MESSAGE(message,p) static_cast<const message*>(p.get());
#endif // __ACTOR_H__

