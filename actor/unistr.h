#ifndef __UNISTR_H__
#define __UNISTR_H__
#include <string>
#include <cstdio>
#include <sstream>
namespace actor
{
#ifdef WIN32
	//
	// On Windows, all strings are wide
	//
#ifndef _XPLATSTR
#define _XPLATSTR(x) L ## x
#endif
	typedef wchar_t char_t;
	typedef std::wstring string_t;
	typedef std::wostringstream ocommon::stringstream_t;
	typedef std::wofstream ofstream_t;
	typedef std::wostream ostream_t;
	typedef std::wistream istream_t;
	typedef std::wifstream ifstream_t;
	typedef std::wistringstream icommon::stringstream_t;
	typedef std::wstringstream common::stringstream_t;
#define xcout std::wcout
#define xcin std::wcin
#define xcerr std::wcerr
#define sprintf_t swprintf
#else
	//
	// On POSIX platforms, all strings are narrow
	//
#ifndef _XPLATSTR
#define _XPLATSTR(x) x
#endif
	typedef char char_t;
	typedef std::string string_t;
	typedef std::ostringstream ocommon::stringstream_t;
	typedef std::ofstream ofstream_t;
	typedef std::ostream ostream_t;
	typedef std::istream istream_t;
	typedef std::ifstream ifstream_t;
	typedef std::istringstream icommon::stringstream_t;
	typedef std::stringstream common::stringstream_t;
#define xcout std::cout
#define xcin std::cin
#define xcerr std::cerr
#define sprintf_t snprintf
#endif // endif WIN32

#ifndef U
#define U(x) _XPLATSTR(x)
#endif
}
#endif //__UNISTR_H__