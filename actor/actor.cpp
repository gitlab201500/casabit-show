#include <algorithm>
#include "actor.h"
#include "commonutil.h"

namespace actor{
	DEFINE_LOGGER(Actor, INFO);
	DEFINE_LOGGER(ActorSystem, INFO);
	void ActorBase::initActorBase(const common::string_t& n, actor::ActorSystemRefWeak as, const ActorBaseConfig& conf){
		if (n.empty()) throw std::exception("Actor name is empty");
		auto ass = as.lock();
		if (ass == nullptr) throw std::exception("ActorSystem is empty");

		_name = n; _as = ass;
		_config = conf;
		auto thisPtr = shared_from_this();
		_handler = [](const MessageRef& m){};
		_terminationSuccess = false;
		_currTask = pplx::task_from_result();
		_state = ready;
		ass->registerActor(thisPtr);
	}
	void ActorBase::startTask(const MessageRef& m){

		if (!isReadyToRun()) return;

		logDebug(logActor, U("Start new task"));
		auto thisPtr = shared_from_this();
		_currTask=pplx::create_task([m, thisPtr](){
			if (thisPtr->isReadyToRun()){
				logDebug(logActor,U("inside user task"));
				// let uncaught exception propagate. Catch it later in then block
				thisPtr->handler()(m);
				logDebug(logActor,U("finished user task"));
			}
		}, _cts.get_token()).then([thisPtr](pplx::task<void>& t){
			try{
				t.get();
				if (!thisPtr->isReadyToRun()) return ;
				//common::AutoCs lock(thisPtr->_cs);
				common::AutoSpinLock lock(thisPtr->_latch);
				if (!thisPtr->isReadyToRun()) return ;

				if (thisPtr->_mailbox.size() == 0){
					thisPtr->_state = ready;
					logDebug(logActor,U("No more messages, task is set to not running"));
				}
				else {
					const MessageRef m = thisPtr->_mailbox.front();
					thisPtr->_mailbox.pop_front();
					logDebug(logActor,U("Process the next message"));
					thisPtr->startTask(m);
				}
			}
			catch (const pplx::task_canceled)
			{
				logDebug(logActor,U("Task is cancelled"));
			}
			catch (const std::exception&e){
				common::stringstream_t ss;
				ss << U("Exception in task: ") << common::s2ws(e.what());
				logError(logActor,ss.str());
				auto as = thisPtr->system().lock();
				if (as){
					logInfo(logActor,U("Teminate ActorSystem"));
					as->terminate();
				}
			}
			catch (...){
				logError(logActor,U("Unexpected exception in task"));
				auto as = thisPtr->system().lock();
				if (as){
					logInfo(logActor,U("Teminate ActorSystem"));
					as->terminate();
				}
			}

		},_cts.get_token());
	}

	pplx::task<bool> ActorBase::terminationTask(unsigned int tmMs){
		auto thisPtr = shared_from_this();
		return pplx::create_task([thisPtr, tmMs](){
			thisPtr->_termEvt.wait(tmMs);
			return thisPtr->_terminationSuccess;
		});
	}

	void ActorBase::tell(const MessageRef&m){

		if (!isReadyToRun()) return;
		common::AutoSpinLock lock(_latch);
		//common::AutoCs lock(_cs);
		if (!isReadyToRun()) return;
		if (_mailbox.size() < _config._mailboxSz){
			_mailbox.push_back(m);
		}
		else {
			common::stringstream_t ss;
			auto sender = m->sender();
			ss << U("Dropped message from actor ") << (sender != nullptr ? sender->name() : U("None"));
			logWarn(logActor,ss.str());
		}

		if (_state==ready){
			_state=running;
			const MessageRef m = _mailbox.front();
			_mailbox.pop_front();
			startTask(m);
		}
	}

	pplx::task<bool> ActorBase::terminate(unsigned int tmMs) {
		common::stringstream_t ss;
		ss << U("Terminate called: ") << _name;
		logDebug(logActor,ss.str());

		if (_state == terminated) { return terminationTask(tmMs); }
		//common::AutoCs lock(_cs);
		common::AutoSpinLock lock(_latch);
		if (_state == terminated) { return terminationTask(tmMs); }
		_state = terminated;
		_cts.cancel();

		auto thisPtr = shared_from_this();
		pplx::create_task([thisPtr](){
			// Wait for task completion. Do not care if task succeeded. Just completed.
			thisPtr->_currTask.wait();
			common::stringstream_t ss;
			ss << U("Curr task completed ");
			logDebug(logActor,ss.str());
			thisPtr->_terminationSuccess = true;
			thisPtr->_termEvt.set();
		});

		return terminationTask(tmMs);;

	}

	DEFINE_SHARED_REF_OBJ(ActorSystem, const common::string_t& n, const ActorSystemConfig& conf){
		auto p = MAKE_SHARED(ActorSystem);
		p->_name = n;
		p->_config = conf;
		p->_state = ready;
		p->_terminationSuccess = false;

		return p;
	}

	void ActorSystem::registerActor(ActorRef& actor){
		if (!isReady()) throw std::exception("Cannot register actor: System is not ready");
		if (!actor->isReady()) throw std::exception("Cannot register actor: Actor is not ready");
		common::AutoCs lock(_cs);
		for (auto a : _actors) { if (a->name() == actor->name()) throw std::exception("Actor with such name already exists."); }
		_actors.push_back(actor);
	}


	pplx::task<bool> ActorSystem::terminationTask(unsigned int tmMs){
		auto thisPtr = shared_from_this();
		return pplx::create_task([thisPtr, tmMs](){
			thisPtr->_termEvt.wait(tmMs);
			return thisPtr->_terminationSuccess;
		});
	}

	pplx::task<bool> ActorSystem::terminate(unsigned int tmMs){

		common::stringstream_t ss;
		ss << U("Terminate called: ") << _name;
		logDebug(logActorSystem,ss.str());

		if (_state==terminated) {return terminationTask(tmMs);}
		common::AutoCs lock(_cs);
		if (_state == terminated) { return terminationTask(tmMs); }
		_state = terminated;

		auto thisPtr = shared_from_this();
		pplx::create_task([thisPtr, tmMs](){
			std::vector<pplx::task<bool>> tasks;
			for (auto a : thisPtr->_actors){
				tasks.push_back(a->terminate(tmMs));
			}

			common::stringstream_t ss;
			for (int i = 0; i < tasks.size();i++){
				auto tt = tasks[i];
				if (!tt.get()){
					ss << thisPtr->_actors[i]->name() << U(", ");
				}
			}

			thisPtr->_actors.clear();
			bool res = false;
			if (!ss.str().empty()){
				common::stringstream_t ss1;
				ss1 << U("Failed to terminate ActorSystem: ") << thisPtr->name() << U(" error: ") << ss.str();
				logError(logActorSystem,ss.str());
			}
			else {
				common::stringstream_t ss;
				ss << U("Succesfully terminated ActorSystem: ") << thisPtr->_name;
				logDebug(logActorSystem,ss.str());

				res = true;
			}
			return res;
		}).then([thisPtr](pplx::task<bool>&t){
			try{ thisPtr->_terminationSuccess = t.get();  }
			catch (...){
				common::stringstream_t ss;
				ss << U("Failed to terminate ActorSystem: ") << thisPtr->name() << U(" unexpected exception. ");
				logError(logActorSystem,ss.str());
			}
			logDebug(logActorSystem, U("ActorSystem::terminate: Set terminaiton event"));
			thisPtr->_termEvt.set();
		});

		return terminationTask(tmMs); 
	}
}