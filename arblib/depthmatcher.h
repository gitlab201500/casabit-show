#ifndef __DEPTHMATCHER__
#define __DEPTHMATCHER__

#include <tuple>
#include <deque>
#include "arbdef.h"
#include "exchutil.h"
#include "tradeutil.h"
#include "commonutil.h"
#include "exchentity.h"
#include "logger.h"
#include "arbtrade.h"
#include "tradeentity.h"


using namespace common;
using namespace exchangelib;
using namespace tradelib;
namespace arblib{
	DECLARE_LOGGER(DepthMatcher);

	const double x_maxTradeBtc = 2.;
	const double x_profitThreshold = 1.005;
	const double x_profitIncThreshold = (x_profitThreshold - 1) / 2;
	const double x_minTradeBtc = 0.005;
	const double x_volumeMinLimit = 0.01;

	typedef std::shared_ptr<std::deque<DepthPairBid>> DepthPairBidSeqRef;
	typedef std::shared_ptr<std::deque<DepthPairAsk>> DepthPairAskSeqRef;


	struct TradeBalance{
		TradeBalance(double c, double bs, double bt) :currency(c), base(bs), btc(bt) {}
		double currency;
		double base;
		double btc;
		bool isValid() const { return currency >= 0.0 && base >= 0.0 && btc >= 0.0; }
		string_t toString() { stringstream_t ss; ss << U(" currency: ") << currency << U(" base: ") << base << U(" btc: ") << btc; return ss.str(); }
	};

	struct TradeState{
		TradeState(DepthPairBidSeqRef b1, DepthPairAskSeqRef a2, TradeBalance balance, double profXVol, double vol,
			__inout std::vector<TradeOrder>& orders1, __inout std::vector<TradeOrder>& orders2) :
			_b1(b1), _a2(a2), _balance(balance), _profXVol(profXVol), _vol(vol), _orders1(orders1), _orders2(orders2){}
		DepthPairBidSeqRef _b1;
		DepthPairAskSeqRef _a2;
		TradeBalance _balance;
		double _profXVol;
		double _vol;
		std::vector<TradeOrder>& _orders1;
		std::vector<TradeOrder>& _orders2;
		bool isValid(){
			return
				!_b1->empty() && !_a2->empty() &&
				_b1->at(0).q > _a2->at(0).q &&
				_balance.isValid() &&
				_b1->at(0).q > 0.0 && _a2->at(0).q > 0.0 &&
				_b1->at(0).v > 0.0 && _a2->at(0).v > 0.0;
		}
	};

	struct DepthMatcherCfg{
		DepthMatcherCfg() :isTest(false), quoteShift(0.){}
		bool isTest;
		double quoteShift;
	};
	class DepthMatcher{
	public:
		DepthMatcher(Pair pair,
			Exchange e1, const DepthRef&d1, EpochMs epoch1, const std::map<Symbol, BalanceRef>& bal1,
			Exchange e2, const DepthRef&d2, EpochMs epoch2, const std::map<Symbol, BalanceRef>& bal2,
			const DepthMatcherCfg&cfg = DepthMatcherCfg());
		OrderCollectionRef orders();
	private:

		typedef std::tuple<Exchange, DepthPairBidSeqRef, double, const std::map<Symbol, BalanceRef>*, EpochMs> ELeft;
		typedef std::tuple<Exchange, DepthPairAskSeqRef, double, const std::map<Symbol, BalanceRef>*, EpochMs> ERight;
		typedef std::pair<ELeft, ERight>LRPair;

		TradeState matchDepth(TradeState ts);

		Pair _pair;
		Exchange _e1;
		DepthRef _d1;
		DepthPairBidSeqRef _b1;
		DepthPairAskSeqRef _a1;
		const std::map<Symbol, BalanceRef>& _bal1;
		Exchange _e2;
		DepthRef _d2;
		DepthPairBidSeqRef _b2;
		DepthPairAskSeqRef _a2;
		const std::map<Symbol, BalanceRef>& _bal2;
		const XactFeesPct _fee1;
		const XactFeesPct _fee2;
		double _feeFactor;
		ELeft _eLeft;
		ERight _eRight;
		OrderCollectionRef _orderColl;
		const DepthMatcherCfg _cfg;
	};
}


#endif //__DEPTHMATCHER__