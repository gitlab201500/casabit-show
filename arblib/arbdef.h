
#ifndef __ARBDEF_H__
#define __ARBDEF_H__
#include <chrono>
#include <unistr.h>
namespace arblib{
	const int x_printFreqDefault = 200;
	const auto x_delayDepthDefault = std::chrono::milliseconds(120);
	const auto x_delayBalanceDefault = std::chrono::milliseconds(1000);
	const common::string_t x_apiKeyFile = U("apikeys.json");
}
#endif //__ARBDEF_H__
