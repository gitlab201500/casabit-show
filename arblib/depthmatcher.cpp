#include <algorithm>
#include "exchanges.h"
#include "depthmatcher.h"

namespace arblib{
	DEFINE_LOGGER(DepthMatcher, DEBUG);

	DepthMatcher::DepthMatcher(Pair pair,
		Exchange e1, const DepthRef&d1, EpochMs epoch1, const std::map<Symbol, BalanceRef>& bal1,
		Exchange e2, const DepthRef&d2, EpochMs epoch2, const std::map<Symbol, BalanceRef>& bal2,
		const DepthMatcherCfg& cfg) :
		_pair(pair), _e1(e1), _d1(d1), _e2(e2), _d2(d2),
		_bal1(bal1), _bal2(bal2),
		_fee1(exchangelib::x_exchangeCollection.find(exchtos(e1))->second->constants()->fee()),
		_fee2(exchangelib::x_exchangeCollection.find(exchtos(e2))->second->constants()->fee()),
		_feeFactor(0.), _cfg(cfg)
	{
		_b1 = std::make_shared < std::deque<DepthPairBid >>(d1->info.bids.cbegin(), d1->info.bids.cend());
		_a1 = std::make_shared < std::deque<DepthPairAsk >>(d1->info.asks.cbegin(), d1->info.asks.cend());
		_b2 = std::make_shared < std::deque<DepthPairBid >>(d2->info.bids.cbegin(), d2->info.bids.cend());
		_a2 = std::make_shared < std::deque<DepthPairAsk >>(d2->info.asks.cbegin(), d2->info.asks.cend());

		// sort quotes
		// bids - descending
		std::sort(_b1->begin(), _b1->end(), [](const DepthPairBid& b1, const DepthPairBid& b2){
			return b2 < b1;
		});
		std::sort(_b2->begin(), _b2->end(), [](const DepthPairBid& b1, const DepthPairBid& b2){
			return b2 < b1;
		});
		// asks - ascending
		std::sort(_a1->begin(), _a1->end());
		std::sort(_a2->begin(), _a2->end());

		// left and right
		// left is lBid > rAsk

		std::tie(_eLeft, _eRight) = (_a2->empty() ||
			(!_b1->empty() && (_b1->at(0).q>_a2->at(0).q))) ?
			LRPair(ELeft(_e1, _b1, _fee1.take, &_bal1, epoch1), ERight(_e2, _a2, _fee2.take, &_bal2, epoch2)) :
			LRPair(ELeft(_e2, _b2, _fee2.take, &_bal2, epoch2), ERight(_e1, _a1, _fee1.take, &_bal1, epoch1));

		if (_cfg.isTest){
			if (_cfg.quoteShift >= 100.) throw std::exception("Value for quoteshift must be less than 100.");
			auto& as = std::get<1>(_eRight);
			for (auto i = as->begin(); i != as->end(); ++i){
				i->q *= 1. - _cfg.quoteShift / 100.;
			}
		}

		_feeFactor = (1 - std::get<2>(_eLeft) / 100.)*(1 - std::get<2>(_eRight) / 100.);
		auto cPair = pairToSymbols(_pair);
		assert(cPair.size() == 2);
		assert(cPair[1] == BTC);
		assert(std::get<3>(_eLeft)->find(cPair[0]) != std::get<3>(_eLeft)->cend());
		assert(std::get<3>(_eLeft)->find(cPair[1]) != std::get<3>(_eLeft)->cend());
		assert(std::get<3>(_eRight)->find(cPair[0]) != std::get<3>(_eRight)->cend());
		assert(std::get<3>(_eRight)->find(cPair[1]) != std::get<3>(_eRight)->cend());

		double curr = std::get<3>(_eLeft)->find(cPair[0])->second->balance;
		double base = std::get<3>(_eRight)->find(cPair[1])->second->balance;

		// adjust balance for 2xfee
		TradeBalance tradeBal(curr*(1 - 2 * std::get<2>(_eLeft) / 100.),
			base*(1 - 2 * std::get<2>(_eRight) / 100.),
			x_maxTradeBtc
			);

		_orderColl = std::make_shared<OrderCollection>(_pair, std::get<0>(_eLeft), std::get<0>(_eRight));
		TradeState ts(std::get<1>(_eLeft), std::get<1>(_eRight), tradeBal, 0., 0., _orderColl->orders1, _orderColl->orders2);
		auto tsOut = matchDepth(ts);
		if (_orderColl->orders1.empty() || _orderColl->orders2.empty()) { _orderColl = nullptr; }
		else if ((x_maxTradeBtc - tsOut._balance.btc) > x_minTradeBtc){
			double profit1 = tsOut._vol > 0 ? tsOut._profXVol / tsOut._vol : 1.0; // >=1.0
			double profitpct = (profit1 - 1.0) * 100;
			double profitBtc = (1 - tsOut._balance.btc / x_maxTradeBtc)*profit1*x_maxTradeBtc;
			logInfo(logDepthMatcher, U("FoundTrade: ") << U(" profpct: ") << profitpct <<
				U(" profBtc: ") << profitBtc <<
				U(" RemBtc: ") << tsOut._balance.btc << U(" v: ") << tsOut._vol <<
				U("RT times: ") << std::get<4>(_eLeft) << U(" ") << std::get<4>(_eRight) << U(" ") << abs(std::get<4>(_eLeft)-std::get<4>(_eRight)) <<
				_orderColl->toString());
		}
		else{
			logDebug(logDepthMatcher, U("Total trade is too small (BTC): ") << x_maxTradeBtc - tsOut._balance.btc);
			_orderColl = nullptr;
		}
	}

	OrderCollectionRef DepthMatcher::orders(){
		return _orderColl;
	}

	TradeState DepthMatcher::matchDepth(TradeState in){
		auto e1 = std::get<0>(_eLeft);
		auto e2 = std::get<0>(_eRight);

		while (true){
			if (!in.isValid()) { return in; }
			//logDebug(logDepthMatcher, U("Match depth: ") << U(" pair: ") << pairtos(_pair)<<
			//	U(" b1: ") << in._b1->at(0).q << U(" a1: ") << in._a2->at(0).q
			//	<< U(" exchange: ") << exchtos(std::get<0>(_eLeft)) << U(" ") << exchtos(std::get<0>(_eRight))
			//	);

			auto askq = in._a2->at(0).q;
			if (askq <= 0.0) return in;

			std::vector<double> vols = { in._a2->at(0).v, in._b1->at(0).v, in._balance.currency, in._balance.base / askq };
			std::sort(vols.begin(), vols.end());
			auto v1 = vols.at(0);
			if (v1 <= 0.0) return in;

			auto bidbal = in._balance.currency - v1;
			auto askbal = in._balance.base - v1*askq;
			auto btcbal = in._balance.btc - v1*askq;

			auto newTBal = TradeBalance(bidbal, askbal, btcbal);
			if (!newTBal.isValid()) {
				logDebug(logDepthMatcher, U("Insufficient funds to initiate trade: ") << newTBal.toString() << U(" pair: ") << pairtos(_pair)
					<< U(" exchange: ") << exchtos(e1) << U(" ") << exchtos(e2));
				return in;
			}

			auto profitDelta = in._b1->at(0).q / in._a2->at(0).q * _feeFactor - 1.0;
			if (profitDelta < x_profitIncThreshold){
				logDebug(logDepthMatcher, U("Profit increment is too low: ") << profitDelta <<
					U("  incthresh: ") << x_profitIncThreshold << U(" pair: ") << pairtos(_pair)
					<< U(" exchange: ") << exchtos(e1) << U(" ") << exchtos(e2));
				return in;
			}
			auto profXVol = in._profXVol + (profitDelta + 1) * v1; // profit*volume cumulative
			auto volume = in._vol + v1; // volume cumulative
			auto profit = profXVol / volume; // profit cumulative
			logDebug(logDepthMatcher, U("profXVol: " << profXVol << U(" volume: ") << volume << U(" profit : ") << profit));
			if (profit < x_profitThreshold){
				logDebug(logDepthMatcher, U("Profit is below threshold: ") << profit << U(" thresh: ") << x_profitThreshold << U("  pair: ") << pairtos(_pair)
					<< U(" exchange: ") << exchtos(e1) << U(" ") << exchtos(e2));
				return in;
			}
			if (v1 > x_volumeMinLimit){
				in._orders1.push_back(TradeOrder(_pair, e1, false, in._b1->at(0).q, v1));
				in._orders2.push_back(TradeOrder(_pair, e2, true, in._a2->at(0).q, v1));
			}
			else{
				logDebug(logDepthMatcher, U("Skip small valume trade: ") << v1 << U(" min vol: ") << x_volumeMinLimit << U("  pair: ") << pairtos(_pair)
					<< U(" exchange: ") << exchtos(e1) << U(" ") << exchtos(e2));

			}

			if (in._a2->at(0).v>v1){
				in._a2->at(0).v -= v1;
			}
			else{
				in._a2->pop_front();
			}

			if (in._b1->at(0).v > v1){
				in._b1->at(0).v -= v1;
			}
			else{
				in._b1->pop_front();
			}
			in._balance = newTBal;
			in._profXVol = profXVol;
			in._vol = volume;
		}
	}

}