
#ifndef __ARBTRADE_H__
#define __ARBTRADE_H__
#include "exchdef.h"



using namespace common;
using namespace exchangelib;
using namespace tradelib;
namespace arblib{
	struct TradeOrder{
		TradeOrder(Pair p, Exchange e, bool ib, double pr, double v) :pair(p), exch(e), isBuy(ib), price(pr), vol(v){}
		const Pair pair;
		const Exchange exch;
		const bool isBuy;
		const double price;
		const double vol;
		string_t toString(){
			stringstream_t ss; ss << U(" e: ") << exchtos(exch)
				<< U(" buy: ") << isBuy << U(" p: ") << price << U(" v: ") << vol; return ss.str();
		}
	};

	struct OrderCollection{
		OrderCollection(Pair p, Exchange e1, Exchange e2) :pair(p), exch1(e1), exch2(e2){}
		const Pair pair;
		const Exchange exch1;
		const Exchange exch2;
		std::vector<TradeOrder> orders1;
		std::vector<TradeOrder> orders2;
		string_t toString(){
			size_t num = 3;
			stringstream_t ss;
			ss << U(" pair: ") << pair;
			for (size_t i = 0; i < (num<orders1.size() ? num : orders1.size()); i++){
				auto o = orders1[i];
				ss << o.toString();
			}
			for (size_t i = 0; i < (num<orders2.size() ? num : orders2.size()); i++){
				auto o = orders2[i];
				ss << o.toString();
			}
			return ss.str();
		}
	};

	typedef std::shared_ptr<OrderCollection> OrderCollectionRef;
}
#endif //__ARBTRADE_H__