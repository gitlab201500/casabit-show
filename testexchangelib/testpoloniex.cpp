// testactor.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "commondefs.h"
#include <vector>
#include <chrono>
#include <typeinfo>
#include <thread>
#include <algorithm>
#include <map>
#include <chrono>
#include <iostream>
#include <functional>
#include "testcommon.h"
#include "exchanges.h"
#include "testtemplates.h"

const common::string_t exchange = U("poloniex");
static common::Logger::Level logLevel = common::Logger::Level::INFO;
std::map<std::string, TestFunc> g_poloniexTests = getTestTemplates(exchange, logLevel);