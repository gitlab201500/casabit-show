// testactor.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "commondefs.h"
#include <vector>
#include <chrono>
#include <typeinfo>
#include <thread>
#include <algorithm>
#include <map>
#include <chrono>
#include <iostream>
#include <functional>
#include "testcommon.h"
#include "exchanges.h"
#include "testtemplates.h"

inline std::string testName(const std::string& t)
{
	return __FILENAME__ + std::string("::") + t;
}

using namespace exchangelib;
using namespace common;

typedef pplx::task<exchangelib::DepthRef> DepthRefTask;

std::map<std::string, TestFunc> getTestTemplates(const common::string_t& exchange, common::Logger::Level logLevel){
	return {
		std::pair<std::string, TestFunc>(testName("test1"), TestFunc([exchange,logLevel]()
		{
			std::cout << "test1: Query depth for exchange " << common::ws2s(exchange)<<std::endl;

			Pair pair=ETH_BTC;
			size_t N = 10;
			Stats<long long> stats1;
			Stats<long long> stats2;
			auto fact = exchangelib::x_exchangeCollection.at(exchange)->depthFactory();
			for (size_t i = 0; i < N; i++){
				auto d = fact(pair, DepthCfg(logLevel));
				assert(d);
				auto r = exchangelib::Try(std::function<DepthRef()>([d](){
					return d->fetch().get();
				}));
				auto end = std::chrono::system_clock::now();
				auto rt = std::chrono::duration_cast<std::chrono::milliseconds>(end - d->start()).count();
				stats1 = stats1 + rt;
				stats2 = stats2 + d->rtMs();
				if (r.isFailure()) std::cout << "exception: " << r.failure().what() << std::endl;
				TSTASSERT(r.isSuccess(), "Failed to fetch depth");
				TSTASSERT(r.success()->pair == pair, "Invalid pair");
				TSTASSERT(r.success()->info.asks.size() > 0, "Invalid asks size");
				TSTASSERT(r.success()->info.bids.size() > 0, "Invalid bids size");
			}

			std::cout << "Events: " << N << std::endl;;
			std::cout << " RT time (ms). Average: " << stats1.mean() << " dev: " << stats1.dev() << " min:" << stats1.min() << " max: " << stats1.max() << " successes:" << stats1.count() << std::endl;
			std::cout << " RT time (ms). Average: " << stats2.mean() << " dev: " << stats2.dev() << " min:" << stats2.min() << " max: " << stats2.max() << " successes:" << stats2.count() << std::endl;

			return true;
		}
		)),

			std::pair<std::string, TestFunc>(testName("test2"), TestFunc([exchange,logLevel]()
		{
			std::cout << "test2: Measure roundtrip time for depth for exchange " << common::ws2s(exchange) << std::endl;
			int nevents = 30;
			Pair pair=ETH_BTC;
			auto level = logLevel;
			auto fact = exchangelib::x_exchangeCollection.at(exchange)->depthFactory();
			assert(fact);
			auto f = [pair, level, fact, logLevel](){
				return fact(pair, DepthCfg(logLevel));
			};
			auto res = testcommon::roundtripTest<DepthRef>(nevents, pair, f);
			auto stats = res.first;
			auto fail = res.second;

			std::cout << "Events: " << nevents << std::endl;
			std::cout << "RT time (ms). Average: " << stats.mean() << " dev: " << stats.dev() << " min:" << stats.min() << " max: " << stats.max() << std::endl;
			std::cout << "Failure rate (%): " << ((double)fail / nevents * 100.0) << std::endl;
			TSTASSERT(((double)fail / nevents * 100.0)  < 10, "Failure rate is too high")

				return true;
		}
		)),

			std::pair<std::string, TestFunc>(testName("test3"), TestFunc([exchange,logLevel]()
		{
			std::cout << "test3: Measure rate for depth for exchange " << common::ws2s(exchange) << std::endl;

			int nevents = 30;
			Pair pair=ETH_BTC;
			auto level = logLevel;

			auto fact = exchangelib::x_exchangeCollection.at(exchange)->depthFactory();
			assert(fact);
			auto f = [pair, level, fact, logLevel](){
				return fact(pair, DepthCfg(logLevel));
			};


			std::vector<std::chrono::milliseconds> delays = {
				std::chrono::milliseconds(125),
				std::chrono::milliseconds(150),
			};
			for (auto delay : delays){
				auto res = testcommon::rateTest<DepthRef>(delay, nevents, pair, f);
				auto stats = res.first;
				auto fail = res.second;

				std::cout << "Events: " << nevents << " delay: " << delay.count() << std::endl;
				std::cout << "RT time (ms). Average: " << stats.mean() << " dev: " << stats.dev() << " min:" << stats.min() << " max: " << stats.max() << std::endl;
				std::cout << "Failure rate (%): " << ((double)fail / nevents * 100.0) << std::endl;
				TSTASSERT(((double)fail / nevents * 100.0)  < 10, "Failure rate is too high")
			}
			return true;
		}
		)),
	};
}