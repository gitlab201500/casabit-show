
#ifndef __COMMONPPLX_H__
#define __COMMONPPLX_H__

#include <agents.h>
#include <chrono>
#include "unistr.h"
#include "pplxtasks.h"
#include "logger.h"

namespace common{
	template <typename Func>
	auto create_delayed_task(std::chrono::milliseconds delay,
		Func func,
		concurrency::cancellation_token token = concurrency::cancellation_token::none()) -> decltype(pplx::create_task(func))
	{
		concurrency::task_completion_event<void> tce;

		auto pTimer = new concurrency::timer<int>(static_cast<int>(delay.count()), 0, NULL, false);
		auto pCallback = new concurrency::call<int>([tce](int) {
			tce.set();
		});

		pTimer->link_target(pCallback);
		pTimer->start();

		return create_task(tce).then([pCallback, pTimer]() {
			delete pCallback;
			delete pTimer;
		}).then(func, token);
	}
	template<class T>
	void taskLoop(std::chrono::milliseconds delay, std::function<T()> f,
		concurrency::cancellation_token token = concurrency::cancellation_token::none()){
		common::create_delayed_task(delay, [f, delay](){
			return f();
		}, token).then([f, delay, token](pplx::task<T>&t){
			auto r = exchangelib::Try(std::function<T()>([t](){
				return t.get();
			}));
			if (r.isSuccess()){
				taskLoop(delay, f, token);
			}
			else {
				common::Logger log(U("takLoop"));
				logError(log, U("Error: ") << common::s2ws(r.failure().what()) << U(" Terminate taskLoop."));
			}
		}, token).then([](pplx::task<void>&t){
			exchangelib::Try(std::function<bool()>([t](){t.get(); return true; }));
		});;
	}
}


#endif //__COMMONPPLX_H__