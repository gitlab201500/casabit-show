
#ifndef __COMMONLOGGER_H__
#define __COMMONLOGGER_H__
#include "unistr.h"
#include <functional>
#include <memory>
#include <mutex>
#include <iostream>
namespace common{
	class Logger{
	public:
		enum Level{
			ERROR=0,
			WARN,
			INFO,
			DEBUG
		};
		Logger(const string_t& n, Level l = INFO) : _name(n),level(l){}
		void flush();
		void debug(const string_t& m);
		void info(const string_t& m);
		void warn(const string_t& m);
		void error(const string_t& m);
		bool doLogging(Level l){ return  l <= level; }
		Level level;
		const string_t name() const { return _name; }
	private:
		string_t _name;
	};
#define logError(__logger__,__body__) if((__logger__).doLogging(common::Logger::Level::ERROR)) {common::stringstream_t __ss__; __ss__<<__body__;__logger__.error(__ss__.str());}
#define logWarn(__logger__,__body__) if((__logger__).doLogging(common::Logger::Level::WARN)) {common::stringstream_t __ss__; __ss__<<__body__;__logger__.warn(__ss__.str());}
#define logInfo(__logger__,__body__) if((__logger__).doLogging(common::Logger::Level::INFO)){common::stringstream_t __ss__; __ss__<<__body__;__logger__.info(__ss__.str());}
#define logDebug(__logger__,__body__) if((__logger__).doLogging(common::Logger::Level::DEBUG)) {common::stringstream_t __ss__; __ss__<<__body__;__logger__.debug(__ss__.str());}

#define DECLARE_LOGGER(name) extern common::Logger log##name
#define DEFINE_LOGGER(name,level) common::Logger log##name(U(#name),common::Logger::level)

}
#endif //__COMMONLOGGER_H__