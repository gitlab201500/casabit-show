#include <deque>
#include <atomic>
#include <thread>
#include "pplxtasks.h"
#include "logger.h"
#include "commonutil.h"

namespace common{

	class LogWriter :public std::enable_shared_from_this<LogWriter>{
	public:
		LogWriter() { _latch = false; _inTask = false; }
		void write(string_t&&m){
			bool unlatched = false;
			while (!_latch.compare_exchange_weak(unlatched, true, std::memory_order_acquire)) {
				unlatched = false;
			}
			_deque.push_back(m);
			_latch.store(false, std::memory_order_release);
			launchWriteTask();
		}
		void flush(){
			bool unlatched = false;
			while (!_latch.compare_exchange_weak(unlatched, true, std::memory_order_acquire)) {
				unlatched = false;
			}
			for (size_t i = 0; i < _deque.size(); i++){
				xcout << _deque.front() << std::endl;
				_deque.pop_front();
			}
			_latch.store(false, std::memory_order_release);
		}
		~LogWriter(){ 
			_cts.cancel(); try{ _task.get(); } catch (...){} 
			flush();
		}
	private:
		void launchWriteTask(){
			bool unlatched = false;
			if (!_inTask.compare_exchange_strong(unlatched, true, std::memory_order_acquire)) {
				return;
			}
			auto thisPtr = shared_from_this();
			_task = pplx::create_task([thisPtr](){
				bool more = true;
				while (more && !pplx::is_task_cancellation_requested()){
					bool unlatched = false;
					while (!thisPtr->_latch.compare_exchange_weak(unlatched, true, std::memory_order_acquire)) {
						unlatched = false;
						std::this_thread::sleep_for(std::chrono::milliseconds(50));
					}
					size_t n = thisPtr->_deque.size() > 100 ? 100 : thisPtr->_deque.size();
					more = thisPtr->_deque.size() > 100;
					std::vector<string_t> tv;
					for (size_t i = 0; i < n; i++){
						tv.push_back(thisPtr->_deque.front());
						thisPtr->_deque.pop_front();
					}
					thisPtr->_latch.store(false, std::memory_order_release);
					for (auto e : tv){
						xcout << e << std::endl;
					}
				}
				thisPtr->_inTask.store(false, std::memory_order_release);

			}, _cts.get_token()).then([thisPtr](pplx::task<void>&t){
				try{ t.get(); }
				catch (...){ thisPtr->_inTask.store(false, std::memory_order_release); }});
		}
		std::atomic_bool _latch;
		std::atomic_bool _inTask;
		std::deque<string_t> _deque;
		pplx::cancellation_token_source _cts;
		pplx::task<void> _task;
	};

	static auto g_logWriter = std::make_shared<LogWriter>();

	void Logger::flush() { g_logWriter->flush(); }
	void Logger::debug(const string_t& m) { stringstream_t ss; ss << common::epochTimeMs()<< U(" DEBUG: ")<< _name <<U(": ")<< m; g_logWriter->write(ss.str()); }
	void Logger::info(const string_t& m) { stringstream_t ss; ss << common::epochTimeMs() << U(" INFO: ") << _name << U(": ") << m; g_logWriter->write(ss.str()); }
	void Logger::warn(const string_t& m) { stringstream_t ss; ss << common::epochTimeMs() << U(" WARN: ") << _name << U(": ") << m; g_logWriter->write(ss.str()); }
	void Logger::error(const string_t& m){ stringstream_t ss; ss << common::epochTimeMs() << U(" ERROR: ") << _name << U(": ") << m; g_logWriter->write(ss.str()); }
}