#include <iostream>
#include <cstdlib>
#include <cstdlib>
#include <sstream>
#include <iomanip>
#include <vector>
#include "commonutil.h"
#ifdef WIN32
#include <codecvt>
#endif


namespace common{
	string_t s2ws(const std::string& str)
	{
#ifdef WIN32
		typedef std::codecvt_utf8<char_t> convert_typeX;
		std::wstring_convert<convert_typeX, char_t> converterX;

		return converterX.from_bytes(str);
#else
		return str;
#endif
	}

	std::string ws2s(const string_t& wstr)
	{
#ifdef WIN32
		typedef std::codecvt_utf8<char_t> convert_typeX;
		std::wstring_convert<convert_typeX, char_t> converterX;

		return converterX.to_bytes(wstr);
#else
		return wstr;
#endif
	}

	std::vector<string_t> split(const string_t &text, char_t sep)
	{
		std::vector<string_t> tokens;
		size_t start = 0, end = 0;
		while ((end = text.find(sep, start)) != string_t::npos) {
			tokens.push_back(text.substr(start, end - start));
			start = end + 1;
		}
		tokens.push_back(text.substr(start));
		return tokens;
	}
}
