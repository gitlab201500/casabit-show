
#ifndef __COMMONUTIL_H__
#define __COMMONUTIL_H__
#include "unistr.h"
#include <functional>
#include <memory>
#include <mutex>
#include <vector>
#include <atomic>
#include <chrono>

namespace common{
	string_t s2ws(const std::string& str);
	std::string ws2s(const string_t& wstr);
	std::vector<string_t> split(const string_t &text, char_t sep);

	template <class R>
	class TryResult{
		std::shared_ptr<R> _r;
		std::shared_ptr<std::exception> _f;
		bool _s;
	public:
		TryResult() :_s(true){}
		TryResult(R r) :_r((std::make_shared<R>(r))), _s(true){}
		TryResult(std::exception& e) :_f(std::make_shared<std::exception>(e)), _s(false){}
		bool isSuccess() const { return _s; }
		bool isFailure() const { return !_s; }
		R success() const{ return *_r; }
		std::exception failure() const{ return *_f; }
	};

	template <class R>
	TryResult<R> Try(std::function<R()>& foo){
		try{
			return TryResult<R>(foo());
		}
		catch (const std::exception&e){
			return TryResult<R>(std::exception(e.what()));
		}
	}

	typedef	std::recursive_mutex CriticalSection;
	using AutoCs = std::unique_lock<CriticalSection>;

	template <class T>
	class Stats{
	public:
		Stats() :_sum(0), _sum2(0), _count(0), _min(std::numeric_limits<T>::max()), _max(std::numeric_limits<T>::min()),_fail(0){}
		Stats& operator+(T v){ _sum += v; _sum2 += v*v; _count++; if (v < _min) _min = v;  if (v > _max) _max = v; return *this; }
		T mean(){ return _count ? (T)(_sum / _count):0; }
		T dev(){ return _count ? (T)sqrt((_sum2 - _sum*_sum / _count) / _count):0; }
		T min(){ return _min; }
		T max(){ return _max; }
		size_t count(){ return _count; }
		void addFail(){ _fail++; }
		size_t fail(){ return _fail; }
		string_t toString(){
			stringstream_t ss; ss << U("Count: ") << count() << U(" mean: ")
				<< mean() << U(" dev: ") << dev() << U(" min: ") << min() << U(" max: ") << max() << U(" fail: ") << fail(); 
			return ss.str(); 
		}
	private:
		T _sum;
		T _sum2;
		T _max;
		T _min;
		size_t _count;
		size_t _fail;
	};

	inline double round(double in, double p){
		return ((int)(in * p)) / p;
	}

	typedef  long long EpochMs;
	typedef  long long EpochDeltaMs;
	inline EpochMs epochTimeMs(){
		return std::chrono::duration_cast<std::chrono::milliseconds>
			(std::chrono::system_clock::now().time_since_epoch()).count();
	}

	class SpinLock{
	public:
		SpinLock(){ _latch = false; }
		void lock(){
			bool unlatched = false;
			while (!_latch.compare_exchange_weak(unlatched, true, std::memory_order_acquire)) {
				unlatched = false;
			}
		}
		void unlock(){
			_latch.store(false, std::memory_order_release);
		}
		
	private:
		std::atomic<bool> _latch;
	};

	using AutoSpinLock = std::unique_lock<SpinLock>;

}
#endif //__COMMONUTIL_H__