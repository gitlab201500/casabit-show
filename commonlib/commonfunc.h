#ifndef __COMMONFUNC_H__
#define __COMMONFUNC_H__
#include <map>
namespace common{
	template<class K, class V >
	inline std::map<K, V> invertMap(const std::map<V, K>& m){
		std::map<K, V> inv;
		std::transform(m.begin(), m.end(), std::inserter(inv, inv.end()), [](const std::pair<V, K>&p){
			return std::pair<K, V>(p.second, p.first);
		});
		return inv;
	}
}

#endif //__COMMONFUNC_H__