#ifndef __COMMONDEFS_H__
#define __COMMONDEFS_H__
#define DISABLE_COPY_ASSIGN(name)\
	name(const name&) = delete; \
	name& operator=(const name&) = delete;

#define DECLARE_SHARED_REF_OBJ(name, ...) \
	DISABLE_COPY_ASSIGN(name)\
	private: \
struct this_is_private {}; \
	name(); \
	public: \
	explicit name(const this_is_private &){} \
	static std::shared_ptr<name> create##name(__VA_ARGS__); \
	private:
#define DEFINE_SHARED_REF_OBJ(name, ...) \
	std::shared_ptr<name> name::create##name(__VA_ARGS__)

#define MAKE_SHARED(name) \
	std::make_shared<name>(this_is_private())

#ifndef WIN32
#define UNREFERENCED_PARAMETER(x) (x);
#ifndef __in
#define __in
#endif
#ifndef __out
#define __out
#endif
#ifndef __inout
#define __inout
#endif
#endif

#ifndef  ARRAYLEN
#define ARRAYLEN(x) sizeof(x)/sizeof((x)[0])
#endif

#include "unistr.h"
#endif // __COMMONDEFS_H__