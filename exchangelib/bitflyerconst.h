#ifndef __BITFLYERCONST_H__
#define __BITFLYERCONST_H__
#include <memory>
#include "exchconst.h"
#include "exchentity.h"

namespace exchangelib{
	namespace bitflyer{
		class BitflyerConstants :public ExchangeConstants{
		public:
			BitflyerConstants();
			const common::string_t& name() const { return _name; }
			const PairMap& pairs() const { return _pairs; }
			const PairMapInv& pairsInv() const { return _pairsInv; }
			const SymbolMap& symbols()  const{ return _symbols; }
			const SymbolMapInv& symbolsInv() const { return _symbolsInv; }
			const XactFeesPct& fee() const{ return _fee; }

		private:
			const common::string_t _name;
			PairMap _pairs;
			PairMapInv _pairsInv;
			SymbolMap _symbols;
			SymbolMapInv _symbolsInv;
			const XactFeesPct _fee;
		};

		extern BitflyerConstants x_bitflyerConstants;
	}
}

#endif //__BITFLYERCONST_H__