#include <http_client.h>
#include <string>
#include "commonutil.h"
#include "exchutil.h"
#include "poloniexdepth.h"


namespace exchangelib
{
	namespace poloniex{

		DepthRef DepthQuery::parseJson(const web::json::value& json){
			auto ret = std::make_shared<Depth>(_pair, DepthInfo({}, {}));
			auto o = GETASORELSE(json, object, throw std::exception("Invalid json: depth response is not an object"));
			auto asks = GETFIELDORELSE(o, asks, array, throw std::exception("Invalid json: depth response contains no asks field"));
			std::vector<DepthPairAsk>& vasks = ret->info.asks;
			for (auto a : asks){
				auto i = GETASORELSE(a, array, throw std::exception("Invalid json: asks field does not contain askpair"));
				if (i.size() != 2) throw std::exception("Invalid json: askspair contains no price field");
				vasks.push_back(DepthPairAsk(
					std::stold(GETASORELSE(i[0], string, throw std::exception("Invalid json: asks field does not contain price"))),
					GETASORELSE(i[1], double, GETASORELSE(i[1], integer, throw std::exception("Invalid json: asks field does not contain vol")))
					));
			}

			auto bids = GETFIELDORELSE(o, bids, array, throw std::exception("Invalid json: depth response contains no bids field"));
			std::vector<DepthPairBid>& vbids = ret->info.bids;
			for (auto b : bids){
				auto i = GETASORELSE(b, array, throw std::exception("Invalid json: bids field does not contain bidpair"));
				if (i.size() != 2) throw std::exception("Invalid json: bidpair contains no price field");
				vbids.push_back(DepthPairBid(
					std::stold(GETASORELSE(i[0], string, throw std::exception("Invalid json: bids field does not contain price"))),
					GETASORELSE(i[1], double, GETASORELSE(i[1], integer, throw std::exception("Invalid json: bids field does not contain vol")))
					));
			}

			return ret;
		}
	}

}