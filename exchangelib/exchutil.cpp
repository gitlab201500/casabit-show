
#include "exchutil.h"
namespace exchangelib{
	web::json::value extractJson(web::http::http_response& response)
	{
		web::json::value val;
		try{ val = response.extract_json().get(); }
		catch (...){ val = web::json::value::string(U("error extracting json")); }
		return val;
	}
}