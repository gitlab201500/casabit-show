#include "commonfunc.h"
#include "poloniexconst.h"

namespace exchangelib{
	namespace poloniex{
		PoloniexConstants::PoloniexConstants() :
			_name(U("poloniex")),
			_fee(XactFeesPct(0.15, 0.25))
		{
			_pairs[ETH_BTC] = U("BTC_ETH");
			_pairs[LTC_BTC] = U("BTC_LTC");
			_pairs[XRP_BTC] = U("BTC_XRP");
			_pairsInv = common::invertMap(_pairs);
			_symbols[BTC] = U("BTC");
			_symbols[ETH] = U("ETH");
			_symbols[LTC] = U("LTC");
			_symbols[XRP] = U("XRP");
			_symbolsInv = common::invertMap(_symbols);
		}
		PoloniexConstants x_poloniexConstants;
	}
}