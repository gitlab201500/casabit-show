#ifndef __EXCHANGES_H__
#define __EXCHANGES_H__
#include <map>
#include "exchconst.h"
#include "exchangeinfo.h"
namespace exchangelib
{
	extern const std::map<common::string_t, ExchangeInfoRef> x_exchangeCollection;
}

#endif //__EXCHANGES_H__