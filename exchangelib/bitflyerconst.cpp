#include "commonfunc.h"
#include "bitflyerconst.h"

namespace exchangelib{
	namespace bitflyer{
		BitflyerConstants::BitflyerConstants() :
			_name(U("bitflyer")),
			_fee(XactFeesPct(0.4, 0.4))
		{
			_pairs[ETH_BTC] = U("ETH_BTC");
			_pairsInv = common::invertMap(_pairs);
			_symbols[BTC] = U("BTC");
			_symbols[ETH] = U("ETH");
			_symbolsInv = common::invertMap(_symbols);
		}

		BitflyerConstants x_bitflyerConstants;
	}
}