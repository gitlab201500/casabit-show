#ifndef __EXCHINFO_H__
#define __EXCHINFO_H__
#include "exchangeinfo.h"

namespace exchangelib{
	template <class DEPTHQ,class CONST>
		class TemplateExchangeInfo :public ExchangeInfo{
		public:
			TemplateExchangeInfo() :_const(std::make_shared<CONST>()){}
			virtual ~TemplateExchangeInfo(){}
			virtual std::function<ExchangeQueryRef<pplx::task<DepthRef>>(Pair pair, const DepthCfg&cfg)> depthFactory() const{
				return  [](Pair pair, const DepthCfg&cfg){
					return std::make_shared<DEPTHQ>(pair, cfg);
				};
			}
			virtual ExchangeConstantsRef constants() const {
				return _const;
			}
		private:
			std::shared_ptr<CONST> _const;
		};
}

#endif //__EXCHINFO_H__