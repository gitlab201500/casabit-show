#ifndef __EXCHCONST_H__
#define __EXCHCONST_H__
#include "unistr.h"
#include <map>
#include <pplxtasks.h>
#include "exchdef.h"
#include "exchentity.h"
namespace exchangelib{

	class ExchangeConstants{
	protected:
		typedef std::map<exchangelib::Pair, common::string_t> PairMap;
		typedef std::map<common::string_t, exchangelib::Pair> PairMapInv;
		typedef std::map<exchangelib::Symbol, common::string_t> SymbolMap;
		typedef std::map<common::string_t, exchangelib::Symbol> SymbolMapInv;

	public:
		virtual ~ExchangeConstants(){};
		virtual const common::string_t& name() const = 0;
		virtual const PairMap& pairs() const = 0;
		virtual const PairMapInv& pairsInv() const = 0;
		virtual const SymbolMap& symbols() const = 0;
		virtual const SymbolMapInv& symbolsInv() const = 0;
		virtual const XactFeesPct& fee() const = 0;
	};

	typedef std::shared_ptr<const ExchangeConstants> ExchangeConstantsRef;

}

#endif // __EXCHCONST_H__