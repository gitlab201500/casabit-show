#include "exchanges.h"
#include "exchinfo.h"
#include "bitflyerconst.h"
#include "poloniexconst.h"
#include "bitflyerdepth.h"
#include "poloniexdepth.h"

namespace exchangelib
{
	static const std::map<common::string_t, ExchangeInfoRef> initMap(){
		auto m = std::map<common::string_t,  ExchangeInfoRef>();
		m[std::make_shared<bitflyer::BitflyerConstants>()->name()] = std::make_shared<TemplateExchangeInfo<bitflyer::DepthQuery, bitflyer::BitflyerConstants>>();
		m[std::make_shared<poloniex::PoloniexConstants>()->name()] = std::make_shared<TemplateExchangeInfo<poloniex::DepthQuery, poloniex::PoloniexConstants>>();
		return m;
	}
	const std::map<common::string_t,  ExchangeInfoRef> x_exchangeCollection = initMap();
}