#ifndef __EXCHENTITY_H__
#define __EXCHENTITY_H__
#include <vector>
#include <chrono>
#include "logger.h"
#include "commonutil.h"
#include "exchdef.h"

namespace exchangelib
{
	struct DepthPairBid{
		DepthPairBid(double qi, double vi) :q(qi), v(vi) {}
		bool operator<(const DepthPairBid&rhs) const { return q < rhs.q; }
		double q;
		double v;
	};

	struct DepthPairAsk{
		DepthPairAsk(double qi, double vi) :q(qi), v(vi) {}
		bool operator<(const DepthPairAsk&rhs){ return q < rhs.q; }
		double q;
		double v;
	};

	struct DepthInfo{
		DepthInfo(const std::vector<DepthPairAsk>& asksi, const std::vector<DepthPairBid>& bidsi) :asks(asksi), bids(bidsi) {}
		DepthInfo(std::vector<DepthPairAsk>&& asksi, std::vector<DepthPairBid>&& bidsi) :asks(asksi), bids(bidsi) {}
		DepthInfo(DepthInfo&& rhs) :asks(std::move(rhs.asks)), bids(std::move(rhs.bids)) {}
		DepthInfo& operator=(const DepthInfo&& rhs) { if (this == &rhs) return *this;  asks = std::move(rhs.asks); bids= std::move(rhs.bids); }
		std::vector<DepthPairAsk> asks; 
		std::vector<DepthPairBid> bids;
	};

	struct Depth{
		Depth(Pair pairi, const DepthInfo& infoi) :pair(pairi),info(infoi){}
		Depth(Depth&& rhs) :pair(rhs.pair), info(rhs.info){}
		Depth& operator=(Depth&& rhs) { if (this == &rhs) return *this; pair = rhs.pair; info = rhs.info; }
		Pair pair;
		DepthInfo info;
	};	
	typedef std::shared_ptr<Depth> DepthRef;

	struct DepthCfg{
		DepthCfg( int szi = 20) :sz(szi){}
		DepthCfg(DepthCfg&& rhs) :sz(rhs.sz){}
		DepthCfg& operator=(DepthCfg&& rhs) { if (this == &rhs) return *this; sz = rhs.sz; }
		int sz;
	};

	struct XactFeesPct{
		XactFeesPct(double makei, double takei) :make(makei), take(takei){}
		double make;
		double take;
	};

	template <class T>
	class ExchangeQuery{
	public:
		ExchangeQuery() :_start(std::chrono::system_clock::now()),_end(_start){}
		virtual ~ExchangeQuery(){}
		virtual T fetch() = 0;
		virtual std::chrono::system_clock::time_point start()const { return  _start; }
		virtual std::chrono::system_clock::time_point end()const { return  _end; }
		virtual long long rtMs()const { return  std::chrono::duration_cast<std::chrono::milliseconds>(_end - _start).count(); }
	protected:
		std::chrono::system_clock::time_point _start;
		std::chrono::system_clock::time_point _end;
	};

	template <class T> 
	using ExchangeQueryRef = std::shared_ptr<ExchangeQuery<T>>;
}

#endif //__EXCHENTITY_H__

