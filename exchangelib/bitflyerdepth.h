#ifndef __BITFLYERDEPTH_H__
#define __BITFLYERDEPTH_H__
#include <http_client.h>
#include <pplxtasks.h>
#include "bitflyerconst.h"
#include "logger.h"
#include "commondefs.h"
#include "depth.h"

namespace exchangelib
{
	namespace bitflyer{
		class DepthQuery :public DepthQueryBase{
		public:
			DepthQuery(Pair pair,
				const DepthCfg& cfg = DepthCfg()
				) :DepthQueryBase(&x_bitflyerConstants, pair,cfg){}
			virtual common::string_t url() { return U("https://lightning.bitflyer.jp/v1/getboard?product_code=") + _pairInv; }
			virtual DepthRef parseJson(const web::json::value& json);
		};
	}
}

#endif // __BITFLYERDEPTH_H__