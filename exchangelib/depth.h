#ifndef __DEPTH_H__
#define __DEPTH_H__
#include <http_client.h>
#include <pplxtasks.h>
#include "logger.h"
#include "commondefs.h"
#include "exchentity.h"
#include "exchconst.h"

namespace exchangelib
{
	class DepthQueryBase :public ExchangeQuery<pplx::task<DepthRef>>, public std::enable_shared_from_this<DepthQueryBase>{
		public:
			DepthQueryBase(const ExchangeConstants* constants,
				Pair pair,
				const DepthCfg& cfg = DepthCfg()
				) :_pair(pair), _cfg(cfg){
				assert(constants);
				auto m = constants->pairs();
				if (m.find(_pair) == m.end()) throw std::exception(common::ws2s(U("Invalid pair: ") + pair).c_str());
				_pairInv = m[_pair];
			}
			virtual ~DepthQueryBase(){}
			virtual pplx::task<DepthRef> fetch();
		protected:
			virtual common::string_t url() = 0;
			virtual DepthRef parseJson(const web::json::value& json) = 0;
			const Pair _pair;
			common::string_t _pairInv;
			const DepthCfg _cfg;
		};
}

#endif // __DEPTH_H__