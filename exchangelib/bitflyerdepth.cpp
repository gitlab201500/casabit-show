#include <http_client.h>
#include "commonutil.h"
#include "exchutil.h"
#include "bitflyerdepth.h"


namespace exchangelib
{
	namespace bitflyer{
		DepthRef DepthQuery::parseJson(const web::json::value& json){
			auto ret = std::make_shared<Depth>(_pair, DepthInfo({}, {}));
			auto o = GETASORELSE(json,object,throw std::exception("Invalid json: depth response is not an object"));
			auto asks = GETFIELDORELSE(o, asks, array, throw std::exception("Invalid json: depth response contains no asks field"));
			std::vector<DepthPairAsk>& vasks = ret->info.asks;
			for (auto a : asks){
				auto i = GETASORELSE(a, object, throw std::exception("Invalid json: asks field does not contain askpair"));
				vasks.push_back(DepthPairAsk(
					GETFIELDASNUMBER(i, price, throw std::exception("Invalid json: askspair contains no price field")),
					GETFIELDASNUMBER(i, size, throw std::exception("Invalid json: askspair contains no size field"))
					));
			}

			auto bids = GETFIELDORELSE(o, bids, array, throw std::exception("Invalid json: depth response contains no bids field"));
			std::vector<DepthPairBid>& vbids = ret->info.bids;
			for (auto b : bids){
				auto i = GETASORELSE(b, object, throw std::exception("Invalid json: bids field does not contain askpair"));
				vbids.push_back(DepthPairBid(
					GETFIELDASNUMBER(i, price, throw std::exception("Invalid json: bidspair contains no price field")),
					GETFIELDASNUMBER(i, size, throw std::exception("Invalid json: bidspair contains no size field"))
					));
			}

			return ret;
		}
	}

}