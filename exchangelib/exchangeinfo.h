#ifndef __EXCHANGEINFO_H__
#define __EXCHANGEINFO_H__
#include "unistr.h"
#include <map>
#include <pplxtasks.h>
#include "exchentity.h"
namespace exchangelib{
	class ExchangeInfo{
	public:
		virtual ~ExchangeInfo(){}
		virtual std::function<ExchangeQueryRef<pplx::task<DepthRef>>(Pair pair, const DepthCfg&cfg)> depthFactory() const = 0;
		virtual ExchangeConstantsRef constants() const = 0;
	};
	typedef std::shared_ptr<const ExchangeInfo> ExchangeInfoRef;
}

#endif // __EXCHANGEINFO_H__