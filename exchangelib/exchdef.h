#ifndef __EXCHDEF_H__
#define __EXCHDEF_H__
#include <vector>
#include <map>
#include "unistr.h"
namespace exchangelib{


	enum Symbol{
		ETH = 0, BTC, LTC, XRP
	};

	inline common::string_t symtos(Symbol e){
		static const std::vector<common::string_t> v = { U("ETH"), U("BTC"), U("XRP") };
		if (e >= v.size()) throw std::exception("Invalid symbol idx");
		return v[(int)e];
	}

	enum Pair{
		ETH_BTC=0,LTC_BTC,XRP_BTC
	};

	inline common::string_t pairtos(Pair e){
		static const std::vector<common::string_t> v = { U("ETH_BTC"), U("LTC_BTC"), U("XRP_BTC") };
		if (e >= v.size()) throw std::exception("Invalid pair idx");
		return v[(int)e];
	}

	inline std::vector<Symbol> pairToSymbols(Pair pair){
		static auto f = std::function<std::map<Pair, std::vector<Symbol>>()>([](){
			std::map<Pair, std::vector<Symbol>> m;
			m[ETH_BTC] = { ETH,BTC };
			m[LTC_BTC] = { LTC,BTC };
			m[XRP_BTC] = { XRP,BTC };
			return m;
		});
		static std::map<Pair, std::vector<Symbol>> m = f();
		auto v = m.find(pair);
		if (v == m.end()) throw std::exception("Invalid pair");
		return v->second;
	}

	enum Exchange{
		Poloniex = 0, Bitlfyer
	};

	inline common::string_t exchtos(Exchange e){
		static const std::vector<common::string_t> v = { U("poloniex"), U("bitflyer") };
		if (e >= v.size()) throw std::exception("Invalid exchange idx");
		return v[(int)e];
	}

	inline Exchange stoexch(const common::string_t& s){
		static auto f = std::function<std::map<common::string_t, Exchange>()>([](){
			std::map<common::string_t, Exchange> m;
			m[U("poloniex")] = Poloniex;
			m[U("bitflyer")] = Bitlfyer;
			return m;
		});

		static const std::map<common::string_t, Exchange> m = f();
		auto ee = m.find(s);
		if (ee == m.end()) throw std::exception("Invalid exchange");
		return ee->second;
	}
}

#endif //__EXCHDEF_H__