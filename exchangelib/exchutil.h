#ifndef __EXCHUTIL_H__
#define __EXCHUTIL_H__

#include "commonutil.h"
#include <http_client.h>
namespace exchangelib{
	web::json::value extractJson(web::http::http_response& response);
	template <class R>
	common::TryResult<R> Try(std::function<R()>& foo){
		try{
			return common::TryResult<R>(foo());
		}
		catch (const pplx::task_canceled)
		{
			return common::TryResult<R>(std::exception("pplx::task_canceled"));
		}
		catch (const std::exception&e){
			return common::TryResult<R>(std::exception(e.what() != nullptr ? e.what():"unknown"));
		}
		catch (...){
			return common::TryResult<R>(std::exception("unexpected exception"));
		}
	}

	inline std::pair<bool,double> getFieldAsNumber(const web::json::object&o,const common::string_t& f){
		auto v = o.find(f);
		if (v == o.end()) return std::pair<bool, double>(false, 0.0);
		if (v->second.is_double()) return std::pair<bool, double>(true, v->second.as_double());
		else if (v->second.is_integer()) return std::pair<bool, double>(true, v->second.as_integer());
		else return std::pair<bool, double>(false, 0.0);
	}

}



#define GETASORELSE(j,type,el) ((j).is_##type()) ? ((j).as_##type()) : (el)
#define GETFIELDORELSE(j,f,type,el) ((j).find(U(#f))!=(j).end() && (j)[U(#f)].is_##type()) ? ((j)[U(#f)].as_##type()) : (el)
#define GETFIELDASNUMBER(j,f,el) std::function<double()>([j](){auto v = exchangelib::getFieldAsNumber(j,U(#f)); return (v.first) ? (v.second) : (el);})()
#endif //__EXCHUTIL_H__