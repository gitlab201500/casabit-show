#ifndef __POLONIEXCONST_H__
#define __POLONIEXCONST_H__
#include "exchconst.h"
#include "exchentity.h"

namespace exchangelib{
	namespace poloniex{
		class PoloniexConstants :public ExchangeConstants{
		public:
			PoloniexConstants();
			const common::string_t& name() const { return _name; }
			const PairMap& pairs() const { return _pairs; }
			const PairMapInv& pairsInv() const { return _pairsInv; }
			const SymbolMap& symbols()  const{ return _symbols; }
			const SymbolMapInv& symbolsInv() const { return _symbolsInv; }
			const XactFeesPct& fee() const{ return _fee; }

		private:
			const common::string_t _name;
			PairMap _pairs;
			PairMapInv _pairsInv;
			SymbolMap _symbols;
			SymbolMapInv _symbolsInv;
			const XactFeesPct _fee;
		};
		extern PoloniexConstants x_poloniexConstants;
	}
}

#endif //__POLONIEXCONST_H__