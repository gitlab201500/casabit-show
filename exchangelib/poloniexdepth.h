#ifndef __POLONIEXDEPTH_H__
#define __POLONIEXDEPTH_H__
#include <http_client.h>
#include <pplxtasks.h>
#include "poloniexconst.h"
#include "logger.h"
#include "commondefs.h"
#include "depth.h"

namespace exchangelib
{
	namespace poloniex{
		class DepthQuery :public DepthQueryBase{
		public:
			DepthQuery(Pair pair,
				const DepthCfg& cfg = DepthCfg()
				) :DepthQueryBase(&x_poloniexConstants, pair, cfg){}
			virtual common::string_t url() { return U("https://poloniex.com/public?command=returnOrderBook&currencyPair=") + _pairInv; }
			virtual DepthRef parseJson(const web::json::value& json);
		};
	}
}

#endif // __POLONIEXDEPTH_H__