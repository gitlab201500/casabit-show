#ifndef __TESTCOMMON_H__
#define __TESTCOMMON_H__
#include <iostream>
#include <chrono>
#include <map>
#include <functional>

typedef std::function<bool()> TestFunc;
#define TSTASSERT(x, text) if(!(x)) {std::cerr << "Error:" << text<< std::endl;return false;}
#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)

#endif //__TESTCOMMON_H__