// testactor.cpp : Defines the entry point for the console application.
//

#ifdef WIN32
#include "stdafx.h"
#endif // WIN32
#include "commondefs.h"
#include "actor.h"
#include <vector>
#include <chrono>
#include <typeinfo>
#include <thread>
#include <algorithm>
#include <map>
#include <iostream>
#include <functional>
#include "testcommon.h"
#include "commonutil.h"


inline std::string testName(const std::string& t)
{
	return __FILENAME__ + std::string("::") + t;
}


namespace messagetest{
	DEFINE_LOGGER(TestMessage, INFO);
	const actor::MessageType MessageTypeStart = 0;
	const actor::MessageType MessageType1 = 1;
	const actor::MessageType MessageType2 = 2;

	class MessageStart : public actor::Message{
	public:
		MessageStart(const common::string_t& m) :Message(MessageTypeStart), _m(m){}
		MessageStart(actor::ActorRefWeak& s, const common::string_t& m) :Message(MessageTypeStart, s), _m(m){}
		const common::string_t& msg() const { return _m; }
	private:
		const common::string_t _m;
	};


	class Message1 : public actor::Message{
	public:
		Message1(const common::string_t& m) :Message(MessageType1), _m(m){}
		Message1(actor::ActorRefWeak& s, const common::string_t& m) :Message(MessageType1, s), _m(m){}
		const common::string_t& msg() const { return _m; }
	private:
		const common::string_t _m;
	};

	class Message2 : public actor::Message{
	public:
		Message2(const common::string_t& m) :Message(MessageType2), _m(m){}
		Message2(actor::ActorRefWeak& s, const common::string_t& m) :Message(MessageType2, s), _m(m){}
		const common::string_t& msg() const { return _m; }
	private:
		const common::string_t _m;
	};

	class Actor1 :public actor::ActorBase{
		DECLARE_SHARED_REF_OBJ(Actor1, const common::string_t& n, actor::ActorSystemRefWeak& as, actor::ActorRefWeak a2);
	public:
		virtual ~Actor1(){}
		std::vector<common::string_t> _messageRecvd;
		pplx::extensibility::event_t _completed;
		pplx::extensibility::event_t _completed100;
	private:
		actor::ActorRefWeak _a2;

	};

	DEFINE_SHARED_REF_OBJ(Actor1, const common::string_t& n, actor::ActorSystemRefWeak& as, actor::ActorRefWeak a2){
		auto p = MAKE_SHARED(Actor1);
		p->initActorBase(n, as);
		p->_a2 = a2;
		p->becomes([p](const actor::MessageRef& m){
			common::stringstream_t ss;
			assert(m);
			actor::MessageType type = m->type();
			switch (type){
			case MessageTypeStart:
			{
				auto mm = CAST_MESSAGE(MessageStart, m);
				ss << U("Received message: ") << type << U(" msg: ") << mm->msg();
				logDebug(logTestMessage,ss.str());
				p->_messageRecvd.push_back(mm->msg());

				auto a2s = p->_a2.lock();
				if (a2s){
					a2s->tell(MAKE_MESSAGE(Message2, p, mm->msg()));
				}
				else {
					logDebug(logTestMessage,U("a2 is empty"));
				}
				break;
			}
			case MessageType1:
				{
					auto mm = CAST_MESSAGE(Message1, m);
					ss << U("Received message: ") << type << U(" msg: ") << mm->msg();
					logDebug(logTestMessage,ss.str());
					p->_messageRecvd.push_back(mm->msg());
					p->_completed.set();
					if (p->_messageRecvd.size() == 100){
						p->_completed100.set();
					}
					break;
				}
				default:
					ss << U("Unexpected message type: ") << type;
					logWarn(logTestMessage,ss.str());
					break;
			}
		});
		return p;
	}

	class Actor2 :public actor::ActorBase{
		DECLARE_SHARED_REF_OBJ(Actor2, const common::string_t& n, actor::ActorSystemRefWeak& as);
	public:
		virtual ~Actor2(){}
		std::vector<common::string_t> _messageRecvd;
		pplx::extensibility::event_t _completed;

	private:


	};

	DEFINE_SHARED_REF_OBJ(Actor2, const common::string_t& n, actor::ActorSystemRefWeak& as){
		auto p = MAKE_SHARED(Actor2);
		p->initActorBase(n, as);
		p->becomes([p](const actor::MessageRef& m){
			common::stringstream_t ss;
			assert(m);
			actor::MessageType type = m->type();
			switch (type){
			case MessageType2:
			{
								 auto mm = CAST_MESSAGE(Message2, m);
								 ss << U("Received message: ") << type << U(" msg: ") << mm->msg();
								 logDebug(logTestMessage,ss.str());
								 p->_messageRecvd.push_back(mm->msg());

								 actor::ActorRef a2s = mm->sender();
								 if (a2s){
									 a2s->tell(MAKE_MESSAGE(Message1, p, mm->msg()));
								 }
								 else {
									 logDebug(logTestMessage,U("sender a2 is empty"));
								 }
								 break;
			}
			default:
				ss << U("Unexpected message type: ") << type;
				logWarn(logTestMessage,ss.str());
				break;
			}
		});
		return p;
	}

	class Actor3 :public actor::ActorBase{
		DECLARE_SHARED_REF_OBJ(Actor3, const common::string_t& n, actor::ActorSystemRefWeak& as, actor::ActorRefWeak a2);
	public:
		virtual ~Actor3(){}
		std::vector<common::string_t> _messageRecvd;
		pplx::extensibility::event_t _completed;

	private:
		actor::ActorRefWeak _a2;
		std::function<void(const actor::MessageRef& m)> _stateFoo;
		std::function<void(const actor::MessageRef& m)> _firstFoo;
		std::function<void(const actor::MessageRef& m)> _secondFoo;
	};

	DEFINE_SHARED_REF_OBJ(Actor3, const common::string_t& n, actor::ActorSystemRefWeak& as, actor::ActorRefWeak a2){
		auto p = MAKE_SHARED(Actor3);
		p->initActorBase(n, as);
		p->_a2 = a2;
		p->_firstFoo = std::function<void(const actor::MessageRef& m)>([p](const actor::MessageRef& m){
			common::stringstream_t ss;
			assert(m);
			actor::MessageType type = m->type();
			switch (type){
			case MessageTypeStart:
			{
									 auto mm = CAST_MESSAGE(MessageStart, m);
									 ss << U("Received message: ") << type << U(" msg: ") << mm->msg();
									 logDebug(logTestMessage,ss.str());
									 p->_messageRecvd.push_back(mm->msg());

									 auto a2s = p->_a2.lock();
									 if (a2s){
										 a2s->tell(MAKE_MESSAGE(Message2, p, mm->msg()));
										 p->becomes(p->_secondFoo);
										 logDebug(logTestMessage,U("Set _secondFoo"));
									 }
									 else {
										 logDebug(logTestMessage,U("a2 is empty"));
									 }
									 break;
			}

			default:
				ss << U("Unexpected message type: ") << type;
				logWarn(logTestMessage,ss.str());
				break;
			}
		});

		p->_secondFoo = std::function<void(const actor::MessageRef& m)>([p](const actor::MessageRef& m){
			common::stringstream_t ss;
			assert(m);
			actor::MessageType type = m->type();
			switch (type){

			case MessageType1:
			{
								 auto mm = CAST_MESSAGE(Message1, m);
								 ss << U("Received message: ") << type << U(" msg: ") << mm->msg();
								 logDebug(logTestMessage,ss.str());
								 p->_messageRecvd.push_back(mm->msg());
								 p->_completed.set();

								 p->becomes(p->_firstFoo);
								 logDebug(logTestMessage,U("Set _firstFoo"));
								 break;
			}
			default:
				ss << U("Unexpected message type: ") << type;
				logWarn(logTestMessage,ss.str());
				break;
			}
		});

		p->becomes(p->_firstFoo);
		return p;
	}


}

typedef std::function<bool()> TestFunc;
const unsigned int waitToTerm = 5000;

std::map<std::string, TestFunc> g_messageTests = {

	std::pair<std::string, TestFunc>(testName("test1"), TestFunc([]()
	{
		std::cout << "test1: Create 2 actors send messages between" << std::endl;

		try{
			auto as = actor::ActorSystem::createActorSystem(U("ActorSystemTest"));
			TSTASSERT(as, "ActorSystem is null");

			auto a2 = messagetest::Actor2::createActor2(U("Actor2"), actor::ActorSystemRefWeak(as));
			auto a1 = messagetest::Actor1::createActor1(U("Actor1"), actor::ActorSystemRefWeak(as),a2);


			common::string_t m = U("aaa");
			auto mm = MAKE_MESSAGE_NOSENDER(messagetest::MessageStart,m);
			a1->tell(mm);
			a1->_completed.wait(10000);
			TSTASSERT(a1->_messageRecvd.size()==2, "a1 Wrong message size");
			TSTASSERT(a2->_messageRecvd.size() == 1, "a2 Wrong message size");
			TSTASSERT(a1->_messageRecvd[0] == m, "a1 Message does not match");
			TSTASSERT(a1->_messageRecvd[1] == m, "a1 Message does not match");
			TSTASSERT(a2->_messageRecvd[0] == m, "a2 Message does not match");

			a1->_completed.reset();
			m = U("bbb");
			mm = MAKE_MESSAGE_NOSENDER(messagetest::MessageStart, m);
			a1->tell(mm);
			a1->_completed.wait(10000);
			TSTASSERT(a1->_messageRecvd.size() == 4, "a1 Wrong message size");
			TSTASSERT(a2->_messageRecvd.size() == 2, "a2 Wrong message size");
			TSTASSERT(a1->_messageRecvd[2] == m, "a1 Message does not match");
			TSTASSERT(a1->_messageRecvd[3] == m, "a1 Message does not match");
			TSTASSERT(a2->_messageRecvd[1] == m, "a2 Message does not match");

			as->terminate();
			TSTASSERT(as->onTerminated(waitToTerm).get(), "Not terminated");
			TSTASSERT(a1->onTerminated(waitToTerm).get(), "Actor1 terminaiton failed");
			TSTASSERT(a2->onTerminated(waitToTerm).get(), "Actor2 terminaiton failed");

			return true;
		}
		catch (const std::exception&e){
			std::cerr<< "Test failed , exception: " << e.what()<<std::endl;
			return false;

		}

	}
	)),

		std::pair<std::string, TestFunc>(testName("test2"), TestFunc([]()
	{
		std::cout << "test2: Create 2 actors send messages when second actor is terminated" << std::endl;

		try{
			auto as = actor::ActorSystem::createActorSystem(U("ActorSystemTest"));
			TSTASSERT(as, "ActorSystem is null");

			auto a2 = messagetest::Actor2::createActor2(U("Actor2"), actor::ActorSystemRefWeak(as));
			auto a1 = messagetest::Actor1::createActor1(U("Actor1"), actor::ActorSystemRefWeak(as), a2);


			common::string_t m = U("aaa");
			auto mm = MAKE_MESSAGE_NOSENDER(messagetest::MessageStart, m);
			TSTASSERT(a2->terminate(waitToTerm).get(), "Actor2 terminaiton failed");
			a1->tell(mm);
			a1->_completed.wait(1000);
			TSTASSERT(a1->_messageRecvd.size() == 1, "a1 Wrong message size");
			TSTASSERT(a2->_messageRecvd.size() == 0, "a2 Wrong message size");
			TSTASSERT(a1->_messageRecvd[0] == m, "a1 Message does not match");

			as->terminate();
			TSTASSERT(as->onTerminated(waitToTerm).get(), "Not terminated");
			TSTASSERT(a1->onTerminated(waitToTerm).get(), "Actor1 terminaiton failed");
			TSTASSERT(a2->onTerminated(waitToTerm).get(), "Actor2 terminaiton failed");

			return true;
		}
		catch (const std::exception&e){
			std::cerr << "Test failed , exception: " << e.what() << std::endl;
			return false;

		}

	}
	)),
		std::pair<std::string, TestFunc>(testName("test3"), TestFunc([]()
	{
		std::cout << "test3: Create 2 actors send messages when first actor is terminated" << std::endl;

		try{
			auto as = actor::ActorSystem::createActorSystem(U("ActorSystemTest"));
			TSTASSERT(as, "ActorSystem is null");

			auto a2 = messagetest::Actor2::createActor2(U("Actor2"), actor::ActorSystemRefWeak(as));
			auto a1 = messagetest::Actor1::createActor1(U("Actor1"), actor::ActorSystemRefWeak(as), a2);


			common::string_t m = U("aaa");
			auto mm = MAKE_MESSAGE_NOSENDER(messagetest::MessageStart, m);
			TSTASSERT(a1->terminate(waitToTerm).get(), "Actor2 terminaiton failed");
			a1->tell(mm);
			a1->_completed.wait(1000);
			TSTASSERT(a1->_messageRecvd.size() == 0, "a1 Wrong message size");
			TSTASSERT(a2->_messageRecvd.size() == 0, "a2 Wrong message size");

			as->terminate();
			TSTASSERT(as->onTerminated(waitToTerm).get(), "Not terminated");
			TSTASSERT(a1->onTerminated(waitToTerm).get(), "Actor1 terminaiton failed");
			TSTASSERT(a2->onTerminated(waitToTerm).get(), "Actor2 terminaiton failed");

			return true;
		}
		catch (const std::exception&e){
			std::cerr << "Test failed , exception: " << e.what() << std::endl;
			return false;

		}

	}
	)),
		std::pair<std::string, TestFunc>(testName("test4"), TestFunc([]()
	{
		std::cout << "test4: Create 2 actors send messages between with state transition" << std::endl;

		try{
			auto as = actor::ActorSystem::createActorSystem(U("ActorSystemTest"));
			TSTASSERT(as, "ActorSystem is null");

			auto a2 = messagetest::Actor2::createActor2(U("Actor2"), actor::ActorSystemRefWeak(as));
			auto a3 = messagetest::Actor3::createActor3(U("Actor3"), actor::ActorSystemRefWeak(as), a2);


			common::string_t m = U("aaa");
			auto mm = MAKE_MESSAGE_NOSENDER(messagetest::MessageStart, m);
			a3->tell(mm);
			a3->_completed.wait(10000);
			TSTASSERT(a3->_messageRecvd.size() == 2, "a3 Wrong message size");
			TSTASSERT(a2->_messageRecvd.size() == 1, "a2 Wrong message size");
			TSTASSERT(a3->_messageRecvd[0] == m, "a3 Message does not match");
			TSTASSERT(a3->_messageRecvd[1] == m, "a3 Message does not match");
			TSTASSERT(a2->_messageRecvd[0] == m, "a2 Message does not match");

			a3->_completed.reset();
			m = U("bbb");
			mm = MAKE_MESSAGE_NOSENDER(messagetest::MessageStart, m);
			a3->tell(mm);
			a3->_completed.wait(10000);
			TSTASSERT(a3->_messageRecvd.size() == 4, "a3 Wrong message size");
			TSTASSERT(a2->_messageRecvd.size() == 2, "a2 Wrong message size");
			TSTASSERT(a3->_messageRecvd[2] == m, "a3 Message does not match");
			TSTASSERT(a3->_messageRecvd[3] == m, "a3 Message does not match");
			TSTASSERT(a2->_messageRecvd[1] == m, "a2 Message does not match");

			as->terminate();
			TSTASSERT(as->onTerminated(waitToTerm).get(), "Not terminated");
			TSTASSERT(a3->onTerminated(waitToTerm).get(), "Actor1 terminaiton failed");
			TSTASSERT(a2->onTerminated(waitToTerm).get(), "Actor2 terminaiton failed");

			return true;
		}
		catch (const std::exception&e){
			std::cerr << "Test failed , exception: " << e.what() << std::endl;
			return false;

		}

	}
	)),
		std::pair<std::string, TestFunc>(testName("test5"), TestFunc([]()
	{
		std::cout << "test5: Send 100 messages check their order" << std::endl;

		try{
			auto as = actor::ActorSystem::createActorSystem(U("ActorSystemTest"));
			TSTASSERT(as, "ActorSystem is null");

			auto a2 = messagetest::Actor2::createActor2(U("Actor2"), actor::ActorSystemRefWeak(as));
			auto a1 = messagetest::Actor1::createActor1(U("Actor1"), actor::ActorSystemRefWeak(as), a2);


			const int sz = 100;
			for (int i = 0; i < sz; i++)
			{
				common::stringstream_t ss;
				ss << i;
				auto mm = MAKE_MESSAGE_NOSENDER(messagetest::Message1, ss.str());
				a1->tell(mm);
			}

			a1->_completed100.wait(10000);
			TSTASSERT(a1->_messageRecvd.size() == sz, "a1 Wrong message size");

			for (int i = 0; i < sz; i++)
			{
				common::stringstream_t ss;
				ss << i;
				TSTASSERT(a1->_messageRecvd[i] == ss.str(), "a3 Message does not match");
			}

			as->terminate();
			TSTASSERT(as->onTerminated(waitToTerm).get(), "Not terminated");
			TSTASSERT(a1->onTerminated(waitToTerm).get(), "Actor1 terminaiton failed");
			TSTASSERT(a2->onTerminated(waitToTerm).get(), "Actor2 terminaiton failed");

			return true;
		}
		catch (const std::exception&e){
			std::cerr << "Test failed , exception: " << e.what() << std::endl;
			return false;

		}

	}
	)),
};