// testactor.cpp : Defines the entry point for the console application.
//

#ifdef WIN32
#include "stdafx.h"
#endif // WIN32

#include "commondefs.h"
#include "actor.h"
#include <vector>
#include <chrono>
#include <typeinfo>
#include <thread>
#include <algorithm>
#include <map>
#include <iostream>
#include <functional>
#include "testcommon.h"
#include "commonutil.h"


inline std::string testName(const std::string& t)
{
	return __FILENAME__ + std::string("::") + t;
}


namespace actorsystest{


	DEFINE_LOGGER(ActorSysTest, INFO);

	const actor::MessageType MessageType1 = 1;
	const actor::MessageType MessageType2 = 2;

	class Message1 : public actor::Message{
	public:
		Message1(const common::string_t& m) :Message(MessageType1), _m(m){}
		Message1(actor::ActorRefWeak& s, const common::string_t& m) :Message(MessageType1, s), _m(m){}
		const common::string_t& msg() const { return _m; }
	private:
		const common::string_t _m;
	};

	class Message2 : public actor::Message{
	public:
		Message2(const common::string_t& m) :Message(MessageType2), _m(m){}
		Message2(actor::ActorRefWeak& s, const common::string_t& m) :Message(MessageType2, s), _m(m){}
		const common::string_t& msg() const { return _m; }
	private:
		const common::string_t _m;
	};

	class Actor1 :public actor::ActorBase{
		DECLARE_SHARED_REF_OBJ(Actor1, const common::string_t& n, actor::ActorSystemRefWeak& as, const actor::ActorBaseConfig& conf = actor::ActorBaseConfig());
	public:
		std::vector<common::string_t> _messageRecvd;
		pplx::extensibility::event_t _completed;
		int _msgcount;
		virtual ~Actor1(){}
	private:
	};

	DEFINE_SHARED_REF_OBJ(Actor1, const common::string_t& n, actor::ActorSystemRefWeak& as, const actor::ActorBaseConfig& conf){
		auto p = MAKE_SHARED(Actor1);
		p->initActorBase(n, as, conf);
		p->_msgcount = 0;
		p->becomes([p](const actor::MessageRef& m){
			common::stringstream_t ss;
			assert(m);
			actor::MessageType type = m->type();
			switch (type){
			case MessageType1:
			{
				auto mm = CAST_MESSAGE(Message1, m);
				ss << U("Received message: ") << type << U(" msg: ") << mm->msg();
				logDebug(logActorSysTest,ss.str());
				p->_messageRecvd.push_back(mm->msg());
				p->_completed.set();
				if (mm->msg() == common::string_t(U("terminate")))
					throw std::exception("terminate msg received");
				else if (mm->msg() == common::string_t(U("hang"))){
					logDebug(logActorSysTest,U("Hang actor"));
					std::this_thread::sleep_for(std::chrono::milliseconds(300000));
				}
				else if (mm->msg() == common::string_t(U("sleep"))){
					logDebug(logActorSysTest,U("Sleep actor 1 sec"));
					std::this_thread::sleep_for(std::chrono::milliseconds(1000));
				}
				else if (mm->msg() == common::string_t(U("check"))){
					for (int i = 0; i < 10000 && !pplx::is_task_cancellation_requested(); i++){
						logDebug(logActorSysTest,U("Sleep actor 20 msec"));
						std::this_thread::sleep_for(std::chrono::milliseconds(200));
					}
					logDebug(logActorSysTest,U("Actor exit loop"));
				}
				else if (mm->msg() == common::string_t(U("exchcatch"))){
					logDebug(logActorSysTest,U("Throw and catch exception in the handler"));
					try{
						throw std::exception("exchcatch msg received");
					}
					catch (...){
						logDebug(logActorSysTest,U("in exchcatch exception handler"));
					}
					logDebug(logActorSysTest,U("finished exchcatch"));

				}
				else if (mm->msg() == common::string_t(U("msgcount"))){
					logDebug(logActorSysTest,U("Sleep actor 1 sec"));
					std::this_thread::sleep_for(std::chrono::milliseconds(1000));
					p->_msgcount++;
				}
			}

				break;
			default:
				ss << U("Unexpected message type: ") << type;
				logWarn(logActorSysTest,ss.str());
				break;
			}
		});
		return p;
	}


	std::shared_ptr<std::pair<actor::ActorSystemRef,actor::ActorRef>> createActorsystem1(){
		auto as = actor::ActorSystem::createActorSystem(U("ActorSystemTest"));
		auto a1 = actorsystest::Actor1::createActor1(U("Actor1"), actor::ActorSystemRefWeak(as));
		return std::make_shared<std::pair<actor::ActorSystemRef, actor::ActorRef>>(as, a1);
	}

}

typedef std::function<bool()> TestFunc;
const unsigned int waitToTerm = 5000;

std::map<std::string, TestFunc> g_asTests = {

	std::pair<std::string, TestFunc>(testName("test1"), TestFunc([]()
	{
		std::cout << "test1: Create ActorSystem and terminate it twice" << std::endl;
		
		auto as = actor::ActorSystem::createActorSystem(U("ActorSystemTest"));
		TSTASSERT(as, "ActorSystem is null");
		as->terminate();
		TSTASSERT(as->onTerminated(waitToTerm).get(), "Not terminated");
		as->terminate();
		TSTASSERT(as->onTerminated(waitToTerm).get(), "Terminaiton failed");

		return true;
	}
	)),

		std::pair<std::string, TestFunc>(testName("test2"), TestFunc([]()
	{
		std::cout << "test2: Create ActorSystem + one actor" << std::endl;

		auto as = actor::ActorSystem::createActorSystem(U("ActorSystemTest"));
		TSTASSERT(as, "ActorSystem is null");
		auto a1 = actorsystest::Actor1::createActor1(U("Actor1"), actor::ActorSystemRefWeak(as));
		as->terminate();
		TSTASSERT(as->onTerminated(waitToTerm).get(), "Not terminated");
		TSTASSERT(a1->onTerminated(waitToTerm).get(), "Actor1 is not terminated");

		a1->terminate();
		TSTASSERT(as->onTerminated(waitToTerm).get(), "Not terminated");
		TSTASSERT(a1->onTerminated(waitToTerm).get(), "Actor1 is not terminated");
		return true;
	}
	)),

		std::pair<std::string, TestFunc>(testName("test3"), TestFunc([]()
	{
		std::cout << "test3: Create Actor with invalid name or ActorSystem" << std::endl;

		auto as = actor::ActorSystem::createActorSystem(U("ActorSystemTest"));


		auto a1 = common::Try(std::function<std::shared_ptr<actorsystest::Actor1>()>([as](){
			return actorsystest::Actor1::createActor1(common::string_t(), actor::ActorSystemRefWeak(as)); }
			));
		TSTASSERT(a1.isFailure(), "Actor1 is not null");
		std::cout << a1.failure().what() << std::endl;

		auto a2 = common::Try(std::function<std::shared_ptr<actorsystest::Actor1>()>([as](){
			return actorsystest::Actor1::createActor1(U("aa"), actor::ActorSystemRefWeak()); }
		));
		TSTASSERT(a2.isFailure(), "Actor1 is not null");
		std::cout << a2.failure().what() << std::endl;

		as->terminate();
		TSTASSERT(as->onTerminated(waitToTerm).get(), "Not terminated");

		auto a3 = common::Try(std::function<std::shared_ptr<actorsystest::Actor1>()>([as](){
			return actorsystest::Actor1::createActor1(U("aa"), actor::ActorSystemRefWeak(as)); }
		));
		TSTASSERT(a3.isFailure(), "Actor1 is not null");
		std::cout << a3.failure().what() << std::endl;

		return true;
	}
	)),

		std::pair<std::string, TestFunc>(testName("test4"), TestFunc([]()
	{
		std::cout << "test4: Create ActorSystem + one actor + send message" << std::endl;

		auto as = actor::ActorSystem::createActorSystem(U("ActorSystemTest"));
		TSTASSERT(as, "ActorSystem is null");
		auto a1 = actorsystest::Actor1::createActor1(U("Actor1"), actor::ActorSystemRefWeak(as));

		auto m = common::string_t(U("req1"));
		auto mm = MAKE_MESSAGE_NOSENDER(actorsystest::Message1, m);
		a1->tell(mm);
		a1->_completed.wait(10000);
		TSTASSERT(a1->_messageRecvd.size()>0 , "Messages are empty");
		TSTASSERT(a1->_messageRecvd[0]==m,"Message does not match");

		as->terminate();
		TSTASSERT(as->onTerminated(waitToTerm).get(), "Not terminated");
		TSTASSERT(a1->onTerminated(waitToTerm).get(), "Actor1 is not terminated");

		return true;
	}
	)),
		std::pair<std::string, TestFunc>(testName("test5"), TestFunc([]()
	{
		std::cout << "test5: Cannot send msg if actor is terminated" << std::endl;

		auto as = actor::ActorSystem::createActorSystem(U("ActorSystemTest"));
		TSTASSERT(as, "ActorSystem is null");
		auto a1 = actorsystest::Actor1::createActor1(U("Actor1"), actor::ActorSystemRefWeak(as));

		auto m = common::string_t(U("req1"));
		auto mm = MAKE_MESSAGE_NOSENDER(actorsystest::Message1, m);
		
		a1->terminate();
		TSTASSERT(a1->onTerminated(waitToTerm).get(), "Not terminated");

		a1->tell(mm);
		a1->_completed.wait(waitToTerm);
		TSTASSERT(a1->_messageRecvd.size()==0, "Messages are not empty");

		as->terminate();
		TSTASSERT(as->onTerminated(waitToTerm).get(), "Not terminated");

		return true;
	}
	)),
		std::pair<std::string, TestFunc>(testName("test6"), TestFunc([]()
	{
		std::cout << "test6: Create ActorSystem + one actor + send non-matching message" << std::endl;

		auto as = actor::ActorSystem::createActorSystem(U("ActorSystemTest"));
		TSTASSERT(as, "ActorSystem is null");
		auto a1 = actorsystest::Actor1::createActor1(U("Actor1"), actor::ActorSystemRefWeak(as));

		auto m = common::string_t(U("req1"));
		auto mm = MAKE_MESSAGE_NOSENDER(actorsystest::Message2, m);
		a1->tell(mm);
		a1->_completed.wait(waitToTerm);
		TSTASSERT(a1->_messageRecvd.size() == 0, "Messages are not empty");

		as->terminate();
		TSTASSERT(as->onTerminated(waitToTerm).get(), "Not terminated");
		TSTASSERT(a1->onTerminated(waitToTerm).get(), "Actor1 is not terminated");

		return true;
	}
	)),
		std::pair<std::string, TestFunc>(testName("test7"), TestFunc([]()
	{
		std::cout << "test7: Send 3 messages one non matching" << std::endl;

		auto as = actor::ActorSystem::createActorSystem(U("ActorSystemTest"));
		TSTASSERT(as, "ActorSystem is null");
		auto a1 = actorsystest::Actor1::createActor1(U("Actor1"), actor::ActorSystemRefWeak(as));

		auto m = common::string_t(U("req1"));
		auto m1 = common::string_t(U("req2"));


		std::vector<actor::MessageRef> mm;
		mm.push_back(MAKE_MESSAGE_NOSENDER(actorsystest::Message1, m)),
		mm.push_back(MAKE_MESSAGE_NOSENDER(actorsystest::Message2, m)),
		mm.push_back(MAKE_MESSAGE_NOSENDER(actorsystest::Message1, m1)),


		std::for_each(mm.begin(), mm.end(), [a1](const actor::MessageRef& p){
			a1->tell(p); });

		std::this_thread::sleep_for(std::chrono::milliseconds(waitToTerm));
		TSTASSERT(a1->_messageRecvd.size() == 2, "Wrong message size: " << a1->_messageRecvd.size());
		TSTASSERT(a1->_messageRecvd[0] == m, "Message1 does not match");
		TSTASSERT(a1->_messageRecvd[1] == m1, "Message2 does not match");

		as->terminate();
		TSTASSERT(as->onTerminated(waitToTerm).get(), "Not terminated");
		TSTASSERT(a1->onTerminated(waitToTerm).get(), "Actor1 is not terminated");

		return true;
	}
	)),
		std::pair<std::string, TestFunc>(testName("test8"), TestFunc([]()
	{
		std::cout << "test8: Create ActorSystem + one actor + send message to throw an exception" << std::endl;

		auto as = actor::ActorSystem::createActorSystem(U("ActorSystemTest"));
		TSTASSERT(as, "ActorSystem is null");
		auto a1 = actorsystest::Actor1::createActor1(U("Actor1"), actor::ActorSystemRefWeak(as));

		auto m = common::string_t(U("terminate"));
		auto mm = MAKE_MESSAGE_NOSENDER(actorsystest::Message1, m);
		a1->tell(mm);
		TSTASSERT(as->onTerminated(waitToTerm).get(), "Terminaiton failed");
		TSTASSERT(a1->onTerminated(waitToTerm).get(), "Actor1 is not terminated");

		return true;
	}
	)),
		std::pair<std::string, TestFunc>(testName("test9"), TestFunc([]()
	{
		std::cout << "test9: Create ActorSystem + one actor + send message to hand actor, fail to terminate" << std::endl;

		auto as = actor::ActorSystem::createActorSystem(U("ActorSystemTest"));
		TSTASSERT(as, "ActorSystem is null");
		auto a1 = actorsystest::Actor1::createActor1(U("Actor1"), actor::ActorSystemRefWeak(as));

		auto m = common::string_t(U("hang"));
		auto mm = MAKE_MESSAGE_NOSENDER(actorsystest::Message1, m);
		a1->tell(mm);
		as->terminate();
		TSTASSERT(!as->onTerminated(waitToTerm).get(), "Not terminated");
		TSTASSERT(!a1->onTerminated(waitToTerm).get(), "Actor1 is  terminated succesfully");

		return true;
	}
	)),
		std::pair<std::string, TestFunc>(testName("test10"), TestFunc([]()
	{
		std::cout << "test10: Create ActorSystem + one actor + send messages to hand actor, terminate succesfully" << std::endl;

		auto as = actor::ActorSystem::createActorSystem(U("ActorSystemTest"));
		TSTASSERT(as, "ActorSystem is null");
		auto a1 = actorsystest::Actor1::createActor1(U("Actor1"), actor::ActorSystemRefWeak(as));

		auto m = common::string_t(U("sleep"));
		auto mm = MAKE_MESSAGE_NOSENDER(actorsystest::Message1, m);
		for (int i = 0; i < 100; i++){ a1->tell(mm); }
		as->terminate();
		TSTASSERT(as->onTerminated(waitToTerm).get(), "Not terminated");
		TSTASSERT(a1->onTerminated(waitToTerm).get(), "Actor1 is  terminated succesfully");

		return true;
	}
	)),
		std::pair<std::string, TestFunc>(testName("test11"), TestFunc([]()
	{
		std::cout << "test11: Create ActorSystem + one actor + actor wait for task cancel, terminate succesfully" << std::endl;

		auto as = actor::ActorSystem::createActorSystem(U("ActorSystemTest"));
		TSTASSERT(as, "ActorSystem is null");
		auto a1 = actorsystest::Actor1::createActor1(U("Actor1"), actor::ActorSystemRefWeak(as));

		auto m = common::string_t(U("check"));
		auto mm = MAKE_MESSAGE_NOSENDER(actorsystest::Message1, m);
		a1->tell(mm); 
		std::this_thread::sleep_for(std::chrono::milliseconds(2000));
		as->terminate();
		TSTASSERT(as->onTerminated(waitToTerm).get(), "Not terminated");
		TSTASSERT(a1->onTerminated(waitToTerm).get(), "Actor1 is  terminated succesfully");

		return true;
	}
	)),
		std::pair<std::string, TestFunc>(testName("test12"), TestFunc([]()
	{
		std::cout << "test12: Create ActorSystem + one actor + send message to throw an exception, restart" << std::endl;

		auto p = actorsystest::createActorsystem1();
		TSTASSERT(p->first, "ActorSystem is null");
		auto m = common::string_t(U("terminate"));
		auto mm = MAKE_MESSAGE_NOSENDER(actorsystest::Message1, m);
		p->second->tell(mm);
		auto r = p->first->onTerminated(waitToTerm).then([&p](pplx::task<bool>&t){
			t.get();
			p =actorsystest::createActorsystem1();
			TSTASSERT(p->first, "ActorSystem is null");
			auto m = common::string_t(U("check"));
			auto mm = MAKE_MESSAGE_NOSENDER(actorsystest::Message1, m);
			p->second->tell(mm); 
			std::this_thread::sleep_for(std::chrono::milliseconds(2000));
			p->first->terminate();
			TSTASSERT(p->first->onTerminated(waitToTerm).get(), "Not terminated");
			TSTASSERT(p->second->onTerminated(waitToTerm).get(), "Actor1 is  terminated succesfully");
			return true;
		}).get();
		TSTASSERT(r, "Invalid result");

		return true;
	}
	)),
		std::pair<std::string, TestFunc>(testName("test13"), TestFunc([]()
	{
		std::cout << "test13: Create ActorSystem + one actor + send message to throw an exception, cacth exception in the handler" << std::endl;

		auto as = actor::ActorSystem::createActorSystem(U("ActorSystemTest"));
		TSTASSERT(as, "ActorSystem is null");
		auto a1 = actorsystest::Actor1::createActor1(U("Actor1"), actor::ActorSystemRefWeak(as));

		auto m = common::string_t(U("exchcatch"));
		auto mm = MAKE_MESSAGE_NOSENDER(actorsystest::Message1, m);
		a1->tell(mm);
		TSTASSERT(!as->onTerminated(waitToTerm).get(), "Terminated");
		TSTASSERT(!a1->onTerminated(waitToTerm).get(), "Actor1 is terminated");

		return true;
	}
	)),
		std::pair<std::string, TestFunc>(testName("test14"), TestFunc([]()
	{
		std::cout << "test14: MEssages a dropped if mailbox is full" << std::endl;

		auto as = actor::ActorSystem::createActorSystem(U("ActorSystemTest"));
		TSTASSERT(as, "ActorSystem is null");
		int msz = 2;
		auto a1 = actorsystest::Actor1::createActor1(U("Actor1"), actor::ActorSystemRefWeak(as), actor::ActorBaseConfig(msz));

		auto m = common::string_t(U("msgcount"));
		auto mm = MAKE_MESSAGE_NOSENDER(actorsystest::Message1, m);
		for (int i = 0; i < 100; i++){ a1->tell(mm); }
		std::this_thread::sleep_for(std::chrono::milliseconds(10000));
		TSTASSERT(a1->_msgcount >= msz && a1->_msgcount<100, "Invalid num of messages")
		as->terminate();
		TSTASSERT(as->onTerminated(waitToTerm).get(), "Not terminated");
		TSTASSERT(a1->onTerminated(waitToTerm).get(), "Actor1 is  terminated succesfully");

		return true;
	}
	)),
	
};



