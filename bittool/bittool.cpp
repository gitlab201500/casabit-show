#ifdef WIN32
#include "stdafx.h"
#endif //WIN32
#include "commondefs.h"
#include <vector>
#include <chrono>
#include <typeinfo>
#include <thread>
#include <algorithm>
#include <map>
#include <chrono>
#include <iostream>
#include <functional>
#include "testcommon.h"
#include "exchanges.h"
#include "testbitflyer.h"


void usage()
{
	std::cout << "Usage bittool rt --exchange <name>" << std::endl;
	std::cout << "Usage bittool rate --exchange <name> --rate <events-per-min>" << std::endl;
}

using namespace exchangelib;
using namespace common;

struct CmdLineCfg{
	CmdLineCfg() :rate(500){}
	string_t cmd;
	string_t exchange;
	int rate;
};

void rtTest(const CmdLineCfg& cfg)
{
	xcout << U("Roundtrip test for exchange ") << cfg.exchange << std::endl;
	int nevents = 100;
	Pair pair=ETH_BTC;
	auto fact = exchangelib::x_exchangeCollection.at(cfg.exchange)->depthFactory();

	auto f = [pair, fact](){
		return fact(pair, DepthCfg());
	};
	auto res = testcommon::roundtripTest<DepthRef>(nevents, pair, f);
	auto stats = res.first;
	auto fail = res.second;

	std::cout << "Events: " << nevents;
	std::cout << " RT time (ms). Average: " << stats.mean() << " dev: " << stats.dev() << " min:" << stats.min() << " max: " << stats.max() << " successes:" << stats.count() << std::endl;
	std::cout << "Failure rate (%): " << (double)(fail / nevents * 100.0) << std::endl;
}

void rateTest(const CmdLineCfg& cfg)
{
	xcout << U("rate test for exchange ") << cfg.exchange << std::endl;
	Pair pair=ETH_BTC;

	auto fact = exchangelib::x_exchangeCollection.at(cfg.exchange)->depthFactory();

	auto f = [pair, fact](){
		return fact(pair, DepthCfg());
	};

	int delayMs = (int) (60.0 / cfg.rate * 1000.0);
	int nevents = cfg.rate;
	auto res = testcommon::rateTest<DepthRef>(std::chrono::milliseconds(delayMs), nevents, pair, f);
	auto stats = res.first;
	auto fail = res.second;

	std::cout << "Events: " << nevents << " delay: " << delayMs;
	std::cout << " RT time (ms). Average: " << stats.mean() << " dev: " << stats.dev() << " min:" << stats.min() << " max: " << stats.max() <<  " successes:"<<stats.count()<< std::endl;
	std::cout << "Failure rate (%): " << ((double)fail / nevents * 100.0) << std::endl;
}

#ifdef WIN32
int wmain(int argc, common::char_t* argv[])
#else
int main(int argc, common::char_t* argv[])
#endif //WIN32
{
	if (argc < 3)
	{
		usage();
		return EXIT_FAILURE;
	}

	CmdLineCfg cfg;
	cfg.cmd = argv[1];

	for (int i = 2; i < argc;)
	{
		if (string_t(argv[i]) == string_t(U("--exchange")) && i + 1<argc)
		{
			cfg.exchange = argv[++i];
		}
		else if (string_t(argv[i]) == string_t(U("--rate")) && i + 1<argc)
		{
			cfg.rate = std::stoi(argv[++i]);
		}
		else
		{
			std::cerr << "invalid option" << std::endl;
			usage();
			return EXIT_FAILURE;
		}
		i++;
	}

	if (cfg.cmd == U("rt")){
		rtTest(cfg);
	}
	else if (cfg.cmd == U("rate")){
		rateTest(cfg);
	}
}
