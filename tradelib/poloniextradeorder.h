#ifndef __POLONIEXTRADEORDER_H__
#define __POLONIEXTRADEORDER_H__
#include <http_client.h>
#include <iomanip>
#include <pplxtasks.h>
#include "poloniexconst.h"
#include "logger.h"
#include "commondefs.h"
#include "tradeorder.h"

namespace tradelib
{
	namespace poloniex{
		class TradeOrderQuery :public TradeOrderQueryBase{
		public:
			TradeOrderQuery(const TradeOrderIn& in,
				const TradeOrderCfg& cfg
				) :TradeOrderQueryBase(&exchangelib::poloniex::x_poloniexConstants, in, cfg){}

			virtual common::string_t url() { return U("https://poloniex.com"); }
			virtual TradeOrderRef parseJson(const web::json::value& json);
			virtual web::http::http_request request();
		private:

			static const int x_presizion = 6;
			string_t req() {
				stringstream_t rate;
				rate<<std::setprecision(x_presizion) << _in.rate;
				stringstream_t amount;
				amount << std::setprecision(x_presizion) << _in.amount;
				stringstream_t ss;
				ss << U("nonce=") << _nonce << U("&amount=") << amount.str() << U("&rate=") << rate.str() << U("&command=") <<
					(_in.type == OrderType::Buy ? U("buy") : U("sell")) <<
					U("&currencyPair=") << _pairInv <<
					(_in.fillOrKill ? U("&fillOrKill=1") : U("&fillOrKill=0")) <<
					(_in.immediateOrCancel ? U("&immediateOrCancel=1") : U("&immediateOrCancel=0"));
				return ss.str();
			}
			common::string_t path() { return U("/tradingApi"); }
			common::string_t method() { return U("POST"); }
			string_t sign();
		};
	}
}

#endif // __POLONIEXTRADEORDER_H__