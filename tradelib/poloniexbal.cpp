#include <http_client.h>
#include <string>
#include "sha512.h"
#include "commonutil.h"
#include "exchutil.h"
#include "poloniexbal.h"


namespace tradelib
{
	namespace poloniex{

		web::http::http_request BalanceQuery::request(){
			_nonce = std::chrono::duration_cast<std::chrono::milliseconds>
				(std::chrono::system_clock::now().time_since_epoch()).count();
			web::http::http_request r(method());
			r.set_request_uri(path());
			r.headers().add(U("Sign"), sign());
			r.headers().add(U("Key"), _cfg.apiKey->key);
			r.set_body(req(), U("application/x-www-form-urlencoded"));
			return r;
		}

		string_t BalanceQuery::sign(){
			common::stringstream_t ss;
			ss << req();
			return s2ws(hashlib::hmac512(ws2s(ss.str()), ws2s(_cfg.apiKey->secret)));
		}

		BalanceSeqRef BalanceQuery::parseJson(const web::json::value& json){
			auto ret = std::make_shared<std::vector<Balance>>();
			auto o = GETASORELSE(json, object, throw std::exception("Invalid json: balance response is not an object"));
			auto m = _constants->symbolsInv();
			for (auto e : o){
				if (e.first == U("error")) throw std::exception(ws2s(e.second.as_string()).c_str());
				auto currency_code = e.first;
				if (std::find(_symbolsInv.cbegin(), _symbolsInv.cend(), currency_code) == _symbolsInv.cend()) continue;
				ret->push_back(Balance(
					m[currency_code],
					std::stod(GETASORELSE(e.second, string, throw std::exception("Invalid json: balance value is not a string")))
					));
			}
			return ret;
		}
	}

}