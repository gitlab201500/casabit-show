#include <http_client.h>
#include "commonutil.h"
#include "exchutil.h"
#include "tradeorder.h"

namespace tradelib
{
	DEFINE_LOGGER(TradeOrderQueryBase,INFO);
	pplx::task<TradeOrderRef> TradeOrderQueryBase::fetch(){
		web::http::http_request req = request();
		web::http::client::http_client client(url());
		auto thisPtr = shared_from_this();
		_start = std::chrono::system_clock::now();
		return client.request(req).then([thisPtr](web::http::http_response response)
		{
			if (response.status_code() != web::http::status_codes::OK)
			{
				thisPtr->_end = std::chrono::system_clock::now();
				common::stringstream_t ss;
				ss << U("Failed HTTP request: ") << response.status_code();
				throw std::exception(common::ws2s(ss.str()).c_str());
			}
			auto js = exchangelib::extractJson(response);
			logDebug(logTradeOrderQueryBase, js.serialize());
			return thisPtr->parseJson(js);
		});
	}
}