#ifndef __TRADEAPICOLL_H__
#define __TRADEAPICOLL_H__
#include <map>
#include "exchconst.h"
#include "tradeapi.h"
namespace tradelib
{
	extern const std::map<common::string_t, TradeApiRef> x_tradeApiCollection;
}

#endif //__TRADEAPICOLL_H__