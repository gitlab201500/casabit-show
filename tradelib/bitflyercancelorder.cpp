#include <http_client.h>
#include <string>
#include "sha256.h"
#include "hmac.h"
#include "commonutil.h"
#include "exchutil.h"
#include "bitflyercancelorder.h"


namespace tradelib
{
	namespace bitflyer{
		DEFINE_LOGGER(CancelOrderQueryBitflyer, INFO);
		web::http::http_request CancelOrderQuery::request(){
			web::http::http_request r(method());
			r.set_request_uri(path());
			r.headers().add(U("ACCESS-SIGN"), sign());
			r.headers().add(U("ACCESS-KEY"), _cfg.apiKey->key);

			stringstream_t ss;
			ss << _nonce;
			r.headers().add(U("ACCESS-TIMESTAMP"), ss.str());
			auto rq = req();
			r.set_body(rq, U("application/json"));
			logDebug(logCancelOrderQueryBitflyer, U("uri: ") << url() << path() << U(" req: ") << rq);
			return r;
		}

		string_t CancelOrderQuery::sign(){
			common::stringstream_t ss;
			ss << _nonce << method() << path()<<req();
			return s2ws(hmac<SHA256>(ws2s(ss.str()), ws2s(_cfg.apiKey->secret)));
		}

		string_t CancelOrderQuery::req(){
			auto js = web::json::value::object();
			js[U("product_code")] = web::json::value::string(_pairInv);
			js[U("child_order_acceptance_id")] = web::json::value::string(_orderNumber);
			return js.serialize();
		}

		CancelOrderRef CancelOrderQuery::parseJson(const web::json::value& json){
			logDebug(logCancelOrderQueryBitflyer, U("Reply: ") << json);
			return std::make_shared<CancelOrder>(_orderNumber, true);
		}

	}


}