#include <http_client.h>
#include <string>
#include "sha512.h"
#include "commonutil.h"
#include "exchutil.h"
#include "poloniexorderstatus.h"


namespace tradelib
{
	namespace poloniex{
		template<OrderStatusQueryBase::Kind kind>
		web::http::http_request OrderStatusQueryTemplate<kind>::request(){
			web::http::http_request r(method());
			r.set_request_uri(path());
			r.headers().add(U("Sign"), sign());
			r.headers().add(U("Key"), _cfg.apiKey->key);
			r.set_body(req(), U("application/x-www-form-urlencoded"));
			return r;
		}

		template<OrderStatusQueryBase::Kind kind>
		string_t OrderStatusQueryTemplate<kind>::sign(){
			common::stringstream_t ss;
			ss << req();
			return s2ws(hashlib::hmac512(ws2s(ss.str()), ws2s(_cfg.apiKey->secret)));
		}

		template<OrderStatusQueryBase::Kind kind>
		OrderStatusRef OrderStatusQueryTemplate<kind>::parseJson(const web::json::value& json){
			auto ret = std::make_shared<OrderStatus>(_pair);
			if (json.is_object()){
				auto o = json.as_object();
				for (auto e : o){
					if (e.first == U("error")) throw std::exception(ws2s(e.second.as_string()).c_str());
					else throw std::exception("Invalid json: unexpected object");
				}
			}

			auto o = GETASORELSE(json, array, throw std::exception("Invalid json: order response is not an array"));
			for (auto e : o){
				auto o1 = GETASORELSE(e, object, throw std::exception("Invalid json: balance element is not an object"));
				ret->infos.push_back(OrderInfo(
					GETFIELDORELSE(o1, orderNumber, string, throw std::exception("Invalid json: balance contains no child_order_acceptance_id field")),
					(GETFIELDORELSE(o1, type, string, throw std::exception("Invalid json: balance contains no side field"))) == U("buy") ? OrderType::Buy : OrderType::Sell,
					std::stod(GETFIELDORELSE(o1, rate, string, throw std::exception("Invalid json: balance contains no rate field"))),
					std::stod(GETFIELDORELSE(o1, amount, string, throw std::exception("Invalid json: balance contains no size field"))),
					std::stod(GETFIELDORELSE(o1, total, string, throw std::exception("Invalid json: balance contains no total field")))
				));
			}
			return ret;
		}
		template OrderStatusQueryTemplate<OrderStatusQueryBase::Kind::Open>;
		template OrderStatusQueryTemplate<OrderStatusQueryBase::Kind::Closed>;

	}

}