#ifndef __BITFLYERCANCELORDER_H__
#define __BITFLYERCANCELORDER_H__
#include <http_client.h>
#include <pplxtasks.h>
#include "bitflyerconst.h"
#include "logger.h"
#include "commondefs.h"
#include "cancelorder.h"

namespace tradelib
{
	namespace bitflyer{
		class CancelOrderQuery :public CancelOrderQueryBase{
		public:
			CancelOrderQuery(const CancelOrderIn& in,
				const CancelOrderCfg& cfg
				) :CancelOrderQueryBase(&exchangelib::bitflyer::x_bitflyerConstants, in, cfg){}

			virtual common::string_t url() { return U("https://api.bitflyer.jp"); }
			virtual CancelOrderRef parseJson(const web::json::value& json);
			virtual web::http::http_request request();
		private:
			string_t req();
			common::string_t path() {return U("/v1/me/cancelchildorder"); }
			common::string_t method() { return U("POST"); }
			string_t sign();
		};
	}
}

#endif // __BITFLYERCANCELORDER_H__