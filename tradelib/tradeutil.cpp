#include <map>
#include <chrono>
#include <fstream>
#include <sstream>
#include <string>
#include <cpprest/json.h>
#include "logger.h"
#include "exchutil.h"
#include "tradeutil.h"

namespace tradelib
{
	using namespace common;
	std::map<string_t, ApiKeyRef> readApiKeys(const string_t& file){
		ifstream_t infile(file);
		string_t line;
		std::map<string_t, ApiKeyRef> r;
		while (std::getline(infile, line))
		{
			auto json = web::json::value::parse(line);
			auto o = GETASORELSE(json, object, throw std::exception("Invalid json: apikey entry is not an object"));
			auto e = GETFIELDORELSE(o, exchange, string, throw std::exception("Invalid json: apikey entry contains no exchange field"));
			auto k = GETFIELDORELSE(o, key, string, throw std::exception("Invalid json: apikey entry contains no key field"));
			auto s = GETFIELDORELSE(o, secret, string, throw std::exception("Invalid json: apikey entry contains no secret field"));
			r[e]=std::make_shared<ApiKey>(e,k,s);
		}
		return r;
	}

	ApiKeyRef readApiKey(const string_t& file, const string_t& exchange){
		auto keys = readApiKeys(file);
		auto f = keys.find(exchange);
		if (f == keys.end()) { std::stringstream ss; ss << "No key for exchange: " << ws2s(exchange); throw  std::exception(ss.str().c_str()); }
		else return f->second;
	}


}
