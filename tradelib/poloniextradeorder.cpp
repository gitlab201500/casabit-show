#include <http_client.h>
#include <string>
#include "sha512.h"
#include "commonutil.h"
#include "exchutil.h"
#include "poloniextradeorder.h"


namespace tradelib
{

	namespace poloniex{
		DEFINE_LOGGER(TradeOrderQueryPoloniex,INFO);
		web::http::http_request TradeOrderQuery::request(){
			_nonce = std::chrono::duration_cast<std::chrono::milliseconds>
				(std::chrono::system_clock::now().time_since_epoch()).count();
			logDebug(logTradeOrderQueryPoloniex, U("nonce: ") << _nonce);
			web::http::http_request r(method());
			r.set_request_uri(path());
			r.headers().add(U("Sign"), sign());
			r.headers().add(U("Key"), _cfg.apiKey->key);
			auto rq = req();
			r.set_body(rq, U("application/x-www-form-urlencoded"));
			logDebug(logTradeOrderQueryPoloniex, U("uri: ") << url() << path() << U(" req: ") << rq);
			return r;
		}

		string_t TradeOrderQuery::sign(){
			common::stringstream_t ss;
			ss << req();
			return s2ws(hashlib::hmac512(ws2s(ss.str()), ws2s(_cfg.apiKey->secret)));
		}

		TradeOrderRef TradeOrderQuery::parseJson(const web::json::value& json){
			logDebug(logTradeOrderQueryPoloniex, U("Reply: ") << json);
			auto o = GETASORELSE(json, object, throw std::exception("Invalid json: tradeOrder response is not an object"));
			auto err = o.find(U("error"));
			if (err != o.end()) throw std::exception(ws2s(err->second.as_string()).c_str());
			auto on = GETFIELDORELSE(o, orderNumber, string, throw std::exception("Invalid json: tradeOrder has no orderNumber field"));
			auto rt = GETFIELDORELSE(o, resultingTrades, array, throw std::exception("Invalid json: tradeOrder has no resultingTrades field"));
			auto ret = std::make_shared<TradeOrder>(_in.pair, TradeOrderInfo(on, {}));
			for (auto e : rt){
				auto o1 = GETASORELSE(e, object, throw std::exception("Invalid json: resultingTrades response is not an object"));
				ret->info.trades.push_back(TradeOrderData(
					GETFIELDORELSE(o1, date, string, throw std::exception("Invalid json: resultingTrades has no date field")),
					std::stod(GETFIELDORELSE(o1, amount, string, throw std::exception("Invalid json: resultingTrades has no amount field"))),
					std::stod(GETFIELDORELSE(o1, rate, string, throw std::exception("Invalid json: resultingTrades has no rate field"))),
					std::stod(GETFIELDORELSE(o1, total, string, throw std::exception("Invalid json: resultingTrades has no total field"))),
					GETFIELDORELSE(o1, tradeID, string, throw std::exception("Invalid json: resultingTrades has no id field")),
					((GETFIELDORELSE(o1, type, string, throw std::exception("Invalid json: id has no type field"))) == U("buy")) ? OrderType::Buy : OrderType::Sell
					));
			}
			return ret;
		}
	}


}