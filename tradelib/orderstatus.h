#ifndef __ORDERSTATUS_H__
#define __ORDERSTATUS_H__
#include <http_client.h>
#include <pplxtasks.h>
#include <algorithm>
#include "logger.h"
#include "commondefs.h"
#include "exchconst.h"
#include "tradeentity.h"

namespace tradelib
{
	class OrderStatusQueryBase :public TradeQuery<pplx::task<OrderStatusRef>>, public std::enable_shared_from_this<OrderStatusQueryBase>{
	public:
		enum Kind{Open,Closed};
		OrderStatusQueryBase(const exchangelib::ExchangeConstants* constants,
			Pair pair,
			const OrderStatusCfg& cfg
			) :_pair(pair), _cfg(cfg), _nonce(
			std::chrono::duration_cast<std::chrono::milliseconds>
			(std::chrono::system_clock::now().time_since_epoch()).count()){
			assert(constants);
			auto m = constants->pairs();
			if (m.find(_pair) == m.end()) throw std::exception(common::ws2s(U("Invalid pair: ") + _pair).c_str());
			_pairInv = m[_pair];

		}
		virtual ~OrderStatusQueryBase(){}
		virtual pplx::task<OrderStatusRef> fetch();

	protected:
		virtual web::http::http_request request() = 0;
		virtual common::string_t url() = 0;
		virtual OrderStatusRef parseJson(const web::json::value& json) = 0;
		const Pair _pair;
		common::string_t _pairInv;
		const OrderStatusCfg _cfg;
		long long _nonce;
	};
}

#endif // __ORDERSTATUS_H__