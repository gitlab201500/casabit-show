#include <http_client.h>
#include <string>
#include "sha256.h"
#include "hmac.h"
#include "commonutil.h"
#include "exchutil.h"
#include "bitflyerbal.h"

namespace tradelib
{
	namespace bitflyer{

		web::http::http_request BalanceQuery::request(){
			_nonce = std::chrono::duration_cast<std::chrono::milliseconds>
				(std::chrono::system_clock::now().time_since_epoch()).count();
			web::http::http_request r(method());
			r.set_request_uri(path());
			r.headers().add(U("ACCESS-SIGN"), sign());
			r.headers().add(U("ACCESS-KEY"), _cfg.apiKey->key);

			stringstream_t ss;
			ss << _nonce;
			r.headers().add(U("ACCESS-TIMESTAMP"), ss.str());
			return r;
		}

		string_t BalanceQuery::sign(){
			common::stringstream_t ss;
			ss << _nonce << method() << path();
			return s2ws(hmac<SHA256>(ws2s(ss.str()), ws2s(_cfg.apiKey->secret)));
		}

		BalanceSeqRef BalanceQuery::parseJson(const web::json::value& json){
			auto ret = std::make_shared<std::vector<Balance>>();
			auto o = GETASORELSE(json, array, throw std::exception("Invalid json: balance response is not an array"));
			auto m = _constants->symbolsInv();
			for (auto e : o){
				auto o1 = GETASORELSE(e, object, throw std::exception("Invalid json: balance element is not an object"));
				auto currency_code = GETFIELDORELSE(o1, currency_code, string, throw std::exception("Invalid json: balance contains no currency_code field"));
				if (std::find(_symbolsInv.cbegin(), _symbolsInv.cend(), currency_code) == _symbolsInv.cend()) continue;
				ret->push_back(Balance(
					m[currency_code],
				GETFIELDASNUMBER(o1, available, throw std::exception("Invalid json: balance response contains no available field"))
					));
			}
			return ret;
		}
	}

}