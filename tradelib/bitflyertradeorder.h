#ifndef __BITFLYERTRADEORDER_H__
#define __BITFLYERTRADEORDER_H__
#include <http_client.h>
#include <pplxtasks.h>
#include "bitflyerconst.h"
#include "logger.h"
#include "commondefs.h"
#include "tradeorder.h"

namespace tradelib
{
	namespace bitflyer{
		class TradeOrderQuery :public TradeOrderQueryBase{
		public:
			TradeOrderQuery(const TradeOrderIn& in,
				const TradeOrderCfg& cfg
				) :TradeOrderQueryBase(&exchangelib::bitflyer::x_bitflyerConstants, in, cfg){}

			virtual common::string_t url() { return U("https://api.bitflyer.jp"); }
			virtual TradeOrderRef parseJson(const web::json::value& json);
			virtual web::http::http_request request();
		private:
			static const int x_presizion = 6;
			string_t req();
			common::string_t path() { return U("/v1/me/sendchildorder"); }
			common::string_t method() { return U("POST"); }
			string_t sign();
		};
	}
}

#endif // __BITFLYERTRADEORDER_H__