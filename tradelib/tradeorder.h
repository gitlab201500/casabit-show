#ifndef __TRADEORDER_H__
#define __TRADEORDER_H__
#include <http_client.h>
#include <pplxtasks.h>
#include <algorithm>
#include "logger.h"
#include "commondefs.h"
#include "exchconst.h"
#include "tradeentity.h"
#include "tradeorder.h"

namespace tradelib
{
	class TradeOrderQueryBase :public TradeQuery<pplx::task<TradeOrderRef>>, public std::enable_shared_from_this<TradeOrderQueryBase>{
	public:
		TradeOrderQueryBase(const exchangelib::ExchangeConstants* constants,
			const TradeOrderIn& in,
			const TradeOrderCfg& cfg
			) :_in(in), _cfg(cfg), _nonce(
			std::chrono::duration_cast<std::chrono::milliseconds>
			(std::chrono::system_clock::now().time_since_epoch()).count()){
			assert(constants);
			auto m = constants->pairs();
			if (m.find(in.pair) == m.end()) throw std::exception(common::ws2s(U("Invalid pair: ") + _in.pair).c_str());
			_pairInv = m[_in.pair];
		}
		virtual ~TradeOrderQueryBase(){}
		virtual pplx::task<TradeOrderRef> fetch();

	protected:
		virtual web::http::http_request request() = 0;
		virtual common::string_t url() = 0;
		virtual TradeOrderRef parseJson(const web::json::value& json) = 0;
		const TradeOrderIn _in;
		common::string_t _pairInv;
		const TradeOrderCfg _cfg;
		long long _nonce;
	};
}

#endif // __TRADEORDER_H__