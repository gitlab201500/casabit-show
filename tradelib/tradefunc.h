#ifndef __TRADEFUNC__
#define __TRADEFUNC__
#include "commondefs.h"
#include <vector>
#include <functional>
#include "pplxtasks.h"
#include <map>

namespace tradelib{
	template<class T>
	using BatchTryResultsRef = std::shared_ptr<std::vector<common::TryResult<T>>>;

	template<class T>
	static void recTask(std::shared_ptr<const std::vector<std::shared_ptr<TradeQuery<pplx::task<T>>>>> qv, size_t i, 
		std::shared_ptr<pplx::extensibility::event_t> evt, __out BatchTryResultsRef<T> ret){
		assert(qv);
		if (i >= qv->size()){ evt->set(); return; }
		else {
			(*qv)[i]->fetch().then([ret,qv,evt,i](pplx::task<T>&t){
				auto r = exchangelib::Try(std::function<T()>([&t](){
					return t.get();
				}));
				ret->push_back(r);
				recTask(qv, i + 1, evt, ret);
			});

		}
	}

	template<class T>
	pplx::task<BatchTryResultsRef<T>> queryBatch(std::shared_ptr<const std::vector<std::shared_ptr<TradeQuery<pplx::task<T>>>>> qv){
		auto evt = std::make_shared<pplx::extensibility::event_t>();
		auto ret = std::make_shared<std::vector<common::TryResult<T>>>();
		return pplx::create_task([evt,qv,ret](){
			recTask(qv, 0, evt,ret);
			evt->wait();
			return ret;
		});
	}

	template <class T>
	using TradeQueryFunc = std::function<TradeQueryRef<pplx::task<T>>()>;


	inline void launchDelayedTask(std::chrono::milliseconds delay, size_t count, std::shared_ptr<std::vector<std::function<void()>>> payloads){
		if (count >= payloads->size()) return;
		((*payloads)[count])();

		common::create_delayed_task(delay, [delay, count, payloads](){

			//std::cout << "delayed task: " << static_cast<int>(delay.count())<< " "<<std::chrono::duration_cast<std::chrono::milliseconds>
			//	(std::chrono::system_clock::now().time_since_epoch()).count() << std::endl;

			launchDelayedTask(delay, count + 1, payloads);
		}).then([](pplx::task<void>&t){try{ t.get(); } catch (...){ throw std::exception("Unexpected exception in create_delayed_task handler"); }});
	}

	template<class T>
	pplx::task<BatchTryResultsRef<T>> queryBatchWithDelay(const std::vector<TradeQueryFunc<T>>& qv,
		std::chrono::milliseconds delay = std::chrono::milliseconds(200)){
		auto cnt = std::make_shared<std::atomic<size_t>>(qv.size());
		auto evt = std::make_shared<pplx::extensibility::event_t>();
		auto ret = std::make_shared<std::vector<common::TryResult<T>>>(qv.size());
		size_t count = 0;
		auto payloads = std::make_shared<std::vector<std::function<void()>>>();
		for (size_t i = 0; i < qv.size(); i++){
			auto q = qv[i];
			payloads->push_back(
				std::function<void()>([q, i, ret, cnt, evt](){

				q()->fetch().then([i, ret, cnt, evt](pplx::task<T>&t){
					auto r = exchangelib::Try(std::function<T()>([&t](){
						return t.get();
					}));
					(ret->data())[i] = r;
					if (cnt->fetch_sub(1) == 1){
						evt->set();
					}
				}).then([](pplx::task<void>&t){try{ t.get(); } catch (...){ throw std::exception("Unexpected exception in queryBatch::fetch handler"); }});
			}));
		}
		launchDelayedTask(delay, count, payloads);
		return pplx::create_task([evt, ret](){evt->wait();
		return ret;
		});
	}


}
#endif //__TRADEFUNC__