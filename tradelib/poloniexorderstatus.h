#ifndef __POLONIEXORDER_H__
#define __POLONIEXORDER_H__
#include <http_client.h>
#include <pplxtasks.h>
#include "poloniexconst.h"
#include "logger.h"
#include "commondefs.h"
#include "orderstatus.h"

namespace tradelib
{
	namespace poloniex{
		template<OrderStatusQueryBase::Kind kind>
		class OrderStatusQueryTemplate :public OrderStatusQueryBase{
		public:
			OrderStatusQueryTemplate(Pair pair,
				const OrderStatusCfg& cfg
				) :OrderStatusQueryBase(&exchangelib::poloniex::x_poloniexConstants, pair, cfg){}

			virtual common::string_t url() { return U("https://poloniex.com"); }
			virtual OrderStatusRef parseJson(const web::json::value& json);
			virtual web::http::http_request request();
		private:
			string_t req() { stringstream_t ss; ss << U("nonce=") << _nonce << U("&command=") << 
				(kind == OrderStatusQueryBase::Kind::Closed ? U("returnTradeHistory") : U("returnOpenOrders"))
				<< U("&currencyPair=") << _pairInv; return ss.str(); }
			common::string_t path() { return U("/tradingApi"); }
			common::string_t method() { return U("POST"); }
			string_t sign();
		};
		using OrderStatusQuery = OrderStatusQueryTemplate<OrderStatusQueryBase::Kind::Open>;
		using ClosedOrderStatusQuery = OrderStatusQueryTemplate<OrderStatusQueryBase::Kind::Closed>;
	}
}

#endif // __POLONIEXORDER_H__