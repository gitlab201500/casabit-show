#ifndef __POLONIEXBAL_H__
#define __POLONIEXBAL_H__
#include <http_client.h>
#include <pplxtasks.h>
#include "poloniexconst.h"
#include "logger.h"
#include "commondefs.h"
#include "balance.h"

namespace tradelib
{
	using namespace exchangelib;
	namespace poloniex{
		class BalanceQuery :public BalanceQueryBase{
		public:
			BalanceQuery(const std::vector<Symbol>& symbols,
				const BalanceCfg& cfg
				) :BalanceQueryBase(&exchangelib::poloniex::x_poloniexConstants, symbols, cfg){}
			virtual common::string_t url() { return U("https://poloniex.com") ; }
			virtual BalanceSeqRef parseJson(const web::json::value& json);
			virtual web::http::http_request request();
		private:
			string_t req() {stringstream_t ss; ss << U("nonce=") << _nonce << U("&command=returnBalances"); return ss.str();}
			common::string_t path() { return U("/tradingApi"); }
			common::string_t method() { return U("POST"); }
			string_t sign();
		};
	}
}

#endif // __POLONIEXBAL_H__