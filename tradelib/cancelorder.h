#ifndef __CANCELORDER_H__
#define __CANCELORDER_H__
#include <http_client.h>
#include <pplxtasks.h>
#include <algorithm>
#include "logger.h"
#include "commondefs.h"
#include "exchconst.h"
#include "tradeentity.h"

namespace tradelib
{
	using namespace exchangelib;

	class CancelOrderQueryBase :public TradeQuery<pplx::task<CancelOrderRef>>, public std::enable_shared_from_this<CancelOrderQueryBase>{
	public:
		CancelOrderQueryBase(const exchangelib::ExchangeConstants* constants,
			const CancelOrderIn& in,
			const CancelOrderCfg& cfg
			) :_pair(in.pair), _orderNumber(in.orderNumber),_cfg(cfg),  _nonce(
			std::chrono::duration_cast<std::chrono::milliseconds>
			(std::chrono::system_clock::now().time_since_epoch()).count()){
			assert(constants);
			auto m = constants->pairs();
			if (m.find(_pair) == m.end()) throw std::exception(common::ws2s(U("Invalid pair: ") + _pair).c_str());
			_pairInv = m[_pair];
		}
		virtual ~CancelOrderQueryBase(){}
		virtual pplx::task<CancelOrderRef> fetch();

	protected:
		virtual web::http::http_request request() = 0;
		virtual common::string_t url() = 0;
		virtual CancelOrderRef parseJson(const web::json::value& json) = 0;
		const Pair _pair;
		common::string_t _pairInv;
		const CancelOrderCfg _cfg;
		long long _nonce;
		const common::string_t _orderNumber;
	};
}

#endif // __CANCELORDER_H__