#ifndef __BITFLYERORDER_H__
#define __BITFLYERORDER_H__
#include <http_client.h>
#include <pplxtasks.h>
#include "bitflyerconst.h"
#include "logger.h"
#include "commondefs.h"
#include "orderstatus.h"

namespace tradelib
{
	namespace bitflyer{
		template<OrderStatusQueryBase::Kind kind>
		class OrderStatusQueryTemplate :public OrderStatusQueryBase{
		public:
			OrderStatusQueryTemplate(Pair pair,
				const OrderStatusCfg& cfg
				) :OrderStatusQueryBase(&exchangelib::bitflyer::x_bitflyerConstants, pair, cfg){}
			virtual common::string_t url() { return U("https://api.bitflyer.jp"); }
			virtual OrderStatusRef parseJson(const web::json::value& json);
			virtual web::http::http_request request();
		private:
			common::string_t path() {
				stringstream_t ss; ss << U("/v1/me/getchildorders?product_code=") << _pairInv
					<< U("&count=100&before=0&after=0&child_order_state=") << 
					(kind == OrderStatusQueryBase::Kind::Closed ? U("COMPLETED") : U("ACTIVE")); return ss.str();
			}
			common::string_t method() { return U("GET"); }
			string_t sign();
		};
		using OrderStatusQuery = OrderStatusQueryTemplate<OrderStatusQueryBase::Kind::Open>;
		using ClosedOrderStatusQuery = OrderStatusQueryTemplate<OrderStatusQueryBase::Kind::Closed>;

	}
}

#endif // __BITFLYERORDER_H__