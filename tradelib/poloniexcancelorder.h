#ifndef __POLONIEXCANCELORDER_H__
#define __POLONIEXCANCELORDER_H__
#include <http_client.h>
#include <pplxtasks.h>
#include "poloniexconst.h"
#include "logger.h"
#include "commondefs.h"
#include "cancelorder.h"

namespace tradelib
{
	namespace poloniex{
		class CancelOrderQuery :public CancelOrderQueryBase{
		public:
			CancelOrderQuery(const CancelOrderIn& in,
				const CancelOrderCfg& cfg
				) :CancelOrderQueryBase(&exchangelib::poloniex::x_poloniexConstants, in, cfg){}

			virtual common::string_t url() { return U("https://poloniex.com"); }
			virtual CancelOrderRef parseJson(const web::json::value& json);
			virtual web::http::http_request request();
		private:
			string_t req() {
				stringstream_t ss; ss <<
					"nonce=" << _nonce << U("&command=cancelOrder&currencyPair=") << _pairInv << 
					U("&orderNumber=") << _orderNumber; 
				return ss.str();
			}
			common::string_t path() { return U("/tradingApi"); }
			common::string_t method() { return U("POST"); }
			string_t sign();
		};
	}
}

#endif // __POLONIEXCANCELORDER_H__