#ifndef __TEMPLTRADEAPI_H__
#define __TEMPLTRADEAPI_H__
#include "tradeapi.h"

namespace tradelib{
	template <
		class BALANCEQ,
		class ORDERQ,
		class CANCELORDERQ,
		class TRADEORDERQ,
		class CONST>
	class TemplateTradeApi :public TradeApi{
	public:
		TemplateTradeApi() :_const(std::make_shared<CONST>()){}
		virtual ~TemplateTradeApi(){}
		virtual std::function<TradeQueryRef<pplx::task<BalanceSeqRef>>(const std::vector<Symbol>& symbols, const BalanceCfg&cfg)> balanceFactory() const{
			return  [](const std::vector<Symbol>& symbols, const BalanceCfg&cfg){
				return std::make_shared<BALANCEQ>(symbols, cfg);
			};
		}
		virtual std::function<TradeQueryRef<pplx::task<OrderStatusRef>>(Pair pair, const OrderStatusCfg&cfg)> orderStatusFactory() const{
			return  [](Pair pair, const OrderStatusCfg&cfg){
				return std::make_shared<ORDERQ>(pair, cfg);
			};
		}
		virtual std::function<TradeQueryRef<pplx::task<CancelOrderRef>>(const CancelOrderIn& in, const CancelOrderCfg&cfg)> cancelOrderFactory() const{
			return  [](const CancelOrderIn& in, const CancelOrderCfg&cfg){
				return std::make_shared<CANCELORDERQ>(in, cfg);
			};
		}
		virtual std::function<TradeQueryRef<pplx::task<TradeOrderRef>>(const TradeOrderIn& in, const TradeOrderCfg&cfg)> tradeOrderFactory() const{
			return  [](const TradeOrderIn& in, const TradeOrderCfg&cfg){
				return std::make_shared<TRADEORDERQ>(in, cfg);
			};
		}
		virtual exchangelib::ExchangeConstantsRef constants(){
			return _const;
		}
	private:
		std::shared_ptr<CONST> _const;
	};
}

#endif //__TEMPLTRADEAPI_H__