#ifndef __BALANCE_H__
#define __BALANCE_H__
#include <http_client.h>
#include <pplxtasks.h>
#include <algorithm>
#include "logger.h"
#include "commondefs.h"
#include "exchconst.h"
#include "tradeentity.h"

namespace tradelib
{
	using namespace exchangelib;
	class BalanceQueryBase :public TradeQuery<pplx::task<BalanceSeqRef>>, public std::enable_shared_from_this<BalanceQueryBase>{
	public:
		BalanceQueryBase(const exchangelib::ExchangeConstants* constants,
			const std::vector<Symbol>& symbols,
			const BalanceCfg& cfg
			) :_constants(constants), _symbols(symbols), _cfg(cfg), 
			_nonce(
			std::chrono::duration_cast<std::chrono::milliseconds>
			(std::chrono::system_clock::now().time_since_epoch()).count()){
			assert(constants);
			auto m = constants->symbols();
			std::for_each(_symbols.begin(), _symbols.end(), [&m, this](Symbol s){
				if (m.find(s) == m.end()) throw std::exception(common::ws2s(U("Invalid symbol: ") + s).c_str());
				_symbolsInv.push_back(m[s]);
			});
		}
		virtual ~BalanceQueryBase(){}
		virtual pplx::task<BalanceSeqRef> fetch();

	protected:
		virtual web::http::http_request request() = 0;
		virtual common::string_t url() = 0;
		virtual BalanceSeqRef parseJson(const web::json::value& json) = 0;
		const std::vector<Symbol> _symbols;
		std::vector<common::string_t> _symbolsInv;
		const BalanceCfg _cfg;
		long long _nonce;
		const exchangelib::ExchangeConstants* _constants;
	};
}

#endif // __BALANCE_H__