#ifndef __BITFLYERBAL_H__
#define __BITFLYERBAL_H__
#include <http_client.h>
#include <pplxtasks.h>
#include "bitflyerconst.h"
#include "logger.h"
#include "commondefs.h"
#include "balance.h"

namespace tradelib
{
	using namespace exchangelib;
	namespace bitflyer{
		class BalanceQuery :public BalanceQueryBase{
		public:
			BalanceQuery(const std::vector<Symbol>& symbols,
				const BalanceCfg& cfg
				) :BalanceQueryBase(&exchangelib::bitflyer::x_bitflyerConstants, symbols, cfg){}
			virtual common::string_t url() { return U("https://api.bitflyer.jp"); }
			virtual BalanceSeqRef parseJson(const web::json::value& json);
			virtual web::http::http_request request();
		private:
			common::string_t path() { return U("/v1/me/getbalance"); }
			common::string_t method() { return U("GET"); }
			string_t sign();
		};
	}
}

#endif // __BITFLYERBAL_H__