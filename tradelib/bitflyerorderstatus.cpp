#include <http_client.h>
#include <string>
#include "sha256.h"
#include "hmac.h"
#include "commonutil.h"
#include "exchutil.h"
#include "bitflyerorderstatus.h"

namespace tradelib
{
	namespace bitflyer{
		template<OrderStatusQueryBase::Kind kind>
		web::http::http_request OrderStatusQueryTemplate<kind>::request(){
			_nonce = std::chrono::duration_cast<std::chrono::milliseconds>
				(std::chrono::system_clock::now().time_since_epoch()).count();

			web::http::http_request r(method());
			r.set_request_uri(path());
			r.headers().add(U("ACCESS-SIGN"), sign());
			r.headers().add(U("ACCESS-KEY"), _cfg.apiKey->key);

			stringstream_t ss;
			ss << _nonce;
			r.headers().add(U("ACCESS-TIMESTAMP"), ss.str());
			return r;
		}
		template<OrderStatusQueryBase::Kind kind>
		string_t OrderStatusQueryTemplate<kind>::sign(){
			common::stringstream_t ss;
			ss << _nonce << method() << path();
			return s2ws(hmac<SHA256>(ws2s(ss.str()), ws2s(_cfg.apiKey->secret)));
		}
		template<OrderStatusQueryBase::Kind kind>
		OrderStatusRef OrderStatusQueryTemplate<kind>::parseJson(const web::json::value& json){
			auto ret = std::make_shared<OrderStatus>(_pair);
			auto o = GETASORELSE(json, array, throw std::exception("Invalid json: order response is not an array"));
			for (auto e : o){
				auto o1 = GETASORELSE(e, object, throw std::exception("Invalid json: balance element is not an object"));
				double price = GETFIELDASNUMBER(o1, price, throw std::exception("Invalid json: balance contains no price field"));
				double size = GETFIELDASNUMBER(o1, size, throw std::exception("Invalid json: balance contains no size field"));
				double commission = GETFIELDASNUMBER(o1, total_commission, throw std::exception("Invalid json: balance contains no total_commission field"));
				double tot = price*size - commission;
				ret->infos.push_back(OrderInfo(
					GETFIELDORELSE(o1, child_order_acceptance_id, string, throw std::exception("Invalid json: balance contains no child_order_acceptance_id field")),
					(GETFIELDORELSE(o1, side, string, throw std::exception("Invalid json: balance contains no side field"))) == U("BUY") ? OrderType::Buy : OrderType::Sell,
					price,
					size,
					tot
					));
			}
			return ret;
		}

		template OrderStatusQueryTemplate<OrderStatusQueryBase::Kind::Open>;
		template OrderStatusQueryTemplate<OrderStatusQueryBase::Kind::Closed>;

	}

}