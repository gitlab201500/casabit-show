#include <http_client.h>
#include <string>
#include <iomanip>
#include "sha256.h"
#include "hmac.h"
#include "commonutil.h"
#include "logger.h"
#include "exchutil.h"
#include "bitflyertradeorder.h"


namespace tradelib
{
	namespace bitflyer{
		DEFINE_LOGGER(TradeOrderQueryBitflyer, INFO);

		web::http::http_request TradeOrderQuery::request(){
			_nonce = std::chrono::duration_cast<std::chrono::milliseconds>
				(std::chrono::system_clock::now().time_since_epoch()).count();

			_nonce = std::chrono::duration_cast<std::chrono::milliseconds>
				(std::chrono::system_clock::now().time_since_epoch()).count();

			web::http::http_request r(method());
			r.set_request_uri(path());
			r.headers().add(U("ACCESS-SIGN"), sign());
			r.headers().add(U("ACCESS-KEY"), _cfg.apiKey->key);

			auto rq = req();
			stringstream_t ss;
			ss << _nonce;
			r.headers().add(U("ACCESS-TIMESTAMP"), ss.str());
			r.set_body(rq, U("application/json"));
			logDebug(logTradeOrderQueryBitflyer, U("uri: ")<<url()<<path()<<U(" req: ") << rq);
			return r;
		}

		string_t TradeOrderQuery::sign(){
			common::stringstream_t ss;
			ss << _nonce << method() << path() << req();
			return s2ws(hmac<SHA256>(ws2s(ss.str()), ws2s(_cfg.apiKey->secret)));
		}

		string_t TradeOrderQuery::req(){
			auto js = web::json::value::object();
			js[U("product_code")] = web::json::value::string(_pairInv);
			js[U("child_order_type")] = web::json::value::string(U("LIMIT"));
			js[U("side")] = web::json::value::string(_in.type==Buy?U("BUY"):U("SELL"));
			js[U("minute_to_expire")] = web::json::value::number(1);
			js[U("time_in_force")] = web::json::value::string(U("GTC"));
			// fix long default formatting of doubles
			auto ser = js.serialize();
			stringstream_t ss;
			ss << ser.substr(0, ser.size() - 1) <<
				U(",\"price\":") << std::setprecision(x_presizion) << _in.rate <<
				U(",\"size\":") << std::setprecision(x_presizion) << _in.amount <<
				U("}");
			return ss.str();
		}

		TradeOrderRef TradeOrderQuery::parseJson(const web::json::value& json){
			logDebug(logTradeOrderQueryBitflyer, U("Reply: ") << json);
			auto o = GETASORELSE(json, object, throw std::exception("Invalid json: order element is not an object"));
			auto on = GETFIELDORELSE(o, child_order_acceptance_id, string, throw std::exception("Invalid json: balance contains no child_order_acceptance_id field"));
			return std::make_shared<TradeOrder>(_in.pair, 
				TradeOrderInfo(on, {}));
		}
	}


}