#ifndef __TradeApi_H__
#define __TradeApi_H__
#include "unistr.h"
#include <map>
#include <pplxtasks.h>
#include "tradeentity.h"
#include "exchconst.h"
namespace tradelib{
	class TradeApi{
	public:
		virtual ~TradeApi(){}
		virtual std::function<TradeQueryRef<pplx::task<BalanceSeqRef>>(const std::vector<Symbol>& symbols, const BalanceCfg&cfg)> balanceFactory() const = 0;
		virtual std::function<TradeQueryRef<pplx::task<OrderStatusRef>>(Pair pair, const OrderStatusCfg&cfg)> orderStatusFactory() const = 0;
		virtual std::function<TradeQueryRef<pplx::task<CancelOrderRef>>(const CancelOrderIn& in, const CancelOrderCfg&cfg)> cancelOrderFactory() const = 0;
		virtual std::function<TradeQueryRef<pplx::task<TradeOrderRef>>(const TradeOrderIn& in, const TradeOrderCfg&cfg)> tradeOrderFactory() const = 0;
		virtual exchangelib::ExchangeConstantsRef constants() = 0;
	};
	typedef std::shared_ptr<const TradeApi> TradeApiRef;
}

#endif // __TradeApi_H__