#include <http_client.h>
#include <string>
#include "sha512.h"
#include "commonutil.h"
#include "exchutil.h"
#include "poloniexcancelorder.h"


namespace tradelib
{
	namespace poloniex{
		DEFINE_LOGGER(CancelOrderQuery,INFO);
		web::http::http_request CancelOrderQuery::request(){
			_nonce = std::chrono::duration_cast<std::chrono::milliseconds>
				(std::chrono::system_clock::now().time_since_epoch()).count();
			web::http::http_request r(method());
			r.set_request_uri(path());
			r.headers().add(U("Sign"), sign());
			r.headers().add(U("Key"), _cfg.apiKey->key);
			auto rq = req();
			r.set_body(rq, U("application/x-www-form-urlencoded"));
			logDebug(logCancelOrderQuery, U("uri: ") << url() << path() << U(" req: ") << rq);
			return r;
		}

		string_t CancelOrderQuery::sign(){
			common::stringstream_t ss;
			ss << req();
			return s2ws(hashlib::hmac512(ws2s(ss.str()), ws2s(_cfg.apiKey->secret)));
		}

		CancelOrderRef CancelOrderQuery::parseJson(const web::json::value& json){
			logDebug(logCancelOrderQuery, U("Reply: ") << json);
			auto o = GETASORELSE(json, object, throw std::exception("Invalid json: cancelOrder response is not an object"));
			auto err = o.find(U("error"));
			if ( err != o.end()) throw std::exception(ws2s(err->second.as_string()).c_str());
			auto success = GETFIELDORELSE(o, success, integer, throw std::exception("Invalid json: CancelOrder has no success field"));
			if (!success) {
				stringstream_t ss1;
				ss1 << U("Failed to cancel order, error: ") <<
					GETFIELDORELSE(o, message, string, throw std::exception("Invalid json: balance contains no rate field"));
				throw std::exception(ws2s(ss1.str()).c_str());
			}
			return std::make_shared<CancelOrder>(_orderNumber, true);
		}

	}


}