#include "exchanges.h"
#include "tradeapicoll.h"
#include "bitflyerconst.h"
#include "poloniexconst.h"
#include "bitflyerbal.h"
#include "poloniexbal.h"

#include "bitflyerorderstatus.h"
#include "poloniexorderstatus.h"

#include "bitflyercancelorder.h"
#include "poloniexcancelorder.h"

#include "bitflyertradeorder.h"
#include "poloniextradeorder.h"

#include "templtradeapi.h"

namespace tradelib
{
	using TemplateTradeApiBitflyer = TemplateTradeApi<
		bitflyer::BalanceQuery, 
		bitflyer::OrderStatusQuery,
		bitflyer::CancelOrderQuery,
		bitflyer::TradeOrderQuery,
		exchangelib::bitflyer::BitflyerConstants>;
	using TemplateTradeApiPoloniex = TemplateTradeApi<
		poloniex::BalanceQuery,
		poloniex::OrderStatusQuery,
		poloniex::CancelOrderQuery,
		poloniex::TradeOrderQuery,
		exchangelib::poloniex::PoloniexConstants>;
	static const std::map<common::string_t, TradeApiRef> initMap(){
		auto m = std::map<common::string_t, TradeApiRef>();
		m[std::make_shared<exchangelib::bitflyer::BitflyerConstants>()->name()] = std::make_shared<TemplateTradeApiBitflyer>();
		m[std::make_shared<exchangelib::poloniex::PoloniexConstants>()->name()] = std::make_shared<TemplateTradeApiPoloniex>();
		return m;
	}
	const std::map<common::string_t, TradeApiRef> x_tradeApiCollection = initMap();
}