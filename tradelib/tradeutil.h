#ifndef __TRADEUTIL_H__
#define __TRADEUTIL_H__
#include <map>
#include <chrono>
#include <memory>
#include "logger.h"
#include "commonutil.h"

namespace tradelib
{
	using namespace common;
	struct ApiKey{
		ApiKey(const string_t& e, const string_t& k, const string_t& s) :exchange(e), key(k), secret(s){}
		const string_t exchange;
		const string_t key;
		const string_t secret;
	};
	typedef std::shared_ptr<const ApiKey> ApiKeyRef;

	std::map<string_t, ApiKeyRef> readApiKeys(const string_t& file);
	ApiKeyRef readApiKey(const string_t& file, const string_t& exchange);
}
#endif //__TRADEUTIL_H__