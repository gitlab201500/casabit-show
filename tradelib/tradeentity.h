#ifndef __TRADEENTITY_H__
#define __TRADEENTITY_H__
#include <vector>
#include <chrono>
#include "logger.h"
#include "commonutil.h"
#include "tradeutil.h"
#include "exchdef.h"

namespace tradelib
{
	using namespace exchangelib;
	// Balance
	struct Balance{
		Balance(Symbol s, double b) :symbol(s), balance(b){}
		const Symbol symbol;
		const double balance;
	};
	
	typedef std::shared_ptr<Balance> BalanceRef;
	typedef std::shared_ptr<std::vector<Balance>> BalanceSeqRef;

	inline string_t balSeqToStr(const BalanceSeqRef& b){
		stringstream_t ss;
		for (auto bb : *b) ss << U(" sym: ") << bb.symbol << U(" bal: ") << bb.balance;
		return ss.str();
	}

	struct BalanceCfg{
		BalanceCfg(ApiKeyRef& k,int szi = 20) :apiKey(k), sz(szi){}
		const int sz;
		ApiKeyRef apiKey;
	};

	enum OrderType { Buy, Sell };

	// OrderStatus
	struct OrderInfo{
		OrderInfo(const string_t& on, OrderType t, double r, double a, double tot) :
			orderNumber(on), type(t), rate(r), amount(a), total(tot){}
		const string_t orderNumber;
		const OrderType type;
		const double rate;
		const double amount;
		const double total;
	};

	typedef std::vector<OrderInfo> OrderInfoSeq;

	struct OrderStatus{
		OrderStatus(Pair p) :pair(p){}
		const Pair pair;
		OrderInfoSeq infos;
	};

	typedef std::shared_ptr<const OrderStatus> OrderStatusRef;

	struct OrderStatusCfg{
		OrderStatusCfg(ApiKeyRef& k) :apiKey(k){}
		ApiKeyRef apiKey;
	};

	// CancelOrder
	struct CancelOrderIn{
		CancelOrderIn(Pair p, const common::string_t& on) :pair(p), orderNumber(on){}
		Pair pair;
		const common::string_t orderNumber;
	};

	struct CancelOrder{
		CancelOrder(const string_t& on, bool s) :orderNumber(on), success(s){}
		const string_t orderNumber;
		const bool success;
	};

	typedef std::shared_ptr<const CancelOrder> CancelOrderRef;

	struct CancelOrderCfg{
		CancelOrderCfg(ApiKeyRef& k) :apiKey(k){}
		ApiKeyRef apiKey;
	};

	// TradeOrder
	struct TradeOrderIn{
		TradeOrderIn(OrderType ty, Pair p, double r,double a,bool fk=false, bool ic=false) :
		type(ty),pair(p), rate(r),amount(a),fillOrKill(fk),immediateOrCancel(ic){}
		OrderType type;
		Pair pair;
		const double rate;
		const double amount;
		const bool fillOrKill;
		const bool immediateOrCancel;
	};

	struct TradeOrderData{
		TradeOrderData(const string_t& dt, double r, double a, double tot, const common::string_t& idi, OrderType ty) :
		date(dt), rate(r), amount(a), total(tot), id(idi), type(ty){}
		const string_t date;
		const double rate;
		const double amount;
		const double total;
		const common::string_t id;
		OrderType type;
	};

	struct TradeOrderInfo{
		TradeOrderInfo(const common::string_t& on, std::vector<TradeOrderData>&& t) :orderNumber(on),trades(t){}
		const common::string_t orderNumber;
		std::vector<TradeOrderData> trades;
	};

	struct TradeOrder{
		TradeOrder(Pair p, TradeOrderInfo&& i) :pair(p),info(i){}
		Pair pair;
		TradeOrderInfo info;
	};

	typedef std::shared_ptr<const TradeOrder> TradeOrderRef;

	struct TradeOrderCfg{
		TradeOrderCfg(ApiKeyRef& k) :apiKey(k){}
		ApiKeyRef apiKey;
	};

	// TradeQuery
	template <class T>
	class TradeQuery{
	public:
		TradeQuery() :_start(std::chrono::system_clock::now()), _end(_start){}
		virtual ~TradeQuery(){}
		virtual T fetch() = 0;
		virtual std::chrono::system_clock::time_point start()const { return  _start; }
		virtual std::chrono::system_clock::time_point end()const { return  _end; }
		virtual long long rtMs()const { return  std::chrono::duration_cast<std::chrono::milliseconds>(_end - _start).count(); }
	protected:
		std::chrono::system_clock::time_point _start;
		std::chrono::system_clock::time_point _end;
	};

	template <class T>
	using TradeQueryRef = std::shared_ptr<TradeQuery<T>>;
}

#endif //__TRADEENTITY_H__

