#ifndef __DEPTHACTOR__
#define __DEPTHACTOR__

#include "actor.h"
#include "arbdef.h"
#include "exchutil.h"
#include "exchdef.h"
#include "tradeutil.h"
#include "commonutil.h"
#include "exchentity.h"


using namespace common;
using namespace exchangelib;
using namespace tradelib;
using namespace actor;
using namespace arblib;
namespace bitdepth{
	DECLARE_LOGGER(DepthActor);
	struct DepthActorConfig{
		DepthActorConfig(std::chrono::milliseconds d = x_delayDepthDefault,
			int pf = x_printFreqDefault) :delay(d),  printFreq(pf){}
		std::chrono::milliseconds delay;
		int printFreq;
	};
	class DepthActor :public ActorBase{
		DECLARE_SHARED_REF_OBJ(DepthActor, const string_t& n, const string_t& exch, std::vector<Pair> pairs, ActorRefWeak exchActor, ActorSystemRefWeak as,
		const DepthActorConfig& aconf = DepthActorConfig(),
		const ActorBaseConfig& conf = ActorBaseConfig());
	public:
		virtual ~DepthActor(){}
	private:
		DepthActorConfig _aconf;
		pplx::task<void> _task;
		ActorRefWeak _exchActor;
		string_t _exch;
		std::vector<Pair> _pairs;
		MessageRef _depthReq;
	};
}
#endif //__DEPTHACTOR__