#include <thread>
#include <cstdlib>
#include "commonpplx.h"
#include "arbactor.h"
#include "exchanges.h"
#include "tradeapicoll.h"
#include "arbmsg.h"
#include "depthmatcher.h"

namespace bitdepth{
	DEFINE_LOGGER(ArbActor, INFO);

	const EpochDeltaMs x_maxRtTime = 1000;
	const EpochDeltaMs x_maxQuoteAge = 1000;
	const EpochDeltaMs x_maxEpochDelta = 500;
	const EpochDeltaMs x_maxBalanceAge = 5000;

	DEFINE_SHARED_REF_OBJ(ArbActor, const common::string_t& n, Pair pair,
		actor::ActorSystemRefWeak as,
		const ArbActorConfig& aconf,
		const actor::ActorBaseConfig& conf){
		auto p = MAKE_SHARED(ArbActor);
		p->initActorBase(n, as, conf);
		p->_aconf = aconf;
		p->_pair = pair;
		p->_syms = pairToSymbols(p->_pair);
		if (p->_syms.empty()) throw std::exception("Invalid pair: cannot find symobls");

		auto& bm = p->_balances;
		for (auto s : p->_syms){
			bm[s] = std::make_shared<Balance>(s,100.0);
		}
		
		p->_mainLoop = std::function<void(const actor::MessageRef&)>([p](const actor::MessageRef m){
			common::stringstream_t ss;
			assert(m);
			actor::MessageType type = m->type();
			switch (type){
			case mtDepthResp:
			{
				auto mm = CAST_MESSAGE(DepthResp, m);
				if (mm->depth->pair != p->_pair){
					logWarn(logArbActor, U("Received depth for invalid pair: ") << U(" pair:") << mm->depth->pair << U(" expected: ") << p->_pair);
				}
				else {
					logDebug(logArbActor, U("Received depth message: ") << U(" exch:") << mm->exch);
					if (p->_depthData.find(mm->exch) == p->_depthData.end() || p->_depthData[mm->exch].epoch < mm->epoch){
						p->_depthData[mm->exch]=DepthData(mm->epoch,mm->rTime,mm->depth);
						auto orderColl = p->computeTrade(mm->exch);
						if (orderColl){
							//logInfo(logArbActor, U("FoundTrade: ") << orderColl->toString());
							// make a call to trade here
						}
					}
				}
				
				break;
			}

			default:
				ss << U("Unexpected message type: ") << type;
				logWarn(logArbActor, ss.str());
				break;
			}
		});

		p->becomes(p->_mainLoop);

		return p;
	}


	bool ArbActor::checkDepthData(Exchange e, common::EpochMs epoch, const DepthData& dd){
		if (dd.rt > x_maxRtTime){
			logDebug(logArbActor, U("RTime is too long, ignore for trade: ") << dd.rt << U(" exchange: ") << exchtos(e));
			return false;
		}

		if (epoch - dd.epoch > x_maxQuoteAge){
			logWarn(logArbActor, U("Quote age is too long, ignore for trade: ") << epoch - dd.epoch << U(" exchange: ") << exchtos(e));
			return false;
		}
		return true;
	}

	OrderCollectionRef ArbActor::computeTrade(Exchange srcExch)
	{
		auto now = epochTimeMs();
		auto srcBal = _balances;
		if (srcBal.empty()) return nullptr;

		auto srcDepth = _depthData.find(srcExch);
		if (srcDepth == _depthData.end()) return nullptr;

		assert(srcDepth->second.depth);
		for (auto e : _depthData){
			auto tgtExch = e.first;
			// depth data is not old
			if (!checkDepthData(tgtExch, now, e.second)) continue;

			// skip same exchange
			if (tgtExch == srcExch) continue;

			// balance is not old
			auto tgtBal = _balances;
			if (tgtBal.empty()) continue;

			// 2 quoates are not too far apart
			auto tgtDepth = e;
			if (std::abs(srcDepth->second.epoch - tgtDepth.second.epoch) > x_maxEpochDelta) continue;
			assert(srcDepth->second.depth);
			assert(tgtDepth.second.depth);
			DepthMatcher dm(_pair, 
				srcExch, srcDepth->second.depth, srcDepth->second.epoch,srcBal,
				tgtExch, tgtDepth.second.depth, tgtDepth.second.epoch, tgtBal, _aconf.toDepthMatcherCfg());
			return dm.orders();
		}
		return nullptr;

	}

}