#ifndef __ARBACTOR__
#define __ARBACTOR__

#include "actor.h"
#include "arbdef.h"
#include "exchutil.h"
#include "tradeutil.h"
#include "arbtrade.h"
#include "commonutil.h"
#include "exchentity.h"
#include "tradeentity.h"
#include "depthmatcher.h"


using namespace common;
using namespace exchangelib;
using namespace tradelib;
using namespace actor;
using namespace arblib;
namespace bitdepth{
	DECLARE_LOGGER(ArbActor);
	struct ArbActorConfig{
		ArbActorConfig(
			int pf = x_printFreqDefault, bool test = false, double qs = 0.) : 
			printFreq(pf), isTest(test), quoteShift(qs){}
		DepthMatcherCfg toDepthMatcherCfg() const{
			DepthMatcherCfg dmcfg;
			dmcfg.isTest = isTest;
			dmcfg.quoteShift = quoteShift;
			return dmcfg;
		}

		int printFreq;
		bool isTest;
		double quoteShift;
	};

	struct DepthData{
		DepthData() :epoch(0), rt(0), depth(nullptr){}
		DepthData(EpochMs e, EpochDeltaMs r, const DepthRef&d) :epoch(e), rt(r), depth(d){}
		EpochMs epoch;
		EpochDeltaMs rt;
		DepthRef depth;
	};

	class ArbActor :public actor::ActorBase{
		DECLARE_SHARED_REF_OBJ(ArbActor, const common::string_t& n, Pair pair,actor::ActorSystemRefWeak as,
		const ArbActorConfig& aconf = ArbActorConfig(),
			const actor::ActorBaseConfig& conf = actor::ActorBaseConfig());
	public:
		virtual ~ArbActor(){}
	private:

		OrderCollectionRef computeTrade(Exchange srcExch);
		bool checkDepthData(Exchange e, common::EpochMs epoch, const DepthData& dd);

		ArbActorConfig _aconf;
		pplx::task<void> _task;
		std::function<void(const actor::MessageRef& m)> _mainLoop;
		Pair _pair;
		std::vector<Symbol> _syms;
		std::map<Symbol, BalanceRef> _balances;
		std::map<Exchange,DepthData >_depthData;
	};
}
#endif //__ARBACTOR__