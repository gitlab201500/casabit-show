#include <thread>
#include "commonpplx.h"
#include "arbmsg.h"
#include "depthactor.h"

namespace bitdepth{
	DEFINE_LOGGER(DepthActor, INFO);

	DEFINE_SHARED_REF_OBJ(DepthActor, const common::string_t& n, const common::string_t& exch, 
		std::vector<Pair> pairs, ActorRefWeak exchActor, actor::ActorSystemRefWeak as,
		const DepthActorConfig& aconf,
		const actor::ActorBaseConfig& conf){
		auto p = MAKE_SHARED(DepthActor);
		p->initActorBase(n, as, conf);
		p->_exch = exch;
		p->_pairs = pairs;
		assert(p->_pairs.size() == 1);
		p->_depthReq = std::make_shared<DepthReq>(p->_pairs[0]);

		p->_exchActor = exchActor;
		p->_aconf = aconf;

		auto ff = std::function<bool()>([p](){
			logDebug(logDepthActor, U("Depth request "));	
			auto a = p->_exchActor.lock();
			if (a){
				a->tell(p->_depthReq);
			}
			return true;
		});
		common::taskLoop(p->_aconf.delay, ff,p->cancellationToken());

		return p;
	}
}