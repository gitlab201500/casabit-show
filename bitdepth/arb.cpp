#include "arb.h"
#include "depthactor.h"
#include "arbactor.h"
#include "exchactor.h"

using namespace exchangelib;
namespace bitdepth{
	DEFINE_LOGGER(Arb, INFO);

	Arb::Arb(const std::vector<string_t>& e,
		const std::vector<Pair> pairs,
		const ArbCfg& cfg) : _exchanges(e), _pairs(pairs), _cfg(cfg){
		if (pairs.size() != 1) throw std::exception("Invalid pirs parameters. There must be only one pair.");
		_as = actor::ActorSystem::createActorSystem(U("ActorSystembitdepth"));
		std::for_each(_pairs.cbegin(), _pairs.cend(), [this](Pair p){auto ss = pairToSymbols(p); 
			std::for_each(ss.cbegin(), ss.cend(), [this](Symbol s){_symbols.push_back(s); }); });
		auto ac = ActorBaseConfig(); 

		std::map<string_t, ActorRef> depthActors;
		std::map<string_t, ActorRef> exchActors;
		std::map<Pair, ActorRefWeak> arbActors;
		for (Pair p : pairs){
			arbActors[p] = ArbActor::createArbActor(string_t(U("ArbActor_") + pairtos(p)), p, _as,
					ArbActorConfig(_cfg.printFreq,_cfg.isTest,_cfg.quoteShift), ac);
		}
		for (auto ee : _exchanges){
			exchActors[ee] = ExchActor::createExchActor(string_t(U("ExchActor_")) + ee, exchangelib::stoexch(ee), arbActors, _as,
				_cfg.toExchActorConfig(), ac);
			depthActors[ee] = DepthActor::createDepthActor(string_t(U("DepthActor_")) + ee, ee, _pairs, exchActors[ee], _as, 
				_cfg.toDepthActorConfig(), ac);
		}

	}


	Arb::~Arb(){
		_as->terminate();

		auto r = exchangelib::Try(std::function<bool()>([this](){
			return _as->onTerminated().get();
		}));
		if (r.isSuccess()){
			logInfo(logArb, U("Succesfully terminated actor system"));
		}
		else {
			logError(logArb, U("Failed to terminate actor system") << common::s2ws(r.failure().what()));
		}
	}

}