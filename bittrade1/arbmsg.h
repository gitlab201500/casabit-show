#ifndef __ARBMSG__
#define __ARBMSG__

#include "actor.h"
#include "exchdef.h"
#include "exchentity.h"
#include "tradeentity.h"
enum MessageTypes{
	mtDepthReq = 0,
	mtBalanceReq,
	mtDepthResp,
	mtBalanceResp,
};

class DepthReq : public actor::Message{
public:
	DepthReq(exchangelib::Pair p) :Message(mtDepthReq), pair(p){}
	const exchangelib::Pair pair;
};

class BalanceReq : public actor::Message{
public:
	BalanceReq(const std::vector<exchangelib::Symbol>& syms) :Message(mtBalanceReq), symbols(syms){}
	const std::vector<exchangelib::Symbol> symbols;
};

class DepthResp : public actor::Message{
public:
	DepthResp(common::EpochMs ep, common::EpochDeltaMs rt, exchangelib::Exchange e, const exchangelib::DepthRef& d) :Message(mtDepthResp), epoch(ep), rTime(rt), exch(e), depth(d){}
	const common::EpochMs epoch;
	const common::EpochDeltaMs rTime;
	const exchangelib::Exchange exch;
	const exchangelib::DepthRef depth;
};

class BalanceResp : public actor::Message{
public:
	BalanceResp(common::EpochMs ep, exchangelib::Exchange e, const tradelib::BalanceSeqRef& b) :Message(mtBalanceResp), epoch(ep), exch(e), balance(b){}
	const common::EpochMs epoch;
	const exchangelib::Exchange exch;
	const tradelib::BalanceSeqRef balance;
};
#endif //__ARBMSG__