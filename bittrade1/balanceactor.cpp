#include <thread>
#include "commonpplx.h"
#include "arbmsg.h"
#include "balanceactor.h"

namespace bittrade1{
	DEFINE_LOGGER(BalanceActor, INFO);

	DEFINE_SHARED_REF_OBJ(BalanceActor, const common::string_t& n, const common::string_t& exch,
		std::vector<Symbol> symbols, ActorRefWeak exchActor, actor::ActorSystemRefWeak as,
		const BalanceActorConfig& aconf,
		const actor::ActorBaseConfig& conf){
		auto p = MAKE_SHARED(BalanceActor);
		p->initActorBase(n, as, conf);
		p->_exch = exch;
		p->_symbols = symbols;
		p->_reqMsg = std::make_shared<BalanceReq>(p->_symbols);
		if (!p->_symbols.size()) throw std::exception("Symbols list is empty.");

		p->_exchActor = exchActor;
		p->_aconf = aconf;

		auto ff = std::function<bool()>([p](){
			logDebug(logBalanceActor, U("Balance request "));
			auto a = p->_exchActor.lock();
			if (a){
				a->tell(p->_reqMsg);
			}
			return true;
		});
		common::taskLoop(p->_aconf.delay, ff, p->cancellationToken());

		return p;
	}
}