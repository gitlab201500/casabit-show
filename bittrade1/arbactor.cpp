#include <thread>
#include <cstdlib>
#include "commonpplx.h"
#include "arbactor.h"
#include "exchanges.h"
#include "tradeapicoll.h"
#include "arbmsg.h"
#include "depthmatcher.h"

namespace bittrade1{
	DEFINE_LOGGER(ArbActor, INFO);

	const EpochDeltaMs x_maxRtTime = 1000;
	const EpochDeltaMs x_maxQuoteAge = 1000;
	const EpochDeltaMs x_maxEpochDelta = 500;
	const EpochDeltaMs x_maxBalanceAge = 5000;

	DEFINE_SHARED_REF_OBJ(ArbActor, const common::string_t& n, Pair pair,
		actor::ActorSystemRefWeak as,
		const ArbActorConfig& aconf,
		const actor::ActorBaseConfig& conf){
		auto p = MAKE_SHARED(ArbActor);
		p->initActorBase(n, as, conf);
		p->_aconf = aconf;
		p->_pair = pair;
		p->_syms = pairToSymbols(p->_pair);
		if (p->_syms.empty()) throw std::exception("Invalid pair: cannot find symobls");

		p->_mainLoop = std::function<void(const actor::MessageRef&)>([p](const actor::MessageRef m){
			common::stringstream_t ss;
			assert(m);
			actor::MessageType type = m->type();
			switch (type){
			case mtDepthResp:
			{
				auto mm = CAST_MESSAGE(DepthResp, m);
				if (mm->depth->pair != p->_pair){
					logWarn(logArbActor, U("Received depth for invalid pair: ") << U(" pair:") << mm->depth->pair << U(" expected: ") << p->_pair);
				}
				else {
					logDebug(logArbActor, U("Received depth message: ") << U(" exch:") << mm->exch);
					if (p->_depthData.find(mm->exch) == p->_depthData.end() || p->_depthData[mm->exch].epoch < mm->epoch){
						p->_depthData[mm->exch]=DepthData(mm->epoch,mm->rTime,mm->depth);
						auto orderColl = p->computeTrade(mm->exch);
						if (orderColl){
							//logInfo(logArbActor, U("FoundTrade: ") << orderColl->toString());
							// make a call to trade here
							// reset balances
							p->resetBalance(orderColl->exch1);
							p->resetBalance(orderColl->exch2);
						}
					}
				}
				
				break;
			}
			case mtBalanceResp:
			{
				auto mm = CAST_MESSAGE(BalanceResp, m);
				logDebug(logArbActor, U("Received balance message: ") << U(" exch:") << mm->exch);
				auto& bm = p->_balances[mm->exch];
				for (auto b : *mm->balance){
					if (std::find(p->_syms.cbegin(), p->_syms.cend(), b.symbol) == p->_syms.cend()){
						logWarn(logArbActor, U("Received balance for invalid symbol: ") << U(" symbol:") << symtos(b.symbol));
					}
					else{
						if (bm.find(b.symbol) == bm.end() || bm[b.symbol].first < mm->epoch){
							bm[b.symbol] = std::pair<EpochMs, BalanceRef>(mm->epoch, std::make_shared<Balance>(b));
						}
					}
				}

				break;
			}

			default:
				ss << U("Unexpected message type: ") << type;
				logWarn(logArbActor, ss.str());
				break;
			}
		});

		p->becomes(p->_mainLoop);

		return p;
	}


	void ArbActor::resetBalance(Exchange exch){
		auto bal = _balances.find(exch);
		if (bal == _balances.end()) return ;
		for (auto s : _syms){
			auto bb = bal->second.find(s);
			if (bb != bal->second.end() )
			{
				bal->second.erase(bb);
			}
		}
	}
	std::map<Symbol,BalanceRef> ArbActor::checkBalance(common::EpochMs epoch, Exchange exch){
		auto v = std::map<Symbol, BalanceRef>();
		auto bal = _balances.find(exch);
		if (bal == _balances.end()) return std::map<Symbol, BalanceRef>();

		for (auto s : _syms){
			auto bb = bal->second.find(s);
			if (bb == bal->second.end() || epoch - bb->second.first > x_maxBalanceAge)
			{
				return std::map<Symbol, BalanceRef>();
			}
			v[s]=bb->second.second;
		}
		return v;
	}

	bool ArbActor::checkDepthData(Exchange e, common::EpochMs epoch, const DepthData& dd){
		if (dd.rt > x_maxRtTime){
			logDebug(logArbActor, U("RTime is too long, ignore for trade: ") << dd.rt << U(" exchange: ") << exchtos(e));
			return false;
		}

		if (epoch - dd.epoch > x_maxQuoteAge){
			logWarn(logArbActor, U("Quote age is too long, ignore for trade: ") << epoch - dd.epoch << U(" exchange: ") << exchtos(e));
			return false;
		}
		return true;
	}

	OrderCollectionRef ArbActor::computeTrade(Exchange srcExch)
	{
		auto now = epochTimeMs();
		auto srcBal = checkBalance(now, srcExch);
		if (srcBal.empty()) return nullptr;

		auto srcDepth = _depthData.find(srcExch);
		if (srcDepth == _depthData.end()) return nullptr;

		assert(srcDepth->second.depth);
		for (auto e : _depthData){
			auto tgtExch = e.first;
			// depth data is not old
			if (!checkDepthData(tgtExch, now, e.second)) continue;

			// skip same exchange
			if (tgtExch == srcExch) continue;

			// balance is not old
			auto tgtBal = checkBalance(now, tgtExch);
			if (tgtBal.empty()) continue;

			// 2 quoates are not too far apart
			auto tgtDepth = e;
			if (std::abs(srcDepth->second.epoch - tgtDepth.second.epoch) > x_maxEpochDelta) continue;
			assert(srcDepth->second.depth);
			assert(tgtDepth.second.depth);
			DepthMatcher dm(_pair, 
				srcExch, srcDepth->second.depth, srcDepth->second.epoch,srcBal,
				tgtExch, tgtDepth.second.depth, tgtDepth.second.epoch, tgtBal, _aconf.toDepthMatcherCfg());
			return dm.orders();
		}
		return nullptr;

	}

}