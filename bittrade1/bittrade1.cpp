#ifdef WIN32
#include "stdafx.h"
#endif //WIN32
#include "commondefs.h"
#include <vector>
#include <chrono>
#include <typeinfo>
#include <thread>
#include <algorithm>
#include <map>
#include <chrono>
#include <iostream>
#include <functional>
#include "exchanges.h"
#include "tradeutil.h"
#include "arb.h"

#ifdef WIN32
#include "windows.h"
#endif //WIN32

DEFINE_LOGGER(Bittrade1, INFO);

void usage()
{
	std::cout << "Usage bittrade1 arb --exchange <name> --pf <print_freq> --d <delay>"<< std::endl;
}

using namespace exchangelib;
using namespace common;
using namespace bittrade1;

string_t apiKeyFile = U("apikeys.json");

Arb* g_arb=nullptr;

#ifdef WIN32
BOOL WINAPI HandlerRoutine(
	_In_  DWORD dwCtrlType
	)
{
	switch (dwCtrlType)
	{
	case CTRL_C_EVENT:
	case CTRL_BREAK_EVENT:
		logDebug(logBittrade1,U("HandlerRoutine: shutdown ..."));
		if (g_arb) g_arb->terminate();
		return true;
	default:
		return false;
	}
}
#endif //WIN32

struct CmdLineCfg{
	CmdLineCfg() :pf(x_printFreqDefault), delay(x_delayDepthDefault), isTest(false), quoteShift(0.)
	{ exchanges = { U("poloniex"), U("bitflyer") }; }
	ArbCfg toArbCfg(const std::map<string_t, ApiKeyRef>& keys )const{
		auto acfg = ArbCfg(keys, pf, delay);
		acfg.isTest = isTest;
		acfg.quoteShift = quoteShift;
		return acfg;
	}
	string_t cmd;
	std::vector<string_t> exchanges;
	int pf;
	std::chrono::milliseconds delay;
	bool isTest;
	double quoteShift; // only in test mode
};

#ifdef WIN32
int wmain(int argc, common::char_t* argv[])
#else
int main(int argc, common::char_t* argv[])
#endif //WIN32
{
#ifdef WIN32
	if (!SetConsoleCtrlHandler(HandlerRoutine, true))
	{
		return EXIT_FAILURE;
	}
#endif //WIN32

	if (argc < 2)
	{
		usage();
		return EXIT_FAILURE;
	}
	try{
		CmdLineCfg cfg;
		cfg.cmd = argv[1];

		for (int i = 2; i < argc;)
		{
			if (string_t(argv[i]) == string_t(U("--exchanges")) && i + 1 < argc)
			{
				cfg.exchanges = common::split(argv[++i], U(','));
			}
			else if (string_t(argv[i]) == string_t(U("--pf")) && i + 1 < argc)
			{
				cfg.pf = std::stoi(argv[++i]);
			}
			else if (string_t(argv[i]) == string_t(U("--d")) && i + 1 < argc)
			{
				cfg.delay = std::chrono::milliseconds(std::stoi(argv[++i]));
			}
			else if (string_t(argv[i]) == string_t(U("--test")) && i + 1 < argc)
			{
				cfg.quoteShift = std::stod(argv[++i]);
				cfg.isTest = true;
			}
			else
			{
				std::cerr << "invalid option" << std::endl;
				usage();
				return EXIT_FAILURE;
			}
			i++;
		}

		if (cfg.cmd == U("arb")){
			auto keys = tradelib::readApiKeys(apiKeyFile);
			if (keys.empty()) throw std::exception("No Api keys found");
			std::vector<exchangelib::Pair> pairs = { ETH_BTC };
			Arb arb(cfg.exchanges, pairs, cfg.toArbCfg(keys));
			g_arb = &arb;
			logInfo(logBittrade1, U("Launched actor system. Waiting onTerminate ..."));
			auto r = exchangelib::Try(std::function<bool()>([&arb](){
				return arb.onTerminate().get();
			}));
			logInfo(logBittrade1, U("onTerminate signalled. Shut down... Wait 2 seconds for things to settle."));
			arb.terminate();
			logBittrade1.flush();
			std::this_thread::sleep_for(std::chrono::milliseconds(2000));
		}
		else {
			std::cerr << "invalid command" << std::endl;
			usage();
			return EXIT_FAILURE;
		}

		return EXIT_SUCCESS;
	}
	catch (const std::exception&e){
		std::cerr << "Exception" << e.what()<<std::endl;
	}
}

