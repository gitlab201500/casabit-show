#ifndef __BALANCEACTOR__
#define __BALANCEACTOR__

#include "actor.h"
#include "arbdef.h"
#include "exchutil.h"
#include "exchdef.h"
#include "tradeutil.h"
#include "commonutil.h"
#include "exchentity.h"


using namespace common;
using namespace exchangelib;
using namespace tradelib;
using namespace actor;
using namespace arblib;
namespace bittrade1{
	DECLARE_LOGGER(BalanceActor);
	struct BalanceActorConfig{
		BalanceActorConfig(std::chrono::milliseconds d = x_delayBalanceDefault,
			int pf = x_printFreqDefault) :delay(d),  printFreq(pf){}
		std::chrono::milliseconds delay;
		int printFreq;
	};
	class BalanceActor :public ActorBase{
		DECLARE_SHARED_REF_OBJ(BalanceActor, const string_t& n, const string_t& exch, std::vector<Symbol> symbols, ActorRefWeak exchActor, ActorSystemRefWeak as,
		const BalanceActorConfig& aconf = BalanceActorConfig(),
			const ActorBaseConfig& conf = ActorBaseConfig());
	public:
		virtual ~BalanceActor(){}
	private:
		BalanceActorConfig _aconf;
		pplx::task<void> _task;
		ActorRefWeak _exchActor;
		string_t _exch;
		std::vector<Symbol> _symbols;
		MessageRef _reqMsg;
	};
}
#endif //__BALANCEACTOR__