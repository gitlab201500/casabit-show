#ifndef __EXCHACTOR__
#define __EXCHACTOR__

#include "actor.h"
#include "arbdef.h"
#include "exchutil.h"
#include "tradeutil.h"
#include "commonutil.h"
#include "exchentity.h"


using namespace common;
using namespace exchangelib;
using namespace tradelib;
using namespace actor;
using namespace arblib;
namespace bittrade1{
	DECLARE_LOGGER(ExchActor);
	struct ExchActorConfig{
		ExchActorConfig( 
			int pf = x_printFreqDefault) : printFreq(pf){}
		int printFreq;
	};
	class ExchActor :public actor::ActorBase{
		DECLARE_SHARED_REF_OBJ(ExchActor, const common::string_t& n, exchangelib::Exchange exch,
		const ApiKeyRef& apiKey, const std::map<Pair, ActorRefWeak> & arbActors,
			actor::ActorSystemRefWeak as,
		const ExchActorConfig& aconf = ExchActorConfig(),
			const actor::ActorBaseConfig& conf = actor::ActorBaseConfig());
	public:
		virtual ~ExchActor(){}
	private:
		ExchActorConfig _aconf;
		pplx::task<void> _task;
		std::function<void(const actor::MessageRef& m)> _mainLoop;
		exchangelib::Exchange _exch;
		string_t _exchStr;
		Stats<long long> _depthStats;
		Stats<long long> _balanceStats;
		ApiKeyRef _apiKey;
		std::map<Pair, ActorRefWeak> _arbActors;
	};
}
#endif //__EXCHACTOR__