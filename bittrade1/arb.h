#ifndef __ARB__
#define __ARB__

#include "actor.h"
#include "arbdef.h"
#include "exchutil.h"
#include "tradeutil.h"
#include "commonutil.h"
#include "exchentity.h"
#include "exchactor.h"
#include "depthactor.h"
#include "balanceactor.h"

using namespace common;
using namespace tradelib;
using namespace actor;
namespace bittrade1{
	DECLARE_LOGGER(Arb);
	struct ArbCfg{
		ArbCfg(const std::map<string_t,ApiKeyRef>& keys,  
		int pf = x_printFreqDefault, std::chrono::milliseconds d = x_delayDepthDefault) :
		apiKeys(keys), printFreq(pf), delay(d),
		isTest(false), quoteShift(0.){}
		ExchActorConfig toExchActorConfig() const {
			return ExchActorConfig(printFreq);
		}
		DepthActorConfig toDepthActorConfig() const{
			return DepthActorConfig(delay, printFreq);
		}
		BalanceActorConfig toBalanceActorConfig() const {
			return BalanceActorConfig(x_delayBalanceDefault, printFreq);
		}
		const std::map<string_t, ApiKeyRef> apiKeys;
		const int printFreq;
		const std::chrono::milliseconds delay;
		bool isTest;
		double quoteShift;
	};
	class Arb{
	public:
		Arb(const std::vector<string_t>& e,
			const std::vector<exchangelib::Pair> pairs,
			const ArbCfg& cfg);
		~Arb();
		pplx::task<bool> onTerminate(){ return _as->onTerminated(); }
		void terminate(){ _as->terminate(); }
	private:
		ActorSystemRef _as;
		const ArbCfg _cfg;
		const std::vector<string_t> _exchanges;
		const std::vector<exchangelib::Pair> _pairs;
		std::vector<exchangelib::Symbol> _symbols;
	};
}
#endif //__ARB__