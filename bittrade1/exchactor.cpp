#include <thread>
#include "commonpplx.h"
#include "exchactor.h"
#include "exchanges.h"
#include "tradeapicoll.h"
#include "arbmsg.h"

namespace bittrade1{
	DEFINE_LOGGER(ExchActor, INFO);
	DEFINE_SHARED_REF_OBJ(ExchActor, const common::string_t& n, exchangelib::Exchange exch,
		const ApiKeyRef& apiKey,
		const std::map<Pair, ActorRefWeak> & arbActors,
		actor::ActorSystemRefWeak as,
		const ExchActorConfig& aconf,
		const actor::ActorBaseConfig& conf){
		auto p = MAKE_SHARED(ExchActor);
		p->initActorBase(n, as, conf);
		p->_aconf = aconf;
		p->_exch = exch;
		p->_exchStr = exchtos(p->_exch);
		p->_apiKey = apiKey;
		p->_arbActors = arbActors;
		auto exchFact = exchangelib::x_exchangeCollection.find(p->_exchStr);
		if (exchFact == exchangelib::x_exchangeCollection.end()){
			throw std::exception(std::string("Invalid exchange: " + ws2s(p->_exchStr)).c_str());
		}
		auto depthFact = exchFact->second->depthFactory();

		auto tradeFact = tradelib::x_tradeApiCollection.find(p->_exchStr);
		if (tradeFact == tradelib::x_tradeApiCollection.end()){
			throw std::exception(std::string("Invalid exchange: " + ws2s(p->_exchStr)).c_str());
		}
		auto balanceFact = tradeFact->second->balanceFactory();

		p->_mainLoop = std::function<void(const actor::MessageRef&)>([p, depthFact, balanceFact](const actor::MessageRef m){
			common::stringstream_t ss;
			assert(m);
			actor::MessageType type = m->type();
			switch (type){
			case mtDepthReq:
			{
				auto mm = CAST_MESSAGE(DepthReq, m);
				logDebug(logExchActor, U("Received message: ") << type << U(" pair:") << mm->pair);

				auto d = depthFact(mm->pair, DepthCfg());
				assert(d);
				auto start = epochTimeMs();
				d->fetch().then([p, start](pplx::task<DepthRef>&t){
					auto r = exchangelib::Try(std::function<DepthRef()>([t,p](){
						return t.get();
					}));
					if (r.isFailure()){
						logDebug(logExchActor, U("Failed to query depth error: ") << exchtos(p->_exch)<<U(" ")<<s2ws(r.failure().what()));
						p->_depthStats.addFail();
					}
					else {
						assert(r.success());
						auto rt = epochTimeMs() - start;
						p->_depthStats = p->_depthStats + rt;
						logDebug(logExchActor, U("Depth: ") << exchtos(p->_exch) << U(" ask: ") <<
							(r.success()->info.asks.size() ? r.success()->info.asks[0].q : 0.0) << U(" bid: ") <<
							(r.success()->info.bids.size() ? r.success()->info.bids[0].q : 0.0));
						auto arbRef = p->_arbActors.find(r.success()->pair);
						assert(arbRef != p->_arbActors.end());
						auto arb = arbRef->second.lock();
						if (arb){
							arb->tell(std::make_shared<DepthResp>(common::epochTimeMs(), rt, p->_exch, r.success()));
						}
						
					}
					if (p->_depthStats.count() && (p->_depthStats.count() % p->_aconf.printFreq == 0)){
						logInfo(logExchActor, exchtos(p->_exch)<<U(" Depth stats: ")<<p->_depthStats.toString());
						p->_depthStats = Stats<long long>();
					}
				},p->cancellationToken());
				
				break;
			}
			case mtBalanceReq:
			{
				auto mm = CAST_MESSAGE(BalanceReq, m);
				logDebug(logExchActor, U("Received message: ") << type << U(" symbols:") << mm->symbols[0]);

				auto d = balanceFact(mm->symbols, BalanceCfg(p->_apiKey));
				assert(d);
				auto start = epochTimeMs();
				d->fetch().then([p, start](pplx::task<BalanceSeqRef>&t){
					auto r = exchangelib::Try(std::function<BalanceSeqRef()>([t, p](){
						return t.get();
					}));
					if (r.isFailure()){
						logDebug(logExchActor, U("Failed to query balance error: ") << exchtos(p->_exch) << U(" ") << s2ws(r.failure().what()));
						p->_balanceStats.addFail();
					}
					else {
						assert(r.success());
						auto rt = epochTimeMs() - start;
						p->_balanceStats = p->_balanceStats + rt;
						logDebug(logExchActor, U("Balance: ") << exchtos(p->_exch) << balSeqToStr(r.success()));
						for (auto aa : p->_arbActors){
							auto v = std::make_shared<std::vector<tradelib::Balance>>();
							for (auto b : *r.success()){
								auto syms = pairToSymbols(aa.first);
								if (std::find(syms.cbegin(), syms.cend(), b.symbol) != syms.cend()){
									v->push_back(b);
								}
							}

							if (!v->empty()){
								auto arb = aa.second.lock();
								if (arb){
									arb->tell(std::make_shared<BalanceResp>(common::epochTimeMs(), p->_exch, v));
								}
							}
						}
					}
					if (p->_balanceStats.count() && (p->_balanceStats.count() % p->_aconf.printFreq == 0)){
						logInfo(logExchActor, exchtos(p->_exch) << U(" Balance: ") << p->_exch << balSeqToStr(r.success()) <<
							U(" Balance stats: ") << p->_balanceStats.toString());
						p->_balanceStats = Stats<long long>();
					}
				}, p->cancellationToken());

				break;
			}

			default:
				ss << U("Unexpected message type: ") << type;
				logWarn(logExchActor, ss.str());
				break;
			}
		});

		p->becomes(p->_mainLoop);

		return p;
	}

}