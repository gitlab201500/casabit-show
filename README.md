# Simple cryptocurrency arbiter

## Exchangelib

Library to query *depth/orderbook* from multiple exchanges

## Actor
Actor library

## Arblib
Implements DepathMatcher to find a profitable trade given depth from 2 exchnages

## Bittool
Cmd line utilities

## Bittrade1

Currency arbiter. Compares depths on 2 exchanges and finds a profitable trade.
Supports test mode when depth values on one exchange are multiplied by test factor less than 1 to create a profitable trade each time. E.g. to decrease real-time quote by
10% use 10 as an argument to test mode

## Tradelib
Library which implements trade api. Requires API key from an exchange.

## How to build
Use VS 2013

